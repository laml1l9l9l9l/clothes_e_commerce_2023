'use strict'

const express = require('express')
const adminService = require('../services/admin')

const adminRoute = express.Router()

// Route: Authorization
adminRoute.post('/login', adminService.login)
adminRoute.post('/verify-token', adminService.verify)
adminRoute.post('/generate-qr-2fa', adminService.generateQr2FA)
adminRoute.post('/remove-qr-2fa', adminService.removeQr2FA)
adminRoute.post('/verify-otp-2fa', adminService.verifyOTP2FA)
adminRoute.post('/update-account', adminService.updateAccount)

// Route: Product
adminRoute.get('/product/search', adminService.searchProducts)
adminRoute.post('/product/add', adminService.addProduct)
adminRoute.post('/product/edit', adminService.editProduct)
adminRoute.post('/product/delete', adminService.deleteProduct)
adminRoute.get('/product/:id', adminService.getProduct)

// Route: Customer
adminRoute.get('/customer/search', adminService.searchCustomers)
adminRoute.post('/customer/add', adminService.addCustomer)
adminRoute.post('/customer/edit', adminService.editCustomer)
adminRoute.post('/customer/delete', adminService.deleteCustomer)
adminRoute.get('/customer/:id', adminService.getCustomer)

// Route: Order (Thống kê + quanr lý đơn hàng)
adminRoute.get('/order/search', adminService.searchOrders)
adminRoute.post('/order/edit', adminService.editOrder)
adminRoute.get('/order/:id', adminService.getOrder)

// Route: Transaction (Thống kê giao dịch)
adminRoute.get('/transaction/search', adminService.searchTransactions)

// Route: Statistic
adminRoute.get('/stats/total-money', adminService.totalMoneyStats)
adminRoute.get('/stats/total-orders', adminService.totalOrdersStats)
adminRoute.get('/stats/total-products', adminService.totalProductsStats)
adminRoute.get('/stats/total-customers', adminService.totalCustomersStats)
adminRoute.get('/stats/revenue', adminService.revenueStats)

module.exports = adminRoute
