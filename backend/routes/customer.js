'use strict'

const express = require('express')

const customerService = require('../services/customer')
const customerRoute = express.Router()

// Route: Authorization
customerRoute.post('/login', customerService.login)
customerRoute.post('/verify-token', customerService.verify)
customerRoute.post('/decode-token', customerService.decodeToken)
customerRoute.post('/register', customerService.register)
customerRoute.post('/update-account', customerService.updateAccount)

// Route: Product
customerRoute.get('/search-products', customerService.searchProducts)

// Route: Payment
customerRoute.post('/valid-data-basket', customerService.validDataBasket)
customerRoute.post('/checkout-basket', customerService.checkoutBasket)
customerRoute.post('/payments-create', customerService.paymentsCreate)

// Route: Order
customerRoute.get('/search-orders', customerService.searchOrders)

module.exports = customerRoute
