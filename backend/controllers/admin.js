'use strict'

const moment = require('moment')
const jwt = require("jsonwebtoken")
const speakeasy = require("speakeasy")
const qrcode = require("qrcode")
const util = require("../utils/util")
const config = require("../utils/config")
const defineCode = require("../utils/define_code")

const getConnDb = util.getConnDb
const makeAdminRes = util.makeAdminRes
const makeAdminResSuccErr = util.makeAdminResSuccErr
const IS_LOG_IN_SERVICE = config.is_log_in_service
  , ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ = defineCode.ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ
  , NAME_SCHEMA = defineCode.NAME_SCHEMA
  , STATUS_CODE_RES = defineCode.STATUS_CODE_RES
  , DATE_FORMAT = config.date_format

const adminController = {
  login: async (connectUsingTransaction, infoAuth) => {
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , token = ''
      , infoAccAdmin = null
    connDb = getConnDb(connectUsingTransaction)
    try {
      const STATUS_ACCOUNT_MANAGE = defineCode.STATUS_ACCOUNT_MANAGE
      const sqlGetInfoUser = `select count(*) countAcc, id, username, fullname, tel, status_2fa from ${NAME_SCHEMA}.account_manage where username = ? and password = ? and status = ?`
      let convertMd5Pwd = ''
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(infoAuth.password)) {
        convertMd5Pwd = util.encryptMd5Password(infoAuth.password)
      }
      let bindsDataUser = [
        infoAuth.username,
        convertMd5Pwd,
        STATUS_ACCOUNT_MANAGE.ACTIVE
      ]
      let resGetUser = await connDb.query(sqlGetInfoUser, bindsDataUser)
      let recordGetUser = resGetUser[0]
      if (recordGetUser && recordGetUser[0] && recordGetUser[0].countAcc == 1) {
        isSuccessRes = true
        messageRes = 'Đăng nhập thành công'
        statusCodeRes = STATUS_CODE_RES.OK_REQ
        let dataAcc = recordGetUser[0]
          , dataAccPushToToken = {
            id: dataAcc.id,
            username: dataAcc.username,
            full_name: dataAcc.fullname,
            tel: dataAcc.tel,
            status_2fa: dataAcc.status_2fa
          }
        // const SECRET = defineCode.SECRET
        //   , EXPIRES = defineCode.EXPIRES
        // let createToken = () => {
        //   return new Promise(async function(resolve, reject) {
        //     jwt.sign(dataAccPushToToken, SECRET, { expiresIn: EXPIRES }, (err, token) => {
        //       if (err) { reject(err) }
        //       resolve(token)
        //     })
        //   })
        // }
        token = await util.createToken(dataAccPushToToken)
        infoAccAdmin = dataAccPushToToken
      } else {
        messageRes = 'Đăng nhập thất bại. Vui lòng kiểm tra lại tài khoản và mật khẩu'
        statusCodeRes = STATUS_CODE_RES.INVALID_USR
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err login(admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      token,
      info_acc: infoAccAdmin,
      status_code: statusCodeRes
    }
    let resDataApi = makeAdminRes(isSuccessRes, messageRes, options)
    return resDataApi
  },
  verify: async (token) => {
    let isVerifyToken = false
      , isSuccessRes = false
      , decodeToken
      , messageRes = ''
      , statusCodeRes
    try {
      let verifyToken = () => {
        return new Promise(async function(resolve, reject) {
          jwt.verify(token, SECRET, async (err, decoded) => {
            if (err) {
              console.log("**Verify token fail:", err)
              statusCodeRes = STATUS_CODE_RES.INVALID_USR
              reject()
            } else {
              isVerifyToken = true
              isSuccessRes = true
              decodeToken = decoded
              statusCodeRes = STATUS_CODE_RES.OK_REQ
              resolve()
            }
          })
        })
      }
      await verifyToken()
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err login(admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      decoded_token: decodeToken,
      is_verify: isVerifyToken,
      status_code: statusCodeRes
    }
    let resDataApi = makeAdminRes(isSuccessRes, messageRes, options)
    return resDataApi
  },
  searchProducts: async (connectUsingTransaction, inputDataSea) => {
    /*
      NOTE:
      - inputData: {
        select_data: 'id, status', (string)
        where_data: {
          id: 123,
          name: 'abc',
          ...
        },
        start: 0,
        max_records: 5
      }
      - outData: {
        is_success: true,
        status_code: 200,
        count_all_records: 10,
        data: [...],
        message: ''
      }
    */
    let connDb
      , listProduct = []
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , searchProdRes
      , countAllRecords = 0
    try {
      connDb = getConnDb(connectUsingTransaction)
      let selectInputData = inputDataSea.select_data
        , whereInputData = inputDataSea.where_data
        , bindsInputData = inputDataSea.binds_data
        , arrWhereQuery = []
        , strWhereQuery = ''
      let sqlGetProduct
        , bindsDataProduct = []
        , bindsCountProducts = []
      let nameInputDataWhere = whereInputData.name
        , nameInputDataBinds = bindsInputData.name
        , statusInputDataWhere = whereInputData.status
        , statusInputDataBinds = bindsInputData.status
        , typeInputDataWhere = whereInputData.type
        , typeInputDataBinds = bindsInputData.type
        , fromDateInputDataWhere = whereInputData.from_date
        , fromDateInputDataBinds = bindsInputData.from_date
        , toDateInputDataWhere = whereInputData.to_date
        , toDateInputDataBinds = bindsInputData.to_date
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(nameInputDataWhere)) {
        arrWhereQuery.push(nameInputDataWhere)
        bindsDataProduct.push(nameInputDataBinds)
        bindsCountProducts.push(nameInputDataBinds)
      }
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(statusInputDataWhere)) {
        arrWhereQuery.push(statusInputDataWhere)
        bindsDataProduct.push(statusInputDataBinds)
        bindsCountProducts.push(statusInputDataBinds)
      }
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(typeInputDataWhere)) {
        arrWhereQuery.push(typeInputDataWhere)
        bindsDataProduct.push(typeInputDataBinds)
        bindsCountProducts.push(typeInputDataBinds)
      }
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(fromDateInputDataWhere)) {
        arrWhereQuery.push(fromDateInputDataWhere)
        bindsDataProduct.push(fromDateInputDataBinds)
        bindsCountProducts.push(fromDateInputDataBinds)
      }
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(toDateInputDataWhere)) {
        arrWhereQuery.push(toDateInputDataWhere)
        bindsDataProduct.push(toDateInputDataBinds)
        bindsCountProducts.push(toDateInputDataBinds)
      }
      if (arrWhereQuery.length > 0) {
        strWhereQuery = arrWhereQuery.join(' and ')
        if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(strWhereQuery)) {
          strWhereQuery = `where ${strWhereQuery}`
        }
      }
      sqlGetProduct = `select ${selectInputData} from ${NAME_SCHEMA}.product ${strWhereQuery} limit ? offset ?`
      bindsDataProduct.push(inputDataSea.max_records)
      bindsDataProduct.push(inputDataSea.start)
      let resGetProduct = await connDb.query(sqlGetProduct, bindsDataProduct)
      listProduct = resGetProduct[0]
      let sqlCountProducts = `select count(*) countProduct from ${NAME_SCHEMA}.product ${strWhereQuery}`
        , resCountProducts = await connDb.query(sqlCountProducts, bindsCountProducts)
      countAllRecords = resCountProducts[0][0].countProduct
      isSuccessRes = true
      statusCodeRes = STATUS_CODE_RES.OK_REQ
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err search product(admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      data: listProduct,
      status_code: statusCodeRes,
      count_all_records: countAllRecords
    }
    searchProdRes = makeAdminRes(isSuccessRes, messageRes, options)
    return searchProdRes
  },
  addProduct: async (connectUsingTransaction, inputDataAdd) => {
    /*
      NOTE:
      - inputData: {
        name: '',
        price: 1000,
        discount: 100,
        quantity_in_warehouse: 10,
        status: 1,
        type: 1,
        description: '',
        picture: 'sdfjhjgfhjsdfg23' // base64 encoded
      }
      - outData: {
        is_success: false,
        status_code: 500,
        message: ''
      }
    */
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , addProdRes
    try {
      connDb = getConnDb(connectUsingTransaction)
      const sqlInsProduct = `insert into ${NAME_SCHEMA}.product set ?`
        , bindsDataProduct = {
          name: inputDataAdd.name,
          price: inputDataAdd.price,
          discount: inputDataAdd.discount,
          quantity_in_warehouse: inputDataAdd.quantity_in_warehouse,
          status: inputDataAdd.status,
          type: inputDataAdd.type,
          description: inputDataAdd.description,
          picture: inputDataAdd.picture
        }
      let resInsProduct
        , recordInsProduct
      resInsProduct = await connDb.query(sqlInsProduct, bindsDataProduct)
      recordInsProduct = resInsProduct[0].affectedRows
      if (recordInsProduct <= 0) {
        messageRes = "Thêm mới thất bại"
      } else {
        isSuccessRes = true
        messageRes = "Thêm mới thành công"
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err add product(admin-controller):", err)
      }
      messageRes = err
    }
    addProdRes = makeAdminRes(isSuccessRes, messageRes)
    return addProdRes
  },
  editProduct: async (connectUsingTransaction, inputDataEdit) => {
    /*
      NOTE:
      - inputData: {
        id: 1,
        name: '',
        status: 1,
        type: 1,
        price: 1000,
        discount: 100,
        quantity_in_warehouse: 10,
        description: '',
        picture: 'sdfjhjgfhjsdfg23' // base64 encoded
      }
      - outData: {
        is_success: false,
        status_code: 500,
        message: ''
      }
    */
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , editProdRes
    try {
      connDb = getConnDb(connectUsingTransaction)
      let sqlUpdProduct
        , bindsDataProduct = []
        , arrDataUpdCondition = []
        , strDataUpdCondition = ''
      // STEP: Add data update into update query
      let newName = inputDataEdit.name
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newName)) {
        arrDataUpdCondition.push('name=?')
        bindsDataProduct.push(newName)
      }
      let newStatus = inputDataEdit.status
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newStatus)) {
        newStatus = parseInt(newStatus)
        arrDataUpdCondition.push('status=?')
        bindsDataProduct.push(newStatus)
      }
      let newType = inputDataEdit.type
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newType)) {
        newType = parseInt(newType)
        arrDataUpdCondition.push('type=?')
        bindsDataProduct.push(newType)
      }
      let newDiscount = inputDataEdit.discount
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newDiscount)) {
        newDiscount = parseFloat(newDiscount)
        arrDataUpdCondition.push('discount=?')
        bindsDataProduct.push(newDiscount)
      }
      let newQuantityInWarehouse = inputDataEdit.quantity_in_warehouse
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newQuantityInWarehouse)) {
        newQuantityInWarehouse = parseFloat(newQuantityInWarehouse)
        arrDataUpdCondition.push('quantity_in_warehouse=?')
        bindsDataProduct.push(newQuantityInWarehouse)
      }
      let newDescription = inputDataEdit.description
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newDescription)) {
        arrDataUpdCondition.push('description=?')
        bindsDataProduct.push(newDescription)
      }
      let newPicture = inputDataEdit.picture
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newPicture)) {
        arrDataUpdCondition.push('picture=?')
        bindsDataProduct.push(newPicture)
      }
      // STEP: Add data where into update query
      bindsDataProduct.push(inputDataEdit.id)
      strDataUpdCondition = arrDataUpdCondition.join(',')
      sqlUpdProduct = `update ${NAME_SCHEMA}.product set ${strDataUpdCondition} where id=?`
      let resUpdProduct
        , recordUpdProduct
      resUpdProduct = await connDb.query(sqlUpdProduct, bindsDataProduct)
      recordUpdProduct = resUpdProduct[0].affectedRows
      if (recordUpdProduct <= 0) {
        messageRes = "Cập nhật thất bại"
      } else {
        isSuccessRes = true
        messageRes = "Cập nhật thành công"
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err edit product(admin-controller):", err)
      }
      messageRes = err
    }
    editProdRes = makeAdminRes(isSuccessRes, messageRes)
    return editProdRes
  },
  delProduct: async (connectUsingTransaction, inputDataDel) => {
    /*
      NOTE:
      - inputData: {
        id: 1
      }
      - outData: {
        is_success: false,
        status_code: 500,
        message: ''
      }
    */
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , delProdRes
    try {
      connDb = getConnDb(connectUsingTransaction)
      let sqlDelProduct = `delete from ${NAME_SCHEMA}.product where id=?`
        , bindsDataProduct = [inputDataDel.id]
      let resDelProduct
        , recordDelProduct
      resDelProduct = await connDb.query(sqlDelProduct, bindsDataProduct)
      recordDelProduct = resDelProduct[0].affectedRows
      if (recordDelProduct <= 0) {
        messageRes = "Xóa thất bại"
      } else {
        isSuccessRes = true
        messageRes = "Xóa thành công"
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err delete product(admin-controller):", err)
      }
      messageRes = err
    }
    delProdRes = makeAdminRes(isSuccessRes, messageRes)
    return delProdRes
  },
  searchProduct: async (connectUsingTransaction, idProduct) => {
    /*
      NOTE:
      - inputData: idProduct
      - outputData: {
        success: {
          data: {
            ...
          }
        },
        error: {
          mess: 'Không tìm thấy sản phẩm'
        }
      }
    */
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , getProdRes
      , infoProduct
    try {
      let sqlGetProduct = `select name, type, status, price, discount, quantity_in_warehouse as quantity, description, picture from ${NAME_SCHEMA}.product where id=? limit 1`
        , bindsDataProduct = [idProduct]
      connDb = getConnDb(connectUsingTransaction)
      let resGetProduct = await connDb.query(sqlGetProduct, bindsDataProduct)
      infoProduct = resGetProduct[0][0]
      let infoPictureProduct = infoProduct.picture
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(infoPictureProduct)) {
        infoProduct.picture = `${infoPictureProduct}`
      }
      isSuccessRes = true
      statusCodeRes = STATUS_CODE_RES.OK_REQ
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err get product(admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      data: infoProduct,
      status_code: statusCodeRes
    }
    getProdRes = makeAdminResSuccErr(isSuccessRes, messageRes, options)
    return getProdRes
  },
  makeWhereAndBindsSearchProducts: (entryData) => {
    /*
      NOTE:
      - inputData: {
        status: '1',
        name: 'Ao so',
        from_date: '2023-03-30 00:00:00',
        to_date: '2023-04-02 23:59:59',
        type: '1',
      }
      - outputData: {
        where_data: {...},
        binds_data: {...}
      }
    */
    let dataRes = {}
      , whereData = {}
      , bindsData = {}
      , nameInputData = entryData.name
      , statusInputData = entryData.status
      , typeInputData = entryData.type
      , fromDateInputData = entryData.from_date
      , toDateInputData = entryData.to_date
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(nameInputData)) {
      whereData.name = 'name like ?'
      bindsData.name = `%${nameInputData}%`
    }
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(statusInputData)) {
      whereData.status = 'status = ?'
      bindsData.status = parseInt(statusInputData)
    }
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(typeInputData)) {
      whereData.type = 'type = ?'
      bindsData.type = parseInt(typeInputData)
    }
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(fromDateInputData)) {
      whereData.from_date = 'created_date >= ?'
      bindsData.from_date = `${moment(fromDateInputData).format(DATE_FORMAT)} 00:00:00`
    }
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(toDateInputData)) {
      whereData.to_date = 'created_date <= ?'
      bindsData.to_date = `${moment(toDateInputData).format(DATE_FORMAT)} 23:59:59`
    }
    dataRes = {
      where_data: whereData,
      binds_data: bindsData
    }
    return dataRes
  },
  searchCustomers: async (connectUsingTransaction, inputDataSea) => {
    /*
      NOTE:
      - inputData: {
        select_data: 'id, status', (string)
        where_data: {
          id: 123,
          name: 'abc',
          ...
        },
        start: 0,
        max_records: 5
      }
      - outData: {
        is_success: true,
        status_code: 200,
        count_all_records: 10,
        data: [...],
        message: ''
      }
    */
    let connDb
      , listCustomer = []
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , searchCusRes
      , countAllRecords = 0
    try {
      connDb = getConnDb(connectUsingTransaction)
      let selectInputData = inputDataSea.select_data
        , whereInputData = inputDataSea.where_data
        , bindsInputData = inputDataSea.binds_data
        , arrWhereQuery = []
        , strWhereQuery = ''
      let sqlGetCustomer
        , bindsDataCustomer = []
        , bindsCountCustomers = []
      let usernameInputDataWhere = whereInputData.username
        , usernameInputDataBinds = bindsInputData.username
        , statusInputDataWhere = whereInputData.status
        , statusInputDataBinds = bindsInputData.status
        , fromDateInputDataWhere = whereInputData.from_date
        , fromDateInputDataBinds = bindsInputData.from_date
        , toDateInputDataWhere = whereInputData.to_date
        , toDateInputDataBinds = bindsInputData.to_date
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(usernameInputDataWhere)) {
        arrWhereQuery.push(usernameInputDataWhere)
        bindsDataCustomer.push(usernameInputDataBinds)
        bindsCountCustomers.push(usernameInputDataBinds)
      }
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(statusInputDataWhere)) {
        arrWhereQuery.push(statusInputDataWhere)
        bindsDataCustomer.push(statusInputDataBinds)
        bindsCountCustomers.push(statusInputDataBinds)
      }
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(fromDateInputDataWhere)) {
        arrWhereQuery.push(fromDateInputDataWhere)
        bindsDataCustomer.push(fromDateInputDataBinds)
        bindsCountCustomers.push(fromDateInputDataBinds)
      }
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(toDateInputDataWhere)) {
        arrWhereQuery.push(toDateInputDataWhere)
        bindsDataCustomer.push(toDateInputDataBinds)
        bindsCountCustomers.push(toDateInputDataBinds)
      }
      if (arrWhereQuery.length > 0) {
        strWhereQuery = arrWhereQuery.join(' and ')
        if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(strWhereQuery)) {
          strWhereQuery = `where ${strWhereQuery}`
        }
      }
      sqlGetCustomer = `select ${selectInputData} from ${NAME_SCHEMA}.account_customer ${strWhereQuery} limit ? offset ?`
      bindsDataCustomer.push(inputDataSea.max_records)
      bindsDataCustomer.push(inputDataSea.start)
      let resGetCustomer = await connDb.query(sqlGetCustomer, bindsDataCustomer)
      listCustomer = resGetCustomer[0]
      let sqlCountCustomers = `select count(*) countCustomer from ${NAME_SCHEMA}.account_customer ${strWhereQuery}`
        , resCountCustomers = await connDb.query(sqlCountCustomers, bindsCountCustomers)
      countAllRecords = resCountCustomers[0][0].countCustomer
      isSuccessRes = true
      statusCodeRes = STATUS_CODE_RES.OK_REQ
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err search customer(admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      data: listCustomer,
      status_code: statusCodeRes,
      count_all_records: countAllRecords
    }
    searchCusRes = makeAdminRes(isSuccessRes, messageRes, options)
    return searchCusRes
  },
  makeWhereAndBindsSearchCustomers: (entryData) => {
    /*
      NOTE:
      - inputData: {
        status: '1',
        name: 'Ao so',
        from_date: '2023-03-30 00:00:00',
        to_date: '2023-04-02 23:59:59',
      }
      - outputData: {
        where_data: {...},
        binds_data: {...}
      }
    */
    let dataRes = {}
      , whereData = {}
      , bindsData = {}
      , usernameInputData = entryData.username
      , statusInputData = entryData.status
      , fromDateInputData = entryData.from_date
      , toDateInputData = entryData.to_date
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(usernameInputData)) {
      whereData.username = 'username like ?'
      bindsData.username = `%${usernameInputData}%`
    }
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(statusInputData)) {
      whereData.status = 'status = ?'
      bindsData.status = parseInt(statusInputData)
    }
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(fromDateInputData)) {
      whereData.from_date = 'created_date >= ?'
      bindsData.from_date = `${moment(fromDateInputData).format(DATE_FORMAT)} 00:00:00`
    }
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(toDateInputData)) {
      whereData.to_date = 'created_date <= ?'
      bindsData.to_date = `${moment(toDateInputData).format(DATE_FORMAT)} 23:59:59`
    }
    dataRes = {
      where_data: whereData,
      binds_data: bindsData
    }
    return dataRes
  },
  delCustomer: async (connectUsingTransaction, inputDataDel) => {
    /*
      NOTE:
      - inputData: {
        id: 1
      }
      - outData: {
        is_success: false,
        status_code: 500,
        message: ''
      }
    */
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , delCustomerRes
    try {
      connDb = getConnDb(connectUsingTransaction)
      let sqlDelCustomer = `delete from ${NAME_SCHEMA}.account_customer where id=?`
        , bindsDataCustomer = [inputDataDel.id]
      let resDelCustomer
        , recordDelCustomer
      resDelCustomer = await connDb.query(sqlDelCustomer, bindsDataCustomer)
      recordDelCustomer = resDelCustomer[0].affectedRows
      if (recordDelCustomer <= 0) {
        messageRes = "Xóa thất bại"
      } else {
        isSuccessRes = true
        messageRes = "Xóa thành công"
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err delete customer(admin-controller):", err)
      }
      messageRes = err
    }
    delCustomerRes = makeAdminRes(isSuccessRes, messageRes)
    return delCustomerRes
  },
  addCustomer: async (connectUsingTransaction, inputDataAdd) => {
    /*
      NOTE:
      - inputData: {
        username: 'abc_hd',
        password: 'ad12323123', (default: '')
        full_name: 'Nguyen Tu A',
        tel: '01231210',
        email: 'sdfshb@gmail.com',
        address: 'So 12, ...',
        btax: '23489834',
        status: 1
      }
      - outData: {
        is_success: false,
        status_code: 500,
        message: ''
      }
    */
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , addCustomerRes
    try {
      connDb = getConnDb(connectUsingTransaction)
      let convertMd5Pwd = ''
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(inputDataAdd.password)) {
        convertMd5Pwd = util.encryptMd5Password(inputDataAdd.password)
      }
      const sqlInsCustomer = `insert into ${NAME_SCHEMA}.account_customer set ?`
        , bindsDataCustomer = {
          username: inputDataAdd.username,
          password: convertMd5Pwd,
          fullname: inputDataAdd.full_name,
          tel: inputDataAdd.tel,
          email: inputDataAdd.email,
          address: inputDataAdd.address,
          btax: inputDataAdd.btax,
          status: inputDataAdd.status
        }
      let resInsCustomer
        , recordInsCustomer
      resInsCustomer = await connDb.query(sqlInsCustomer, bindsDataCustomer)
      recordInsCustomer = resInsCustomer[0].affectedRows
      if (recordInsCustomer <= 0) {
        messageRes = "Thêm mới thất bại"
      } else {
        isSuccessRes = true
        messageRes = "Thêm mới thành công"
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err add customer(admin-controller):", err)
      }
      messageRes = err.message
    }
    addCustomerRes = makeAdminRes(isSuccessRes, messageRes)
    return addCustomerRes
  },
  editCustomer: async (connectUsingTransaction, inputDataEdit) => {
    /*
      NOTE:
      - inputData: {
        id: 1,
        password: 'ad12323123', (default: '')
        full_name: 'Nguyen Tu A',
        tel: '01231210',
        email: 'sdfshb@gmail.com',
        address: 'So 12, ...',
        btax: '23489834',
        status: 1
      }
      - outData: {
        is_success: false,
        message: ''
      }
    */
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , editProdRes
    try {
      connDb = getConnDb(connectUsingTransaction)
      let sqlUpdCustomer
        , bindsDataCustomer = []
        , arrDataUpdCondition = []
        , strDataUpdCondition = ''
      // STEP: Add data update into update query
      let newPwd = inputDataEdit.password
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newPwd)) {
        newPwd = util.encryptMd5Password(newPwd)
        arrDataUpdCondition.push('password=?')
        bindsDataCustomer.push(newPwd)
      }
      let newFullName = inputDataEdit.full_name
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newFullName)) {
        arrDataUpdCondition.push('fullname=?')
        bindsDataCustomer.push(newFullName)
      }
      let newTel = inputDataEdit.tel
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newTel)) {
        arrDataUpdCondition.push('tel=?')
        bindsDataCustomer.push(newTel)
      }
      let newEmail = inputDataEdit.email
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newEmail)) {
        arrDataUpdCondition.push('email=?')
        bindsDataCustomer.push(newEmail)
      }
      let newAddress = inputDataEdit.address
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newAddress)) {
        arrDataUpdCondition.push('address=?')
        bindsDataCustomer.push(newAddress)
      }
      let newTaxCode = inputDataEdit.btax
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newTaxCode)) {
        arrDataUpdCondition.push('btax=?')
        bindsDataCustomer.push(newTaxCode)
      }
      let newStatus = inputDataEdit.status
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newStatus)) {
        arrDataUpdCondition.push('status=?')
        bindsDataCustomer.push(newStatus)
      }
      // STEP: Add data where into update query
      bindsDataCustomer.push(inputDataEdit.id)
      strDataUpdCondition = arrDataUpdCondition.join(',')
      sqlUpdCustomer = `update ${NAME_SCHEMA}.account_customer set ${strDataUpdCondition} where id=?`
      let resUpdCustomer
        , recordUpdCustomer
      resUpdCustomer = await connDb.query(sqlUpdCustomer, bindsDataCustomer)
      recordUpdCustomer = resUpdCustomer[0].affectedRows
      if (recordUpdCustomer <= 0) {
        messageRes = "Cập nhật thất bại"
      } else {
        isSuccessRes = true
        messageRes = "Cập nhật thành công"
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err edit customer(admin-controller):", err)
      }
      messageRes = err
    }
    editProdRes = makeAdminRes(isSuccessRes, messageRes)
    return editProdRes
  },
  searchCustomer: async (connectUsingTransaction, idCustomer) => {
    /*
      NOTE:
      - inputData: idCustomer
      - outputData: {
        success: {
          data: {
            ...
          }
        },
        error: {
          mess: 'Không tìm thấy sản phẩm'
        }
      }
    */
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , getCusRes
      , infoCustomer
    try {
      let sqlGetCustomer = `select username, fullname, tel, email, address, btax, status from ${NAME_SCHEMA}.account_customer where id=? limit 1`
        , bindsDataCustomer = [idCustomer]
      connDb = getConnDb(connectUsingTransaction)
      let resGetCustomer = await connDb.query(sqlGetCustomer, bindsDataCustomer)
      infoCustomer = resGetCustomer[0][0]
      isSuccessRes = true
      statusCodeRes = STATUS_CODE_RES.OK_REQ
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err get customer(admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      data: infoCustomer,
      status_code: statusCodeRes
    }
    getCusRes = makeAdminResSuccErr(isSuccessRes, messageRes, options)
    return getCusRes
  },
  searchOrders: async (connectUsingTransaction, inputDataSea) => {
    /*
      NOTE:
      - inputData: {
        select_data: 'id, status', (string)
        where_data: {
          id: 123,
          status: 1,
          ...
        },
        start: 0,
        max_records: 5
      }
      - outData: {
        is_success: true,
        status_code: 200,
        count_all_records: 10,
        data: [...],
        message: ''
      }
    */
    let connDb
      , listOrder = []
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , searchOrderRes
      , countAllRecords = 0
    try {
      connDb = getConnDb(connectUsingTransaction)
      let selectInputData = inputDataSea.select_data
        , whereInputData = inputDataSea.where_data
        , bindsInputData = inputDataSea.binds_data
        , arrWhereQuery = []
        , strWhereQuery = ''
      let sqlGetOrder
        , bindsDataOrder = []
        , bindsCountOrders = []
      let statusInputDataWhere = whereInputData.status
        , statusInputDataBinds = bindsInputData.status
        , fromDateInputDataWhere = whereInputData.from_date
        , fromDateInputDataBinds = bindsInputData.from_date
        , toDateInputDataWhere = whereInputData.to_date
        , toDateInputDataBinds = bindsInputData.to_date
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(statusInputDataWhere)) {
        arrWhereQuery.push(statusInputDataWhere)
        bindsDataOrder.push(statusInputDataBinds)
        bindsCountOrders.push(statusInputDataBinds)
      }
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(fromDateInputDataWhere)) {
        arrWhereQuery.push(fromDateInputDataWhere)
        bindsDataOrder.push(fromDateInputDataBinds)
        bindsCountOrders.push(fromDateInputDataBinds)
      }
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(toDateInputDataWhere)) {
        arrWhereQuery.push(toDateInputDataWhere)
        bindsDataOrder.push(toDateInputDataBinds)
        bindsCountOrders.push(toDateInputDataBinds)
      }
      if (arrWhereQuery.length > 0) {
        strWhereQuery = arrWhereQuery.join(' and ')
        if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(strWhereQuery)) {
          strWhereQuery = `where ${strWhereQuery}`
        }
      }
      sqlGetOrder = `select ${selectInputData} from ${NAME_SCHEMA}.order ${strWhereQuery} limit ? offset ?`
      bindsDataOrder.push(inputDataSea.max_records)
      bindsDataOrder.push(inputDataSea.start)
      let resGetOrder = await connDb.query(sqlGetOrder, bindsDataOrder)
      listOrder = resGetOrder[0]
      let sqlCountOrders = `select count(*) countOrder from ${NAME_SCHEMA}.order ${strWhereQuery}`
        , resCountOrders = await connDb.query(sqlCountOrders, bindsCountOrders)
      countAllRecords = resCountOrders[0][0].countOrder
      isSuccessRes = true
      statusCodeRes = STATUS_CODE_RES.OK_REQ
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err search order(admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      data: listOrder,
      status_code: statusCodeRes,
      count_all_records: countAllRecords
    }
    searchOrderRes = makeAdminRes(isSuccessRes, messageRes, options)
    return searchOrderRes
  },
  makeWhereAndBindsSearchOrders: (entryData) => {
    /*
      NOTE:
      - inputData: {
        status: '1',
        from_date: '2023-03-30 00:00:00',
        to_date: '2023-04-02 23:59:59',
      }
      - outputData: {
        where_data: {...},
        binds_data: {...}
      }
    */
    let dataRes = {}
      , whereData = {}
      , bindsData = {}
      , statusInputData = entryData.status
      , fromDateInputData = entryData.from_date
      , toDateInputData = entryData.to_date
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(statusInputData)) {
      whereData.status = 'status = ?'
      bindsData.status = parseInt(statusInputData)
    }
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(fromDateInputData)) {
      whereData.from_date = 'created_date >= ?'
      bindsData.from_date = `${moment(fromDateInputData).format(DATE_FORMAT)} 00:00:00`
    }
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(toDateInputData)) {
      whereData.to_date = 'created_date <= ?'
      bindsData.to_date = `${moment(toDateInputData).format(DATE_FORMAT)} 23:59:59`
    }
    dataRes = {
      where_data: whereData,
      binds_data: bindsData
    }
    return dataRes
  },
  editOrder: async (connectUsingTransaction, inputDataEdit) => {
    /*
      NOTE:
      - inputData: {
        id: 1,
        status: 1,
      }
      - outData: {
        is_success: false,
        status_code: 500,
        message: ''
      }
    */
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , editEditRes
    try {
      connDb = getConnDb(connectUsingTransaction)
      let sqlUpdOrder
        , bindsDataOrder = []
        , arrDataUpdCondition = []
        , strDataUpdCondition = ''
      // STEP: Add data update into update query
      let newStatus = inputDataEdit.status
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newStatus)) {
        newStatus = parseInt(newStatus)
        arrDataUpdCondition.push('status=?')
        bindsDataOrder.push(newStatus)
      }
      // STEP: Add data where into update query
      bindsDataOrder.push(inputDataEdit.id)
      strDataUpdCondition = arrDataUpdCondition.join(',')
      sqlUpdOrder = `update ${NAME_SCHEMA}.order set ${strDataUpdCondition} where id=?`
      let resUpdOrder
        , recordUpdOrder
      resUpdOrder = await connDb.query(sqlUpdOrder, bindsDataOrder)
      recordUpdOrder = resUpdOrder[0].affectedRows
      if (recordUpdOrder <= 0) {
        messageRes = "Cập nhật thất bại"
      } else {
        isSuccessRes = true
        messageRes = "Cập nhật thành công"
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err edit order(admin-controller):", err)
      }
      messageRes = err
    }
    editEditRes = makeAdminRes(isSuccessRes, messageRes)
    return editEditRes
  },
  makeWhereAndBindsSearchTransactions: (entryData) => {
    /*
      NOTE:
      - inputData: {
        status: '1',
        from_date: '2023-03-30 00:00:00',
        to_date: '2023-04-02 23:59:59',
      }
      - outputData: {
        where_data: {...},
        binds_data: {...}
      }
    */
    let dataRes = {}
      , whereData = {}
      , bindsData = {}
      , statusInputData = entryData.status
      , fromDateInputData = entryData.from_date
      , toDateInputData = entryData.to_date
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(statusInputData)) {
      whereData.status = 'status = ?'
      bindsData.status = parseInt(statusInputData)
    }
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(fromDateInputData)) {
      whereData.from_date = 'created_date >= ?'
      bindsData.from_date = `${moment(fromDateInputData).format(DATE_FORMAT)} 00:00:00`
    }
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(toDateInputData)) {
      whereData.to_date = 'created_date <= ?'
      bindsData.to_date = `${moment(toDateInputData).format(DATE_FORMAT)} 23:59:59`
    }
    dataRes = {
      where_data: whereData,
      binds_data: bindsData
    }
    return dataRes
  },
  searchTransactions: async (connectUsingTransaction, inputDataSea) => {
    /*
      NOTE:
      - inputData: {
        select_data: 'id, status', (string)
        where_data: {
          id: 123,
          status: 1,
          ...
        },
        start: 0,
        max_records: 5
      }
      - outData: {
        is_success: true,
        status_code: 200,
        count_all_records: 10,
        data: [...],
        message: ''
      }
    */
    let connDb
      , listTransaction = []
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , searchTransRes
      , countAllRecords = 0
    try {
      connDb = getConnDb(connectUsingTransaction)
      let selectInputData = inputDataSea.select_data
        , whereInputData = inputDataSea.where_data
        , bindsInputData = inputDataSea.binds_data
        , arrWhereQuery = []
        , strWhereQuery = ''
      let sqlGetTransaction
        , bindsDataTransaction = []
        , bindsCountTrans = []
      let statusInputDataWhere = whereInputData.status
        , statusInputDataBinds = bindsInputData.status
        , fromDateInputDataWhere = whereInputData.from_date
        , fromDateInputDataBinds = bindsInputData.from_date
        , toDateInputDataWhere = whereInputData.to_date
        , toDateInputDataBinds = bindsInputData.to_date
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(statusInputDataWhere)) {
        arrWhereQuery.push(statusInputDataWhere)
        bindsDataTransaction.push(statusInputDataBinds)
        bindsCountTrans.push(statusInputDataBinds)
      }
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(fromDateInputDataWhere)) {
        arrWhereQuery.push(fromDateInputDataWhere)
        bindsDataTransaction.push(fromDateInputDataBinds)
        bindsCountTrans.push(fromDateInputDataBinds)
      }
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(toDateInputDataWhere)) {
        arrWhereQuery.push(toDateInputDataWhere)
        bindsDataTransaction.push(toDateInputDataBinds)
        bindsCountTrans.push(toDateInputDataBinds)
      }
      if (arrWhereQuery.length > 0) {
        strWhereQuery = arrWhereQuery.join(' and ')
        if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(strWhereQuery)) {
          strWhereQuery = `where ${strWhereQuery}`
        }
      }
      sqlGetTransaction = `select ${selectInputData} from ${NAME_SCHEMA}.transaction ${strWhereQuery} limit ? offset ?`
      bindsDataTransaction.push(inputDataSea.max_records)
      bindsDataTransaction.push(inputDataSea.start)
      let resGetTransaction = await connDb.query(sqlGetTransaction, bindsDataTransaction)
      listTransaction = resGetTransaction[0]
      let sqlCountTrans = `select count(*) countTransaction from ${NAME_SCHEMA}.transaction ${strWhereQuery}`
        , resCountTrans = await connDb.query(sqlCountTrans, bindsCountTrans)
      countAllRecords = resCountTrans[0][0].countTransaction
      isSuccessRes = true
      statusCodeRes = STATUS_CODE_RES.OK_REQ
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err search transaction(admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      data: listTransaction,
      status_code: statusCodeRes,
      count_all_records: countAllRecords
    }
    searchTransRes = makeAdminRes(isSuccessRes, messageRes, options)
    return searchTransRes
  },
  searchOrder: async (connectUsingTransaction, idOrder) => {
    /*
      NOTE:
      - inputData: idOrder
      - outputData: {
        success: {
          data: {
            ...
          }
        },
        error: {
          mess: 'Không tìm thấy đơn hàng'
        }
      }
    */
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , getOrderRes
      , infoOrder
    try {
      let sqlGetOrder = `select sum, vat, total, data, status from ${NAME_SCHEMA}.order where id=? limit 1`
        , bindsDataOrder = [idOrder]
      connDb = getConnDb(connectUsingTransaction)
      let resGetOrder = await connDb.query(sqlGetOrder, bindsDataOrder)
      infoOrder = resGetOrder[0][0]
      isSuccessRes = true
      statusCodeRes = STATUS_CODE_RES.OK_REQ
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err get order(admin-controller):", err)
      }
      messageRes = err.message
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      data: infoOrder,
      status_code: statusCodeRes
    }
    getOrderRes = makeAdminResSuccErr(isSuccessRes, messageRes, options)
    return getOrderRes
  },
  generateQr2FA: async (connectUsingTransaction, username, idAcc) => {
    let connDb
      , isSuccessRes = false
      , qrCodeHtml
      , messageRes = ''
      , statusCodeRes
    try {
      connDb = getConnDb(connectUsingTransaction)
      const STATUS_2FA_ACCOUNT_MANAGE = defineCode.STATUS_2FA_ACCOUNT_MANAGE
        , secret = speakeasy.generateSecret({ name: `Clothes_e_commerce (${username})` })
        , otpAuthUrl = secret.otpauth_url
        , base32 = secret.base32
      let generateQrCodeImageHTML = (otpAuthUrl) => {
        return new Promise(async function(resolve, reject) {
          qrcode.toDataURL(otpAuthUrl, (err, url) => {
            if (err) {
              console.log("**Generate qrcode image fail:", err)
              statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
              reject()
            } else {
              // NOTE: url is base64 image
              resolve(url)
            }
          })
        })
      }
      qrCodeHtml = await generateQrCodeImageHTML(otpAuthUrl)
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(qrCodeHtml)) {
        // STEP: Update base32_2fa and status_2fa = 1 to account_manage table
        let sqlUpdAccManage = `update ${NAME_SCHEMA}.account_manage set base32_2fa=?, status_2fa=? where id=?`
          , bindsDataUpdAccManage = [
            base32,
            STATUS_2FA_ACCOUNT_MANAGE.ACTIVE,
            idAcc
          ]
        let resUpdAccManage
          , recordAccManage
        resUpdAccManage = await connDb.query(sqlUpdAccManage, bindsDataUpdAccManage)
        recordAccManage = resUpdAccManage[0].affectedRows
        if (recordAccManage <= 0) {
          messageRes = "Cập nhật qr code thất bại"
          statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
        } else {
          isSuccessRes = true
          statusCodeRes = STATUS_CODE_RES.OK_REQ
        }
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err generate qr-code 2FA (admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      qr_html: qrCodeHtml,
      status_code: statusCodeRes
    }
    let resDataApi = makeAdminRes(isSuccessRes, messageRes, options)
    return resDataApi
  },
  verifyOTP2FA: async (connectUsingTransaction, otp2FA, idAcc) => {
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , token = ''
      , infoAccAdmin = null
    try {
      connDb = getConnDb(connectUsingTransaction)
      const sqlGetAccManage = `select id, username, fullname, tel, status_2fa, base32_2fa from ${NAME_SCHEMA}.account_manage where id=? limit 1`
        , bindsDataGetAccManage = [idAcc]
      let resGetAccManage
        , infoAccManage
      resGetAccManage = await connDb.query(sqlGetAccManage, bindsDataGetAccManage)
      infoAccManage = resGetAccManage[0][0]
      let dataBase32 = infoAccManage.base32_2fa
        , isVerifyOTP
      isVerifyOTP = speakeasy.totp.verify({ secret: dataBase32, encoding: 'base32', token: otp2FA, window: 0 })
      if (isVerifyOTP) {
        isSuccessRes = true
        messageRes = "Mã OTP xác thực hợp lệ"
        statusCodeRes = STATUS_CODE_RES.OK_REQ
        let dataAccPushToToken = {
          id: infoAccManage.id,
          username: infoAccManage.username,
          full_name: infoAccManage.fullname,
          tel: infoAccManage.tel,
          status_2fa: infoAccManage.status_2fa
        }
        token = await util.createToken(dataAccPushToToken)
        infoAccAdmin = dataAccPushToToken
      } else {
        messageRes = "Mã OTP xác thực không hợp lệ. Vui lòng kiểm tra lại mã OTP"
        statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err verify otp 2FA (admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      token,
      info_acc: infoAccAdmin,
      status_code: statusCodeRes
    }
    let resDataApi = makeAdminRes(isSuccessRes, messageRes, options)
    return resDataApi
  },
  removeQr2FA: async (connectUsingTransaction, idAcc) => {
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , token = ''
      , infoAccAdmin = null
    try {
      connDb = getConnDb(connectUsingTransaction)
      const DEFAULT_BASE32_2FA = null
        ,  STATUS_2FA_ACCOUNT_MANAGE = defineCode.STATUS_2FA_ACCOUNT_MANAGE
      const sqlUpdAccManage = `update ${NAME_SCHEMA}.account_manage set base32_2fa=?, status_2fa=? where id=?`
        , bindsDataUpdAccManage = [
          DEFAULT_BASE32_2FA,
          STATUS_2FA_ACCOUNT_MANAGE.INACTIVE,
          idAcc
        ]
      let resUpdAccManage
        , recordUpdAccManage
      resUpdAccManage = await connDb.query(sqlUpdAccManage, bindsDataUpdAccManage)
      recordUpdAccManage = resUpdAccManage[0].affectedRows
      const sqlGetAccManage = `select id, username, fullname, tel, status_2fa from ${NAME_SCHEMA}.account_manage where id=? limit 1`
        , bindsDataGetAccManage = [idAcc]
      let resGetAccManage
        , infoAccManage
      resGetAccManage = await connDb.query(sqlGetAccManage, bindsDataGetAccManage)
      infoAccManage = resGetAccManage[0][0]
      if (recordUpdAccManage <= 0) {
        messageRes = "Xảy ra lỗi. Vui lòng thử lại sau"
        statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
      } else {
        messageRes = "Bỏ xác thực 2 thành phần thành công"
        statusCodeRes = STATUS_CODE_RES.OK_REQ
        isSuccessRes = true
        let dataAccPushToToken = {
          id: infoAccManage.id,
          username: infoAccManage.username,
          full_name: infoAccManage.fullname,
          tel: infoAccManage.tel,
          status_2fa: infoAccManage.status_2fa
        }
        token = await util.createToken(dataAccPushToToken)
        infoAccAdmin = dataAccPushToToken
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err remove QR code 2FA (admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      token,
      info_acc: infoAccAdmin,
      status_code: statusCodeRes
    }
    let resDataApi = makeAdminRes(isSuccessRes, messageRes, options)
    return resDataApi
  },
  updateAccount: async (connectUsingTransaction, inputDataEdit) => {
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , updAccRes
      , token = ''
      , infoAccAdmin = null
    try {
      connDb = getConnDb(connectUsingTransaction)
      let sqlUpdAccount
        , bindsDataAccAdmin = []
        , arrDataUpdCondition = []
        , strDataUpdCondition = ''
      // STEP: Add data update into update query
      let newPwd = inputDataEdit.password
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newPwd)) {
        newPwd = util.encryptMd5Password(newPwd)
        arrDataUpdCondition.push('password=?')
        bindsDataAccAdmin.push(newPwd)
      }
      let newFullName = inputDataEdit.full_name
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newFullName)) {
        arrDataUpdCondition.push('fullname=?')
        bindsDataAccAdmin.push(newFullName)
      }
      let newTel = inputDataEdit.tel
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newTel)) {
        arrDataUpdCondition.push('tel=?')
        bindsDataAccAdmin.push(newTel)
      }
      // STEP: Add data where into update query
      bindsDataAccAdmin.push(inputDataEdit.id)
      strDataUpdCondition = arrDataUpdCondition.join(',')
      sqlUpdAccount = `update ${NAME_SCHEMA}.account_manage set ${strDataUpdCondition} where id=?`
      let resUpdAdmin
        , recordUpdAccAdmin
      resUpdAdmin = await connDb.query(sqlUpdAccount, bindsDataAccAdmin)
      recordUpdAccAdmin = resUpdAdmin[0].affectedRows
      if (recordUpdAccAdmin <= 0) {
        messageRes = "Cập nhật thất bại"
      } else {
        isSuccessRes = true
        messageRes = "Cập nhật thành công"
        let sqlSelAccount = `select id, username, fullname, tel from ${NAME_SCHEMA}.account_manage where id=? limit 1`
          , bindsSelAccount = [inputDataEdit.id]
          , resSelAccount = await connDb.query(sqlSelAccount, bindsSelAccount)
          , dataAcc = resSelAccount[0][0]
          , dataAccPushToToken = {
            id: dataAcc.id,
            username: dataAcc.username,
            full_name: dataAcc.fullname,
            tel: dataAcc.tel,
          }
        const SECRET = defineCode.SECRET
          , EXPIRES = defineCode.EXPIRES
        let createToken = () => {
          return new Promise(async function(resolve, reject) {
            jwt.sign(dataAccPushToToken, SECRET, { expiresIn: EXPIRES }, (err, token) => {
              if (err) { reject(err) }
              resolve(token)
            })
          })
        }
        token = await createToken()
        infoAccAdmin = dataAccPushToToken
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err update account(admin-controller):", err)
      }
      messageRes = err
    }
    let options = {
      token,
      info_acc: infoAccAdmin,
    }
    updAccRes = makeAdminRes(isSuccessRes, messageRes, options)
    return updAccRes
  },
  totalMoneyStats: async (connectUsingTransaction) => {
    /*
      NOTE:
      - inputData: {}
      - outData: {
        status_code: 200,
        stats: 2000,
      }
    */
    let connDb
      , listData = []
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , statsRes
      , statsMoney = 0
    try {
      connDb = getConnDb(connectUsingTransaction)
      let sqlSumMoneyInTransaction
      sqlSumMoneyInTransaction = `select sum(amount) sum_amount from ${NAME_SCHEMA}.transaction`
      let resSumMoneyInTransaction = await connDb.query(sqlSumMoneyInTransaction)
      listData = resSumMoneyInTransaction[0]
      statsMoney = listData[0].sum_amount
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(statsMoney)) {
        statsMoney = parseFloat(statsMoney)
      }
      isSuccessRes = true
      statusCodeRes = STATUS_CODE_RES.OK_REQ
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err total money statistic(admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      stats: statsMoney,
      status_code: statusCodeRes
    }
    statsRes = makeAdminRes(isSuccessRes, messageRes, options)
    return statsRes
  },
  totalOrdersStats: async (connectUsingTransaction) => {
    /*
      NOTE:
      - inputData: {}
      - outData: {
        status_code: 200,
        stats: 2000,
      }
    */
    let connDb
      , listData = []
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , statsRes
      , statsCountOrders = 0
    try {
      connDb = getConnDb(connectUsingTransaction)
      let sqlCountOrders
      sqlCountOrders = `select count(*) count_order from ${NAME_SCHEMA}.order`
      let resCountOrders = await connDb.query(sqlCountOrders)
      listData = resCountOrders[0]
      statsCountOrders = listData[0].count_order
      isSuccessRes = true
      statusCodeRes = STATUS_CODE_RES.OK_REQ
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err total orders statistic(admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      stats: statsCountOrders,
      status_code: statusCodeRes
    }
    statsRes = makeAdminRes(isSuccessRes, messageRes, options)
    return statsRes
  },
  totalProductsStats: async (connectUsingTransaction) => {
    /*
      NOTE:
      - inputData: {}
      - outData: {
        status_code: 200,
        stats: 2000,
      }
    */
    let connDb
      , listData = []
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , statsRes
      , statsCountProducts = 0
    try {
      connDb = getConnDb(connectUsingTransaction)
      let sqlCountProducts
      sqlCountProducts = `select count(*) count_product from ${NAME_SCHEMA}.product`
      let resCountProducts = await connDb.query(sqlCountProducts)
      listData = resCountProducts[0]
      statsCountProducts = listData[0].count_product
      isSuccessRes = true
      statusCodeRes = STATUS_CODE_RES.OK_REQ
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err total products statistic(admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      stats: statsCountProducts,
      status_code: statusCodeRes
    }
    statsRes = makeAdminRes(isSuccessRes, messageRes, options)
    return statsRes
  },
  totalCustomersStats: async (connectUsingTransaction) => {
    /*
      NOTE:
      - inputData: {}
      - outData: {
        status_code: 200,
        stats: 2000,
      }
    */
    let connDb
      , listData = []
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , statsRes
      , statsCountAccCustomers = 0
    try {
      connDb = getConnDb(connectUsingTransaction)
      let sqlCountAccCustomers
      sqlCountAccCustomers = `select count(*) count_acc_customer from ${NAME_SCHEMA}.account_customer`
      let resCountAccCustomers = await connDb.query(sqlCountAccCustomers)
      listData = resCountAccCustomers[0]
      statsCountAccCustomers = listData[0].count_acc_customer
      isSuccessRes = true
      statusCodeRes = STATUS_CODE_RES.OK_REQ
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err total customers statistic(admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      stats: statsCountAccCustomers,
      status_code: statusCodeRes
    }
    statsRes = makeAdminRes(isSuccessRes, messageRes, options)
    return statsRes
  },
  revenueStats: async (connectUsingTransaction, inputDataStats) => {
    /*
      NOTE:
      - inputData: {
        type: six_months | one_year
      }
      - outData: {
        status_code: 200,
        labels: [
          'thứ 2', 'thứ 3', ...
        ],
        data: [0, 10000, 0]
      }
    */
    let connDb
      , listData = []
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , statsRes
      , labels = []
      , dataOfLabels = []
    try {
      connDb = getConnDb(connectUsingTransaction)
      let typeStats = inputDataStats.type
        , sqlRevenueOnTransaction
        , resRevenueOnTransaction
        , list12Month = [
          'T1', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'T8', 'T9', 'T10', 'T11', 'T12'
        ]
        , date
        , listMonthInQuery = []
        , strListMonthInQuery = ''
      switch (typeStats) {case 'six_months':
          let listLast6Month = []
          date = new Date()
          // NOTE: set ngày 01 mỗi tháng
          date.setDate(1)
          for (let i=1; i<=6; i++) {
            let monthName = list12Month[date.getMonth()] + ' ' + date.getFullYear()
              , strMonthInQuery = `SELECT ${date.getMonth()+1} AS MONTH`
            listLast6Month.push(monthName)
            listMonthInQuery.push(strMonthInQuery)
            date.setMonth(date.getMonth(monthName) - 1)
          }
          strListMonthInQuery = listMonthInQuery.join(' UNION ')
          sqlRevenueOnTransaction = `SELECT meses.month, IFNULL(SUM(stats.amount), 0) sum_amount
          FROM
          (${strListMonthInQuery}) as meses
          LEFT JOIN ${NAME_SCHEMA}.transaction stats ON meses.month = MONTH(stats.created_date)
          GROUP BY meses.month`
          resRevenueOnTransaction = await connDb.query(sqlRevenueOnTransaction)
          listData = resRevenueOnTransaction[0]
          labels = listLast6Month
          dataOfLabels = listData.map(dataStats => dataStats.sum_amount)
          isSuccessRes = true
          statusCodeRes = STATUS_CODE_RES.OK_REQ
          break
        case 'one_year':
          let listLast12Month = []
          date = new Date()
          // NOTE: set ngày 01 mỗi tháng
          date.setDate(1)
          for (let i=1; i<=12; i++) {
            let monthName = list12Month[date.getMonth()] + ' ' + date.getFullYear()
              , strMonthInQuery = `SELECT ${date.getMonth()+1} AS MONTH`
            listLast12Month.push(monthName)
            listMonthInQuery.push(strMonthInQuery)
            date.setMonth(date.getMonth(monthName) - 1)
          }
          strListMonthInQuery = listMonthInQuery.join(' UNION ')
          sqlRevenueOnTransaction = `SELECT meses.month, IFNULL(SUM(stats.amount), 0) sum_amount
          FROM
          (${strListMonthInQuery}) as meses
          LEFT JOIN ${NAME_SCHEMA}.transaction stats ON meses.month = MONTH(stats.created_date)
          GROUP BY meses.month`
          resRevenueOnTransaction = await connDb.query(sqlRevenueOnTransaction)
          listData = resRevenueOnTransaction[0]
          labels = listLast12Month
          dataOfLabels = listData.map(dataStats => dataStats.sum_amount)
          isSuccessRes = true
          statusCodeRes = STATUS_CODE_RES.OK_REQ
          break
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err total customers statistic(admin-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      labels: labels,
      data: dataOfLabels,
      status_code: statusCodeRes
    }
    statsRes = makeAdminRes(isSuccessRes, messageRes, options)
    return statsRes
  },
}

module.exports = adminController
