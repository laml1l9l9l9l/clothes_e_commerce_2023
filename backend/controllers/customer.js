'use strict'

const jwt = require("jsonwebtoken")
const TRANSLATOR = require("../utils/translator")
const util = require("../utils/util")
const config = require("../utils/config")
const defineCode = require("../utils/define_code")
const stripe = require("stripe")(
  "sk_test_51HPvU9DFg5koCdLGeOEiFvwHat4v8eMjX6SY0YCwxPBQBUPhKy1fPVhiSM5cQtgW7QBG9ydQcXnW57TDxVE2f3H000HSfmEQZF"
)

const IS_LOG_IN_SERVICE = config.is_log_in_service
  , ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ = defineCode.ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ
  , NAME_SCHEMA = defineCode.NAME_SCHEMA
  , STATUS_PRODUCT = defineCode.STATUS_PRODUCT
  , STATUS_CODE_RES = defineCode.STATUS_CODE_RES
  , STATUS_ACCOUNT_CUSTOMER = defineCode.STATUS_ACCOUNT_CUSTOMER

const getConnDb = util.getConnDb
const makeCustomerRes = util.makeCustomerRes
const makeDataResApi = (messInput, isSucc, lang = null, options = {}) => {
  let dataResApi = {
      success: {},
      error: {}
    }
    , messageObj = {}
    , valResMess

  if (typeof messInput == "string") {
    messageObj.message = messInput
  } else if (typeof messInput == "obj") {
    messageObj = messInput
  }

  if (lang) {
    // NOTE: Get value massage from language
    valResMess = TRANSLATOR.translate(messageObj.message, lang)
  } else {
    // NOTE: Get value massage with vietnamese translation
    valResMess = messageObj.message
  }
  if (isSucc) {
    let dataSuccess = {}
    for (const [key, value] of Object.entries(options)) {
      switch (key) {
        case "token":
          dataSuccess["token"] = value
          break
      }
    }
    dataResApi['success'] = dataSuccess
  } else {
    let dataErr = {}
    dataErr["message"] = valResMess
    for (const [key, value] of Object.entries(options)) {
      switch (key) {
        case "message_from_exception":
          dataSuccess["message_from_exception"] = value
          break
      }
    }
    dataResApi['error'] = dataErr
  }
  return dataResApi
}

const customerController = {
  login: async (connectUsingTransaction, infoAuth) => {
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , token = ''
      , infoAccCustomer = null
    connDb = getConnDb(connectUsingTransaction)
    try {
      const sqlGetInfoCusAcc = `select count(*) countAcc, id, username, fullname, tel, email, address, btax from ${NAME_SCHEMA}.account_customer where username = ? and password = ? and status = ?`
      let convertMd5Pwd = ''
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(infoAuth.password)) {
        convertMd5Pwd = util.encryptMd5Password(infoAuth.password)
      }
      let bindsDataGetCusAcc = [
        infoAuth.username,
        convertMd5Pwd,
        STATUS_ACCOUNT_CUSTOMER.ACTIVE
      ]
      let resGetUser = await connDb.query(sqlGetInfoCusAcc, bindsDataGetCusAcc)
      let recordGetUser = resGetUser[0]
      if (recordGetUser && recordGetUser[0] && recordGetUser[0].countAcc == 1) {
        isSuccessRes = true
        messageRes = 'Logged in successfully'
        statusCodeRes = STATUS_CODE_RES.OK_REQ
        let dataAcc = recordGetUser[0]
          , dataAccPushToToken = {
            id: dataAcc.id,
            username: dataAcc.username,
            full_name: dataAcc.fullname,
            tel: dataAcc.tel,
            email: dataAcc.email,
            address: dataAcc.address,
            btax: dataAcc.btax,
          }
        const SECRET = defineCode.SECRET
          , EXPIRES = defineCode.EXPIRES
        let createToken = () => {
          return new Promise(async function(resolve, reject) {
            jwt.sign(dataAccPushToToken, SECRET, { expiresIn: EXPIRES }, (err, token) => {
              if (err) { reject(err) }
              resolve(token)
            })
          })
        }
        token = await createToken()
        infoAccCustomer = dataAccPushToToken
      } else {
        messageRes = 'Login failed. Please enter account and password'
        statusCodeRes = STATUS_CODE_RES.INVALID_USR
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err login(customer-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      token,
      info_acc: infoAccCustomer,
      status_code: statusCodeRes
    }
    let resDataApi = makeCustomerRes(isSuccessRes, messageRes, options)
    return resDataApi
  },
  decodeToken: async (token) => {
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , dataDecodeToken = null
    connDb = getConnDb(connectUsingTransaction)
    try {
      dataDecodeToken = jwt.decode(token)
      if (dataDecodeToken && util.isNotEmptyObj(dataDecodeToken)) {
        isSuccessRes = true
        messageRes = 'Decode token successfully'
        statusCodeRes = STATUS_CODE_RES.OK_REQ
      } else {
        messageRes = 'Please enter account and password'
        statusCodeRes = STATUS_CODE_RES.INVALID_USR
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err decode token(customer-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      decode_token: dataDecodeToken,
      status_code: statusCodeRes
    }
    let resDataApi = makeCustomerRes(isSuccessRes, messageRes, options)
    return resDataApi
  },
  makeDataResApi,
  searchProducts: async (connectUsingTransaction, inputDataSea) => {
    /*
      NOTE:
      - inputData: {
        selectData: 'id, doc', (string)
        whereData: {
          name: 'Ao so',
          ...
        },
        start: 0,
        max_records: 5
      }
      - outData: {
        is_success: true,
        status_code: 200,
        data: [...],
        message: ''
      }
    */
    let connDb
      , listProduct = []
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      , searchProdRes
      , countAllRecords = 0
    try {
      connDb = getConnDb(connectUsingTransaction)
      let selectInputData = inputDataSea.select_data
        , whereInputData = inputDataSea.where_data
        , bindsInputData = inputDataSea.binds_data
        , arrWhereQuery = []
        , strWhereQuery = ''
      let sqlGetProduct
        , bindsDataProduct = []
        , bindsCountProducts = []
      let nameInputDataWhere = whereInputData.name
        , nameInputDataBinds = bindsInputData.name
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(nameInputDataWhere)) {
        arrWhereQuery.push(nameInputDataWhere)
        bindsDataProduct.push(nameInputDataBinds)
        bindsCountProducts.push(nameInputDataBinds)
      }
      /*
        NOTE: (Disable nút) Không được thêm sản phẩm hết hàng, ko bán vào giỏ hàng
      */
      arrWhereQuery.push('status in (?)')
      bindsDataProduct.push([STATUS_PRODUCT.INACTIVE, STATUS_PRODUCT.ACTIVE])
      bindsCountProducts.push([STATUS_PRODUCT.INACTIVE, STATUS_PRODUCT.ACTIVE])
      if (arrWhereQuery.length > 0) {
        strWhereQuery = arrWhereQuery.join(' and ')
        if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(strWhereQuery)) {
          strWhereQuery = `where ${strWhereQuery}`
        }
      }
      sqlGetProduct = `select ${selectInputData} from ${NAME_SCHEMA}.product ${strWhereQuery} limit ? offset ?`
      bindsDataProduct.push(inputDataSea.max_records)
      bindsDataProduct.push(inputDataSea.start)
      let resGetProduct = await connDb.query(sqlGetProduct, bindsDataProduct)
      listProduct = resGetProduct[0]
      let sqlCountProducts = `select count(*) countProduct from ${NAME_SCHEMA}.product ${strWhereQuery}`
        , resCountProducts = await connDb.query(sqlCountProducts, bindsCountProducts)
      countAllRecords = resCountProducts[0][0].countProduct
      isSuccessRes = true
      statusCodeRes = STATUS_CODE_RES.OK_REQ
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err search product(customer-controller):", err)
      }
      messageRes = err.message
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      data: listProduct,
      status_code: statusCodeRes,
      count_all_records: countAllRecords
    }
    searchProdRes = makeCustomerRes(isSuccessRes, messageRes, options)
    return searchProdRes
  },
  makeWhereAndBindsSearchProducts: (entryData) => {
    /*
      NOTE:
      - inputData: {
        status: '1',
        name: 'Ao so',
        from_date: '2023-03-30 00:00:00',
        to_date: '2023-04-02 23:59:59',
        type: '1',
      }
      - outputData: {
        where_data: {...},
        binds_data: {...}
      }
    */
    let dataRes = {}
      , whereData = {}
      , bindsData = {}
      , nameInputData = entryData.name
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(nameInputData)) {
      whereData.name = 'name like ?'
      bindsData.name = `%${nameInputData}%`
    }
    dataRes = {
      where_data: whereData,
      binds_data: bindsData
    }
    return dataRes
  },
  checkoutBasket: async (connectUsingTransaction, bodyData) => {
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
    try {
      let infoAcc = bodyData.info_acc
        , dataBasket = bodyData.basket
        , sumOrder = bodyData.sum
        , vatOrder = bodyData.vat
        , totalOrder = bodyData.total
        , statusTransaction = bodyData.status
        , infoPayment = bodyData.info_payment
      connDb = getConnDb(connectUsingTransaction)
      const sqlInsTransaction = `insert into ${NAME_SCHEMA}.transaction set ?`
        , bindsDataTransaction = {
          status: statusTransaction,
          user_id: infoAcc.id,
          user_name: infoAcc.username,
          user_full_name: infoAcc.full_name,
          user_address: infoAcc.address,
          user_phone: infoAcc.tel,
          amount: totalOrder,
          payment_id: infoPayment.id,
          payment_created: infoPayment.created
        }
      let resInsTransaction
        , recordInsTransaction
      resInsTransaction = await connDb.query(sqlInsTransaction, bindsDataTransaction)
      recordInsTransaction = resInsTransaction[0].affectedRows
      let messFailToPaymentTransaction = 'Payment failed. Please try again later'
      if (recordInsTransaction <= 0) {
        messageRes = messFailToPaymentTransaction
      } else {
        let idTransaction = resInsTransaction[0].insertId
        const STATUS_ORDER = defineCode.STATUS_ORDER
          , DEFAULT_STATUS_ORDER = STATUS_ORDER.UNCONFIRMED
          , sqlInsOrder = `insert into ${NAME_SCHEMA}.order set ?`
          , bindsDataOrder = {
            transaction_id: idTransaction,
            sum: sumOrder,
            vat: vatOrder,
            total: totalOrder,
            data: JSON.stringify(dataBasket),
            status: DEFAULT_STATUS_ORDER
          }
        let resInsOrder
          , recordInsOrder
        resInsOrder = await connDb.query(sqlInsOrder, bindsDataOrder)
        recordInsOrder = resInsOrder[0].affectedRows
        if (recordInsOrder <= 0) {
          messageRes = messFailToPaymentTransaction
          statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
        } else {
          for (let i = 0; i < dataBasket.length; i++) {
            let itemProdInBasket = dataBasket[i]
              , sqlUpdProduct = `update ${NAME_SCHEMA}.product set quantity_in_warehouse=quantity_in_warehouse-? where id=?`
              , bindsDataUpdProduct = [
                itemProdInBasket.quantity,
                itemProdInBasket.id
              ]
              , resUpdProduct = await connDb.query(sqlUpdProduct, bindsDataUpdProduct)
              , recordUpdProduct = resUpdProduct[0].affectedRows
            if (recordUpdProduct <= 0) {
              messageRes = messFailToPaymentTransaction
              statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
              break
            }
          }
          if (!messageRes) {
            // CASE: Payment successful
            isSuccessRes = true
            messageRes = "Payment successful"
            statusCodeRes = STATUS_CODE_RES.OK_REQ
          }
        }
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err checkout(customer-controller):", err)
      }
      messageRes = err.message
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      status_code: statusCodeRes
    }
    let resDataApi = makeCustomerRes(isSuccessRes, messageRes, options)
    return resDataApi
  },
  searchOrders: async (connectUsingTransaction, inputDataSea) => {
    /*
      NOTE:
      - inputData: {
        selectData: 'id, doc', (string)
        whereData: {
          id: 1,
          ...
        },
        start: 0,
        max_records: 5
      }
      - outData: {
        is_success: true,
        status_code: 200,
        data: [...],
        message: ''
      }
    */
    let connDb
      , listOrder = []
      , isSuccessRes = false
      , messageRes = ''
      , searchProdRes
      , statusCodeRes
    try {
      connDb = getConnDb(connectUsingTransaction)
      let selectInputData = inputDataSea.select_data
        , whereInputData = inputDataSea.where_data
        , bindsInputData = inputDataSea.binds_data
        , arrWhereQuery = []
        , strWhereQuery = ''
      let sqlGetOrder
        , bindsDataProduct = []
      let userIdInputDataWhere = whereInputData.trans_user_id
        , userIdInputDataBinds = bindsInputData.trans_user_id
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(userIdInputDataWhere)) {
        arrWhereQuery.push(userIdInputDataWhere)
        bindsDataProduct.push(userIdInputDataBinds)
      }
      if (arrWhereQuery.length > 0) {
        strWhereQuery = arrWhereQuery.join(' and ')
        if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(strWhereQuery)) {
          strWhereQuery = `where ${strWhereQuery}`
        }
      }
      sqlGetOrder = `select ${selectInputData} from ${NAME_SCHEMA}.transaction trans join ${NAME_SCHEMA}.order orders on trans.id = orders.transaction_id ${strWhereQuery} limit ? offset ?`
      bindsDataProduct.push(inputDataSea.max_records)
      bindsDataProduct.push(inputDataSea.start)
      let resGetProduct = await connDb.query(sqlGetOrder, bindsDataProduct)
      listOrder = resGetProduct[0]
      isSuccessRes = true
      statusCodeRes = STATUS_CODE_RES.OK_REQ
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err search order(customer-controller):", err)
      }
      messageRes = err
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      status_code: statusCodeRes,
      data: listOrder
    }
    searchProdRes = makeCustomerRes(isSuccessRes, messageRes, options)
    return searchProdRes
  },
  makeWhereAndBindsSearchOrders: (entryData) => {
    /*
      NOTE:
      - inputData: {
        id_acc: 1
      }
      - outputData: {
        where_data: {...},
        binds_data: {...}
      }
    */
    let dataRes = {}
      , whereData = {}
      , bindsData = {}
      , userId = entryData.id_acc
    if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(userId)) {
      whereData.trans_user_id = 'trans.user_id = ?'
      bindsData.trans_user_id = parseInt(userId)
    }
    dataRes = {
      where_data: whereData,
      binds_data: bindsData
    }
    return dataRes
  },
  register: async (connectUsingTransaction, infoAuth) => {
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
    connDb = getConnDb(connectUsingTransaction)
    try {
      const sqlInsCusAcc = `insert into ${NAME_SCHEMA}.account_customer set ?`
      let convertMd5Pwd = ''
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(infoAuth.password)) {
        convertMd5Pwd = util.encryptMd5Password(infoAuth.password)
      }
      let bindsDataInsCusAcc = {
        username: infoAuth.username,
        password: convertMd5Pwd,
        fullname: infoAuth.full_name,
        status: STATUS_ACCOUNT_CUSTOMER.ACTIVE
      }
      let resInsCusAcc = await connDb.query(sqlInsCusAcc, bindsDataInsCusAcc)
      let recordInsCusAcc = resInsCusAcc[0].affectedRows
      if (recordInsCusAcc > 0) {
        isSuccessRes = true
        messageRes = 'Sign up successfully'
        statusCodeRes = STATUS_CODE_RES.OK_REQ
      } else {
        messageRes = 'Sign up failed. Please try again'
        statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err register(customer-controller):", err)
      }
      messageRes = err.message
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      status_code: statusCodeRes
    }
    let resDataApi = makeCustomerRes(isSuccessRes, messageRes, options)
    return resDataApi
  },
  updateAccount: async (connectUsingTransaction, inputDataEdit) => {
    /*
      NOTE:
      - inputData: {
        id: 1,
        password: 'ad12323123', (default: '')
        full_name: 'Nguyen Tu A',
        tel: '01231210',
        email: 'sdfshb@gmail.com',
        address: 'So 12, ...',
        btax: '23489834',
        status: 1
      }
      - outData: {
        is_success: false,
        message: '',
        token: '',
        info_acc: {...},
      }
    */
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , updAccRes
      , token = ''
      , infoAccCustomer = null
    try {
      connDb = getConnDb(connectUsingTransaction)
      let sqlUpdAccount
        , bindsDataCustomer = []
        , arrDataUpdCondition = []
        , strDataUpdCondition = ''
      // STEP: Add data update into update query
      let newPwd = inputDataEdit.password
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newPwd)) {
        newPwd = util.encryptMd5Password(newPwd)
        arrDataUpdCondition.push('password=?')
        bindsDataCustomer.push(newPwd)
      }
      let newFullName = inputDataEdit.full_name
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newFullName)) {
        arrDataUpdCondition.push('fullname=?')
        bindsDataCustomer.push(newFullName)
      }
      let newTel = inputDataEdit.tel
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newTel)) {
        arrDataUpdCondition.push('tel=?')
        bindsDataCustomer.push(newTel)
      }
      let newEmail = inputDataEdit.email
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newEmail)) {
        arrDataUpdCondition.push('email=?')
        bindsDataCustomer.push(newEmail)
      }
      let newAddress = inputDataEdit.address
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newAddress)) {
        arrDataUpdCondition.push('address=?')
        bindsDataCustomer.push(newAddress)
      }
      let newTaxCode = inputDataEdit.btax
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newTaxCode)) {
        arrDataUpdCondition.push('btax=?')
        bindsDataCustomer.push(newTaxCode)
      }
      let newStatus = inputDataEdit.status
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(newStatus)) {
        arrDataUpdCondition.push('status=?')
        bindsDataCustomer.push(newStatus)
      }
      // STEP: Add data where into update query
      bindsDataCustomer.push(inputDataEdit.id)
      strDataUpdCondition = arrDataUpdCondition.join(',')
      sqlUpdAccount = `update ${NAME_SCHEMA}.account_customer set ${strDataUpdCondition} where id=?`
      let resUpdCustomer
        , recordUpdCustomer
      resUpdCustomer = await connDb.query(sqlUpdAccount, bindsDataCustomer)
      recordUpdCustomer = resUpdCustomer[0].affectedRows
      if (recordUpdCustomer <= 0) {
        messageRes = "Update account failed"
      } else {
        isSuccessRes = true
        messageRes = "Update account successfully"
        let sqlSelAccount = `select id, username, fullname, tel, email, address, btax from ${NAME_SCHEMA}.account_customer where id=? limit 1`
          , bindsSelAccount = [inputDataEdit.id]
          , resSelAccount = await connDb.query(sqlSelAccount, bindsSelAccount)
          , dataAcc = resSelAccount[0][0]
          , dataAccPushToToken = {
            id: dataAcc.id,
            username: dataAcc.username,
            full_name: dataAcc.fullname,
            tel: dataAcc.tel,
            email: dataAcc.email,
            address: dataAcc.address,
            btax: dataAcc.btax,
          }
        const SECRET = defineCode.SECRET
          , EXPIRES = defineCode.EXPIRES
        let createToken = () => {
          return new Promise(async function(resolve, reject) {
            jwt.sign(dataAccPushToToken, SECRET, { expiresIn: EXPIRES }, (err, token) => {
              if (err) { reject(err) }
              resolve(token)
            })
          })
        }
        token = await createToken()
        infoAccCustomer = dataAccPushToToken
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err update account(customer-controller):", err)
      }
      messageRes = err
    }
    let options = {
      token,
      info_acc: infoAccCustomer,
    }
    updAccRes = makeCustomerRes(isSuccessRes, messageRes, options)
    return updAccRes
  },
  paymentsCreate: async (bodyData) => {
    let isSuccessRes = false
      , messageRes = ''
      , clientSecret = ''
      , statusCodeRes
    try {
      let currOrder = bodyData.curr
        , totalOrder = bodyData.total
      messageRes = "Created payment successful"
      const paymentIntent = await stripe.paymentIntents.create({
        amount: totalOrder, // subunits of the currency
        currency: currOrder,
      })
      clientSecret = paymentIntent.client_secret
      statusCodeRes = STATUS_CODE_RES.OK_REQ
      isSuccessRes = true
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err created payments(customer-controller):", err)
      }
      messageRes = 'Created payment failed. Please try again later'
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      client_secret: clientSecret,
      status_code: statusCodeRes
    }
    let resDataApi = makeCustomerRes(isSuccessRes, messageRes, options)
    return resDataApi
  },
  validDataBasket: async (connectUsingTransaction, bodyData) => {
    let connDb
      , isSuccessRes = false
      , messageRes = ''
      , statusCodeRes
      connDb = getConnDb(connectUsingTransaction)
    try {
      let infoAcc = bodyData.info_acc
        , basket = bodyData.basket
      if (ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(infoAcc.address) || ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(infoAcc.tel)) {
        messageRes = "Address and phone number are required. Please update account information"
        statusCodeRes = STATUS_CODE_RES.BAD_REQ
      } else {
        // STEP: Get list data product
        let listIdProdInBasket = basket.map(prod => prod.id)
          , sqlSelProds = `select id, status, quantity_in_warehouse quantity from ${NAME_SCHEMA}.product where id in (?)`
          , bindsSelProds = [listIdProdInBasket]
          , resSelProds = await connDb.query(sqlSelProds, bindsSelProds)
          , listProduct = resSelProds[0]
        for (let i = 0; i < basket.length; i++) {
          let itemProdInBasket = basket[i]
            , itemProdInDB = listProduct.find((prod) => prod.id === itemProdInBasket.id)
          if (itemProdInDB) {
            if (itemProdInDB.status != STATUS_PRODUCT.ACTIVE) {
              messageRes = `Product ${itemProdInBasket.title} not activated`
              statusCodeRes = STATUS_CODE_RES.BAD_REQ
            } else if (itemProdInDB.quantity < itemProdInBasket.quantity) {
              messageRes = "Exceed the quantity in the warehouse"
              statusCodeRes = STATUS_CODE_RES.BAD_REQ
            }
          } else {
            // CASE: Không tìm thấy id sản phẩm trong db
            messageRes = `No product name ${itemProdInBasket.title} could be found`
            statusCodeRes = STATUS_CODE_RES.BAD_REQ
          }
          if (messageRes) {
            // NOTE: Occur error -> break loop
            break
          }
        }
        if (!messageRes) {
          // CASE: Validation success data in basket
          isSuccessRes = true
          messageRes = "Validation success"
          statusCodeRes = STATUS_CODE_RES.OK_REQ
        }
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) {
        console.log("**Err valid data basket(customer-controller):", err)
      }
      messageRes = err.message
      statusCodeRes = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    }
    let options = {
      status_code: statusCodeRes
    }
    let resDataApi = makeCustomerRes(isSuccessRes, messageRes, options)
    return resDataApi
  }
}

module.exports = customerController
