"use strict"
const dictionary = require("./dictionary_locals")
const Service = {
  translate: (code, lang = "vi") => {
    let result = ""
    let _dictionaryObj = dictionary[code]
    if (_dictionaryObj) {
      result = _dictionaryObj[lang]
    }
    return result || code
  }
}

module.exports = Service
