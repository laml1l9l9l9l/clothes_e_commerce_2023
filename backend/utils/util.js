'use strict'
const crypto = require("crypto")
const jwt = require("jsonwebtoken")
const dbs = require('./dbs')
const defineCode = require('./define_code')
const ARR_DATA_EMPTY_VAL = [null, undefined, '']
  , SECRET = defineCode.SECRET
  , EXPIRES = defineCode.EXPIRES

const getConnDb = (connectUsingTransaction) => {
  let connDb = dbs
    , arrDataEmptyKeyObj = [null, undefined, '']
  if (!arrDataEmptyKeyObj.includes(connectUsingTransaction)) {
    connDb = connectUsingTransaction
  }
  return connDb
}
const createNewConnDb = () => {
  let connDb = dbs
  return connDb
}
const getTransactionDb = async (connectUsingTransaction) => {
  let connDb
    , arrDataEmptyKeyObj = [null, undefined, '']
  if (!arrDataEmptyKeyObj.includes(connectUsingTransaction)) {
    connDb = connectUsingTransaction
  } else {
    connDb = await dbs.getConnection()
    await connDb.beginTransaction()
  }
  return connDb
}
const makeAdminRes =  (isSuccess, mess, options = {}) => {
  let dataRes = {}
  dataRes["is_success"] = isSuccess
  dataRes["message"] = mess
  for (const [key, value] of Object.entries(options)) {
    switch (key) {
      case "data":
        dataRes["data"] = value
        break
      case "status_code":
        dataRes["status_code"] = value
        break
      case "token":
        dataRes["token"] = value
        break
      case "is_verify":
        dataRes["is_verify"] = value
        break
      case "info_acc":
        dataRes["info_acc"] = value
        break
      case "count_all_records":
        dataRes["count_all_records"] = value
        break
      case "qr_html":
        dataRes["qr_html"] = value
        break
      case "stats":
        dataRes["stats"] = value
        break
      case "labels":
        dataRes["labels"] = value
        break
    }
  }
  return dataRes
}
const makeAdminResSuccErr =  (isSuccess, mess, options = {}) => {
  let dataRes = {
      success: {},
      error: {}
    }
    , childRes = {}
  if (isSuccess) {
    childRes["message"] = mess
    for (const [key, value] of Object.entries(options)) {
      switch (key) {
        case "data":
          childRes["data"] = value
          break
        case "status_code":
          childRes["status_code"] = value
          break
        case "token":
          childRes["token"] = value
          break
        case "qr_html":
          childRes["qr_html"] = value
          break
        case "info_acc":
          childRes["info_acc"] = value
          break
      }
    }
    dataRes["success"] = childRes
  } else {
    childRes["message"] = mess
    for (const [key, value] of Object.entries(options)) {
      switch (key) {
        case "status_code":
          childRes["status_code"] = value
          break
      }
    }
    dataRes["error"] = childRes
  }
  return dataRes
}
const makeCustomerRes =  (isSuccess, mess, options = {}) => {
  let dataRes = {}
  dataRes["is_success"] = isSuccess
  dataRes["message"] = mess
  for (const [key, value] of Object.entries(options)) {
    switch (key) {
      case "data":
        dataRes["data"] = value
        break
      case "count_all_records":
        dataRes["count_all_records"] = value
        break
      case "status_code":
        dataRes["status_code"] = value
        break
      case "token":
        dataRes["token"] = value
        break
      case "is_verify":
        dataRes["is_verify"] = value
        break
      case "decoded_token":
        dataRes["decode_token"] = value
        break
      case "info_acc":
        dataRes["info_acc"] = value
        break
      case "client_secret":
        dataRes["client_secret"] = value
        break
    }
  }
  return dataRes
}
const makeCustomerResSuccErr =  (isSuccess, mess, options = {}) => {
  let dataRes = {
      success: {},
      error: {}
    }
    , childRes = {}
  if (isSuccess) {
    childRes["message"] = mess
    for (const [key, value] of Object.entries(options)) {
      switch (key) {
        case "data":
          childRes["data"] = value
          break
        case "status_code":
          childRes["status_code"] = value
          break
        case "token":
          childRes["token"] = value
          break
        case "decode_token":
          childRes["decode_token"] = value
          break
        case "info_acc":
          childRes["info_acc"] = value
          break
        case "client_secret":
          childRes["client_secret"] = value
          break
      }
    }
    dataRes["success"] = childRes
  } else {
    childRes["message"] = mess
    for (const [key, value] of Object.entries(options)) {
      switch (key) {
        case "status_code":
          childRes["status_code"] = value
          break
      }
    }
    dataRes["error"] = childRes
  }
  return dataRes
}
const isNotEmptyObj = (obj) => {
  let isNotEmptyObj = false
    , arrDataEmptyKeyObj = ARR_DATA_EMPTY_VAL
  if (!arrDataEmptyKeyObj.includes(obj) && Object.keys(obj).length !== 0 && Object.getPrototypeOf(obj) === Object.prototype) {
    isNotEmptyObj = true
  }
  return isNotEmptyObj
}
const encryptMd5Password = (passwordOri) => {
  let passwordEncrypt
  passwordEncrypt = crypto.createHash('md5').update(passwordOri).digest('hex')
  return passwordEncrypt
}
const isEmptyObj = (obj) => {
  let isEmptyObj = false
  if (!ARR_DATA_EMPTY_VAL.includes(obj) && Object.keys(obj).length === 0 && typeof obj === "object") {
    isEmptyObj = true
  }
  return isEmptyObj
}
const makeStartRecordQuery = (currPagination) => {
  let startRecordQuery = currPagination - 1
  if (startRecordQuery < 0) { startRecordQuery = 0 }
  const MAX_RECORD = defineCode.MAX_RECORD
  startRecordQuery = startRecordQuery * MAX_RECORD
  return startRecordQuery
}
const makeListPagination = (currPagination, countAllRecords) => {
  let listPagination = []
    , numPagination = 1
  const MAX_ITEMS_IN_LIST_PAGING = 3
    , MAX_RECORD = defineCode.MAX_RECORD
  numPagination = Math.ceil(countAllRecords/MAX_RECORD)
  if (numPagination < MAX_ITEMS_IN_LIST_PAGING) {
    // CASE: bản ghi <= 15
    for (let i = 1; i <= numPagination; i++) {
      listPagination.push(i)
    }
  } else if (numPagination == MAX_ITEMS_IN_LIST_PAGING) {
    // CASE: bản ghi = 15
    listPagination = [1, 2, 3]
  } else {
    // CASE: bản ghi > 15
    if (currPagination == numPagination) {
      // CASE: Trang hiện tại được chọn là trang cuối trong danh sách
      listPagination = [currPagination - 2, currPagination - 1, currPagination]
    } else {
      if (currPagination == 1) {
        // CASE: Nếu trang hiện tại được chọn là trang 1
        listPagination = [1, 2, 3]
      } else if (currPagination > numPagination) {
        // CASE: Nếu trang hiện tại được chọn > tổng số trang hiện có
        listPagination = [numPagination - 2, numPagination - 1, numPagination]
      } else {
        let previousCurrPagination = currPagination - 1
          , nextCurrPagination = currPagination + 1
        listPagination = [previousCurrPagination, currPagination, nextCurrPagination]
      }
    }
  }
  return listPagination
}
let createToken = (dataAccPushToToken) => {
  return new Promise(async function(resolve, reject) {
    jwt.sign(dataAccPushToToken, SECRET, { expiresIn: EXPIRES }, (err, token) => {
      if (err) { reject(err) }
      resolve(token)
    })
  })
}

const Util = {
  arr_data_empty_val: ARR_DATA_EMPTY_VAL,
  getConnDb,
  createNewConnDb,
  getTransactionDb,
  makeAdminRes,
  makeAdminResSuccErr,
  isNotEmptyObj,
  isEmptyObj,
  encryptMd5Password,
  makeCustomerRes,
  makeCustomerResSuccErr,
  makeStartRecordQuery,
  makeListPagination,
  createToken,
}

module.exports = Util