'use strict'

const dictionary = {
  "infoLoginCusIsEmpty": {
    vi: "Thiếu thông tin đăng nhập tài khoản",
    en: "Username and password are required",
  },
  "Fail to get info icr in s_inv": {
    vi: "Không tìm thấy bản ghi",
    en: "Get ICR info error",
  },
}

module.exports = dictionary
