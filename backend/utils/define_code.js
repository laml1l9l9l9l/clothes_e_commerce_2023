"use strict"

const Data = {
  STATUS_CODE_RES: {
    OK_REQ: 200,
    BAD_REQ: 400,
    INTERNAL_SERVER_ERR: 500,
    NOT_IMPLEMENTED: 501,
    INVALID_USR: 401,
    FORBIDDEN_REQ: 403,
    TOO_MANY_REQS: 429,
  },
  LANG: {
    EN: 'en',
    VI: 'vi',
  },
  ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ: [null, undefined, ''],
  NAME_SCHEMA: 'clothes_e_commerce',
  STATUS_ACCOUNT_MANAGE: {
    INACTIVE: 0,
    ACTIVE: 1
  },
  STATUS_ACCOUNT_CUSTOMER: {
    INACTIVE: 0,
    ACTIVE: 1
  },
  STATUS_PRODUCT: {
    INACTIVE: 0, // 'Không bán'
    ACTIVE: 1, // 'Đang bán'
    CANCEL: 2 // 'Đã hủy'
  },
  STATUS_TRANSACTION: {
    UNPAID: 0, // 'Chưa thanh toán'
    PAID: 1, // 'Đã thanh toán'
  },
  STATUS_ORDER: {
    UNCONFIRMED: 0, // 'Đơn hàng chưa xác nhận'
    CONFIRMED: 1, // 'Đơn hàng đã xác nhận'
    PREPARING: 2, // 'Đang chuẩn bị hàng'
    DELIVERING: 3, // 'Đang vận chuyển'
    RECEIVED: 4, // 'Đã nhận'
  },
  SECRET: 'admin#0986301253AbC&$(SeCrEt)$$$#@',
  EXPIRES: 86400,
  MAX_RECORD: 5,
  STATUS_2FA_ACCOUNT_MANAGE: {
    INACTIVE: 0,
    ACTIVE: 1
  },
}

module.exports = Data
