'use strict'

const IS_LOG_IN_SERVICE = true
const PORT = 3030
const POOL_CONFIG_DB = {
  user: "root",
  password: "admin@123",
  host: "localhost",
  port: 3360,
  connectionLimit: 10
}
const DATE_TIME_FORMAT = "YYYY-MM-DD HH:mm:ss"
const DATE_FORMAT = "YYYY-MM-DD"

module.exports = {
  port: PORT,
  poolConfig: POOL_CONFIG_DB,
  is_log_in_service: IS_LOG_IN_SERVICE,
  date_time_format: DATE_TIME_FORMAT,
  date_format: DATE_FORMAT,
}
