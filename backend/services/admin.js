'use strict'
const adminController = require('../controllers/admin')
const config = require('../utils/config')
const dbs = require('../utils/dbs')
const defineCode = require('../utils/define_code')
const util = require('../utils/util')

const makeAdminResSuccErr = util.makeAdminResSuccErr

const IS_LOG_IN_SERVICE = config.is_log_in_service
  , STATUS_CODE = defineCode.STATUS_CODE_RES

const adminService = {
  login: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        username: 'abc_xyz',
        password: '123456',
      }
      - outputData: {
        success: {
          token: 'dsjfhsdjkfkj',
          info_acc: {
            id: 1,
            username: 'custom',
            full_name: 'Nguyen Van Yeah',
            tel: '123456',
            email: 'custom@gmail.com',
          },
          message: 'Đăng nhập thành công'
        },
        error: {
          token: '',
          info_acc: null,
          message: 'Đăng nhập thất bại. Vui lòng kiểm tra lại tài khoản và mật khẩu'
        }
      }
    */
    let infoAuth = req.body
      , statusCode
      , resBodyApi
      , messageRes = ''
      , conn
      , isSuccessRes = false
      , token = ''
      , infoAccAdmin = null
    try {
      let resLogin
      conn = dbs
      resLogin = await adminController.login(conn, infoAuth)
      if (resLogin.is_success) {
        statusCode = resLogin.status_code
        messageRes = resLogin.message
        token = resLogin.token
        infoAccAdmin = resLogin.info_acc
        isSuccessRes = true
      } else {
        statusCode = resLogin.status_code
        messageRes = resLogin.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err login api(service-admin):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      let options = {
        token,
        info_acc: infoAccAdmin
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  verify: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        token: 'dsjfhsdjkfkj',
      }
      - outputData: {
        success: {
          decode_token: {
            username: 'admin',
            fullname: 'Admin ABC',
            tel: '012312399'
          },
          message: ''
        },
        error: {
          message: 'Tài khoản không hợp lệ. Vui lòng đăng nhập'
        }
      }
    */
    let body = req.body
      , token = body.token
      , statusCode
      , resBodyApi
      , decodeToken
      , messageRes = ''
      , isSuccessRes = false
    try {
      let resVerify
      resVerify = await adminController.verify(token)
      if (resVerify.is_success) {
        if (resVerify.is_verify) {
          decodeToken = resVerify.decoded_token
          isSuccessRes = true
        } else {
          statusCode = resVerify.status_code
          messageRes = 'Tài khoản không hợp lệ. Vui lòng đăng nhập'
        }
      } else {
        statusCode = resVerify.status_code
        messageRes = resVerify.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err verify api(service-admin):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      let options = {
        decode_token: decodeToken
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  searchProducts: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        status: 1,
        name: 'Ao so',
        from_date: '2023-03-30 00:00:00',
        to_date: '2023-04-02 23:59:59',
        type: 1,
        curr_pagination: 1
      }
      - outputData: {
        data: [{...}, {...}],
        list_pagination: [1,2,3],
        curr_pagination: 1
      }
    */
    let entryData = req.query
      , statusCode
      , resBodyApi
      , conn
      , listProduct = []
      , listPagination = [1]
      , currPagination = entryData.curr_pagination || 1
    try {
      conn = dbs
      currPagination = parseInt(currPagination)
      let selectDataProduct = 'id, name, price, type, status, quantity_in_warehouse'
        , whereDataProduct = {}
        , bindsDataProduct = {}
        , makeWhereAndBindsSearchProductsRes
      makeWhereAndBindsSearchProductsRes = adminController.makeWhereAndBindsSearchProducts(entryData)
      whereDataProduct = makeWhereAndBindsSearchProductsRes.where_data
      bindsDataProduct = makeWhereAndBindsSearchProductsRes.binds_data
      let start = util.makeStartRecordQuery(currPagination)
        , maxRecords = defineCode.MAX_RECORD // 5
      let inputDataSearch = {
        select_data: selectDataProduct,
        where_data: whereDataProduct,
        binds_data: bindsDataProduct,
        start,
        max_records: maxRecords
      }
      let resSeaProduct
      resSeaProduct = await adminController.searchProducts(conn, inputDataSearch)
      listProduct = resSeaProduct.data
      statusCode = resSeaProduct.status_code
      listPagination = util.makeListPagination(currPagination, resSeaProduct.count_all_records)
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err search product api(service-admin):`, err) }
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      resBodyApi = {
        data: listProduct,
        list_pagination: listPagination,
        curr_pagination: currPagination
      }
      res.status(statusCode).send(resBodyApi)
    }
  },
  addProduct: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        name: '',
        price: 1000,
        discount: 100,
        quantity_in_warehouse: 10,
        status: 1,
        type: 1,
        description: '',
        picture: 'sdfjhjgfhjsdfg23' // base64 encoded
      }
      - outputData: {
        success: {
          message: 'Thêm mới thành công'
        },
        error: {
          message: 'Thêm mới thất bại'
        }
      }
    */
    let body = req.body
      , statusCode
      , messageRes = ''
      , resBodyApi
      , conn
      , addProdRes
      , isSuccessRes = false
      , isCommitTrans = false
    try {
      let inputDataAddProduct = body
      conn = await dbs.getConnection()
      await conn.beginTransaction()
      addProdRes = await adminController.addProduct(conn, inputDataAddProduct)
      if (addProdRes.is_success) {
        statusCode = STATUS_CODE.OK_REQ
        messageRes = addProdRes.message
        isSuccessRes = true
        isCommitTrans = true
      } else {
        statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
        messageRes = addProdRes.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err add product api(service-admin):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      if (conn) {
        if (isCommitTrans) {
          await conn.commit()
        } else {
          await conn.rollback()
        }
        conn.release()
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes)
      res.status(statusCode).send(resBodyApi)
    }
  },
  editProduct: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        id: 1,
        name: '',
        status: 1,
        type: 1,
        price: 1000,
        discount: 100,
        quantity_in_warehouse: 10,
        description: '',
        picture: 'sdfjhjgfhjsdfg23' // base64 encoded
      }
      - outputData: {
        success: {
          message: 'Cập nhật thành công'
        },
        error: {
          message: 'Cập nhật thất bại'
        }
      }
    */
    let body = req.body
      , statusCode
      , messageRes = ''
      , resBodyApi
      , conn
      , editProdRes
      , isSuccessRes = false
      , isCommitTrans = false
    try {
      let inputDataEditProduct = body
      conn = await dbs.getConnection()
      await conn.beginTransaction()
      editProdRes = await adminController.editProduct(conn, inputDataEditProduct)
      if (editProdRes.is_success) {
        statusCode = STATUS_CODE.OK_REQ
        messageRes = editProdRes.message
        isSuccessRes = true
        isCommitTrans = true
      } else {
        statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
        messageRes = editProdRes.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err edit product api(service-admin):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      if (conn) {
        if (isCommitTrans) {
          await conn.commit()
        } else {
          await conn.rollback()
        }
        conn.release()
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes)
      res.status(statusCode).send(resBodyApi)
    }
  },
  deleteProduct: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        id: 1,
      }
      - outputData: {
        success: {
          message: 'Xóa thành công'
        },
        error: {
          message: 'Xóa thất bại'
        }
      }
    */
    let body = req.body
      , statusCode
      , messageRes = ''
      , resBodyApi
      , conn
      , delProdRes
      , isCommitTrans = false
      , isSuccessRes = false
    try {
      let inputDataDelProduct = body
      conn = await dbs.getConnection()
      await conn.beginTransaction()
      delProdRes = await adminController.delProduct(conn, inputDataDelProduct)
      if (delProdRes.is_success) {
        statusCode = STATUS_CODE.OK_REQ
        messageRes = delProdRes.message
        isCommitTrans = true
        isSuccessRes = true
      } else {
        statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
        messageRes = delProdRes.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err delete product api(service-admin):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      if (conn) {
        if (isCommitTrans) {
          await conn.commit()
        } else {
          await conn.rollback()
        }
        conn.release()
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes)
      res.status(statusCode).send(resBodyApi)
    }
  },
  getProduct: async (req, res, next) => {
    /*
      NOTE:
      - inputData: req.params.id
      - outputData: {
        success: {
          data: {
            ...
          }
        },
        error: {
          message: 'Không tìm thấy sản phẩm'
        }
      }
    */
    let idProduct = req.params.id
      , statusCode
      , messageRes = ''
      , resBodyApi
      , conn
      , isSuccessRes = false
      , infoProduct
    try {
      conn = dbs
      let resSearchProduct
      resSearchProduct = await adminController.searchProduct(conn, idProduct)
      let childRes
      if (util.isNotEmptyObj(resSearchProduct.success)) {
        childRes = resSearchProduct.success
        statusCode = childRes.status_code
        infoProduct = childRes.data
        isSuccessRes = true
      } else {
        childRes = resSearchProduct.error
        statusCode = childRes.status_code
        messageRes = childRes.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err search product api(service-admin):`, err) }
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      let options = {
        data: infoProduct,
        status_code: statusCode
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  searchCustomers: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        status: 1,
        username: 'Ao so',
        from_date: '2023-03-30 00:00:00',
        to_date: '2023-04-02 23:59:59',
        curr_pagination: 1
      }
      - outputData: {
        data: [{...}, {...}],
        list_pagination: [1,2,3],
        curr_pagination: 1
      }
    */
    let entryData = req.query
      , statusCode
      , resBodyApi
      , conn
      , listAccCustomer = []
      , listPagination = [1]
      , currPagination = entryData.curr_pagination || 1
    try {
      conn = dbs
      currPagination = parseInt(currPagination)
      let selectDataProduct = 'id, username, status, created_date'
        , whereDataProduct = {}
        , bindsDataProduct = {}
        , makeWhereAndBindsSearchCustomersRes
      makeWhereAndBindsSearchCustomersRes = adminController.makeWhereAndBindsSearchCustomers(entryData)
      whereDataProduct = makeWhereAndBindsSearchCustomersRes.where_data
      bindsDataProduct = makeWhereAndBindsSearchCustomersRes.binds_data
      const start = util.makeStartRecordQuery(currPagination)
        , maxRecords = defineCode.MAX_RECORD
      let inputDataSearch = {
        select_data: selectDataProduct,
        where_data: whereDataProduct,
        binds_data: bindsDataProduct,
        start,
        max_records: maxRecords
      }
      let resSeaCustomer
      resSeaCustomer = await adminController.searchCustomers(conn, inputDataSearch)
      listAccCustomer = resSeaCustomer.data
      statusCode = resSeaCustomer.status_code
      listPagination = util.makeListPagination(currPagination, resSeaCustomer.count_all_records)
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err search customer api(service-admin):`, err) }
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      resBodyApi = {
        data: listAccCustomer,
        list_pagination: listPagination,
        curr_pagination: currPagination
      }
      res.status(statusCode).send(resBodyApi)
    }
  },
  addCustomer: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        username: 'abc_hd',
        password: 'ad12323123', (default: '')
        full_name: 'Nguyen Tu A',
        tel: '01231210',
        email: 'sdfshb@gmail.com',
        address: 'So 12, ...',
        btax: '23489834',
        status: 1
      }
      - outputData: {
        success: {
          message: 'Thêm mới thành công'
        },
        error: {
          message: 'Thêm mới thất bại'
        }
      }
    */
    let body = req.body
      , statusCode
      , messageRes = ''
      , resBodyApi
      , conn
      , addCustomerRes
      , isSuccessRes = false
      , isCommitTrans = false
    try {
      let inputDataAddCustomer = body
      conn = await dbs.getConnection()
      await conn.beginTransaction()
      addCustomerRes = await adminController.addCustomer(conn, inputDataAddCustomer)
      if (addCustomerRes.is_success) {
        statusCode = STATUS_CODE.OK_REQ
        messageRes = addCustomerRes.message
        isSuccessRes = true
        isCommitTrans = true
      } else {
        statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
        messageRes = addCustomerRes.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err add customer api(service-admin):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      if (conn) {
        if (isCommitTrans) {
          await conn.commit()
        } else {
          await conn.rollback()
        }
        conn.release()
      }
      let options = {
        status_code: statusCode
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  editCustomer: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        id: 1,
        password: 'ad12323123', (default: '')
        full_name: 'Nguyen Tu A',
        tel: '01231210',
        email: 'sdfshb@gmail.com',
        address: 'So 12, ...',
        btax: '23489834',
        status: 1
      }
      - outputData: {
        success: {
          message: 'Cập nhật thành công'
        },
        error: {
          message: 'Cập nhật thất bại'
        }
      }
    */
    let body = req.body
      , statusCode
      , messageRes = ''
      , resBodyApi
      , conn
      , editCusRes
      , isSuccessRes = false
      , isCommitTrans = false
    try {
      let inputDataEditCustomer = body
      conn = await dbs.getConnection()
      await conn.beginTransaction()
      editCusRes = await adminController.editCustomer(conn, inputDataEditCustomer)
      if (editCusRes.is_success) {
        statusCode = STATUS_CODE.OK_REQ
        messageRes = editCusRes.message
        isSuccessRes = true
        isCommitTrans = true
      } else {
        statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
        messageRes = editCusRes.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err edit customer api(service-admin):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      if (conn) {
        if (isCommitTrans) {
          await conn.commit()
        } else {
          await conn.rollback()
        }
        conn.release()
      }
      let options = {
        status_code: statusCode
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  deleteCustomer: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        id: 1,
      }
      - outputData: {
        success: {
          message: 'Xóa thành công'
        },
        error: {
          message: 'Xóa thất bại'
        }
      }
    */
    let body = req.body
      , statusCode
      , messageRes = ''
      , resBodyApi
      , conn
      , delCustomerRes
      , isCommitTrans = false
      , isSuccessRes = false
    try {
      let inputDataDelCustomer = body
      conn = await dbs.getConnection()
      await conn.beginTransaction()
      delCustomerRes = await adminController.delCustomer(conn, inputDataDelCustomer)
      if (delCustomerRes.is_success) {
        statusCode = STATUS_CODE.OK_REQ
        messageRes = delCustomerRes.message
        isCommitTrans = true
        isSuccessRes = true
      } else {
        statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
        messageRes = delCustomerRes.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err delete customer api(service-admin):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      if (conn) {
        if (isCommitTrans) {
          await conn.commit()
        } else {
          await conn.rollback()
        }
        conn.release()
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes)
      res.status(statusCode).send(resBodyApi)
    }
  },
  getCustomer: async (req, res, next) => {
    /*
      NOTE:
      - inputData: req.params.id
      - outputData: {
        success: {
          data: {
            ...
          }
        },
        error: {
          message: 'Không tìm thấy khách hàng'
        }
      }
    */
    let idCustomer = req.params.id
      , statusCode
      , messageRes = ''
      , resBodyApi
      , conn
      , isSuccessRes = false
      , infoCustomer
    try {
      conn = dbs
      let resSearchCustomer
      resSearchCustomer = await adminController.searchCustomer(conn, idCustomer)
      let childRes
      if (util.isNotEmptyObj(resSearchCustomer.success)) {
        childRes = resSearchCustomer.success
        statusCode = childRes.status_code
        infoCustomer = childRes.data
        isSuccessRes = true
      } else {
        childRes = resSearchCustomer.error
        statusCode = childRes.status_code
        messageRes = childRes.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err search customer api(service-admin):`, err) }
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      let options = {
        data: infoCustomer,
        status_code: statusCode
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  searchOrders: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        status: 1,
        from_date: '2023-03-30 00:00:00',
        to_date: '2023-04-02 23:59:59',
        curr_pagination: 1
      }
      - outputData: {
        data: [{...}, {...}],
        list_pagination: [1,2,3],
        curr_pagination: 1
      }
    */
    let entryData = req.query
      , statusCode
      , resBodyApi
      , conn
      , listOrder = []
      , listPagination = [1]
      , currPagination = entryData.curr_pagination || 1
    try {
      conn = dbs
      currPagination = parseInt(currPagination)
      let selectDataOrder = 'id, sum, vat, total, status, created_date'
        , whereDataOrder = {}
        , bindsDataOrder = {}
        , makeWhereAndBindsSearchOrdersRes
      makeWhereAndBindsSearchOrdersRes = adminController.makeWhereAndBindsSearchOrders(entryData)
      whereDataOrder = makeWhereAndBindsSearchOrdersRes.where_data
      bindsDataOrder = makeWhereAndBindsSearchOrdersRes.binds_data
      const start = util.makeStartRecordQuery(currPagination)
        , maxRecords = defineCode.MAX_RECORD
      let inputDataSearch = {
        select_data: selectDataOrder,
        where_data: whereDataOrder,
        binds_data: bindsDataOrder,
        start,
        max_records: maxRecords
      }
      let resSeaOrder
      resSeaOrder = await adminController.searchOrders(conn, inputDataSearch)
      listOrder = resSeaOrder.data
      statusCode = resSeaOrder.status_code
      listPagination = util.makeListPagination(currPagination, resSeaOrder.count_all_records)
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err search order api(service-admin):`, err) }
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      resBodyApi = {
        data: listOrder,
        list_pagination: listPagination,
        curr_pagination: currPagination
      }
      res.status(statusCode).send(resBodyApi)
    }
  },
  editOrder: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        id: 1,
        status: 1,
      }
      - outputData: {
        success: {
          message: 'Cập nhật thành công'
        },
        error: {
          message: 'Cập nhật thất bại'
        }
      }
    */
    let body = req.body
      , statusCode
      , messageRes = ''
      , resBodyApi
      , conn
      , editOrderRes
      , isSuccessRes = false
      , isCommitTrans = false
    try {
      let inputDataEditOrder = body
      conn = await dbs.getConnection()
      await conn.beginTransaction()
      editOrderRes = await adminController.editOrder(conn, inputDataEditOrder)
      if (editOrderRes.is_success) {
        statusCode = STATUS_CODE.OK_REQ
        messageRes = editOrderRes.message
        isSuccessRes = true
        isCommitTrans = true
      } else {
        statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
        messageRes = editOrderRes.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err edit order api(service-admin):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      if (conn) {
        if (isCommitTrans) {
          await conn.commit()
        } else {
          await conn.rollback()
        }
        conn.release()
      }
      let options = {
        status_code: statusCode
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  searchTransactions: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        status: 1,
        from_date: '2023-03-30 00:00:00',
        to_date: '2023-04-02 23:59:59',
        curr_pagination: 1
      }
      - outputData: {
        data: [{...}, {...}],
        list_pagination: [1,2,3],
        curr_pagination: 1
      }
    */
    let entryData = req.query
      , statusCode
      , resBodyApi
      , conn
      , listTransaction = []
      , listPagination = [1]
      , currPagination = entryData.curr_pagination || 1
    try {
      conn = dbs
      currPagination = parseInt(currPagination)
      let selectDataTransaction = 'id, user_name, user_full_name, amount, status, created_date'
        , whereDataTransaction = {}
        , bindsDataTransaction = {}
        , makeWhereAndBindsSearchTransactionRes
      makeWhereAndBindsSearchTransactionRes = adminController.makeWhereAndBindsSearchTransactions(entryData)
      whereDataTransaction = makeWhereAndBindsSearchTransactionRes.where_data
      bindsDataTransaction = makeWhereAndBindsSearchTransactionRes.binds_data
      const start = util.makeStartRecordQuery(currPagination)
        , maxRecords = defineCode.MAX_RECORD
      let inputDataSearch = {
        select_data: selectDataTransaction,
        where_data: whereDataTransaction,
        binds_data: bindsDataTransaction,
        start,
        max_records: maxRecords
      }
      let resSeaTransaction
      resSeaTransaction = await adminController.searchTransactions(conn, inputDataSearch)
      listTransaction = resSeaTransaction.data
      statusCode = resSeaTransaction.status_code
      listPagination = util.makeListPagination(currPagination, resSeaTransaction.count_all_records)
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err search transaction api(service-admin):`, err) }
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      resBodyApi = {
        data: listTransaction,
        list_pagination: listPagination,
        curr_pagination: currPagination
      }
      res.status(statusCode).send(resBodyApi)
    }
  },
  getOrder: async (req, res, next) => {
    /*
      NOTE:
      - inputData: req.params.id
      - outputData: {
        success: {
          data: {
            ...
          }
        },
        error: {
          message: 'Không tìm thấy đơn hàng'
        }
      }
    */
    let idOrder = req.params.id
      , statusCode
      , messageRes = ''
      , resBodyApi
      , conn
      , isSuccessRes = false
      , infoOrder
    try {
      conn = dbs
      let resSearchOrder
      resSearchOrder = await adminController.searchOrder(conn, idOrder)
      let childRes
      if (util.isNotEmptyObj(resSearchOrder.success)) {
        childRes = resSearchOrder.success
        statusCode = childRes.status_code
        infoOrder = childRes.data
        isSuccessRes = true
      } else {
        childRes = resSearchOrder.error
        statusCode = childRes.status_code
        messageRes = childRes.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err search order api(service-admin):`, err) }
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      let options = {
        data: infoOrder,
        status_code: statusCode
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  generateQr2FA: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        id: 1,
        username: 'admin123',
      }
      - outputData: {
        success: {
          qr_html: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMQAAADEC...',
          message: ''
        },
        error: {
          message: 'Xảy ra lỗi. Vui lòng thử lại sau'
        }
      }
    */
    /**
     * Các bước xử lý:
     * 1. Kiểm tra cột base32_2fa đã có giá trị -> case true generate qrcode html + update cột status_2fa = 1 và trả về
     * 2. Nếu base32_2fa == null -> tạo ra base32 + update cột base32_2fa, status_2fa = 1 + generate qrcode html và trả về
    */
    let body = req.body
      , idAcc = body.id
      , username = body.username
      , statusCode
      , resBodyApi
      , conn
      , qrCodeHTML
      , messageRes = ''
      , isSuccessRes = false
      , isCommitTrans = false
    try {
      let resGenerateQrCode
      conn = await dbs.getConnection()
      await conn.beginTransaction()
      resGenerateQrCode = await adminController.generateQr2FA(conn, username, idAcc)
      if (resGenerateQrCode.is_success) {
        if (resGenerateQrCode.qr_html) {
          qrCodeHTML = resGenerateQrCode.qr_html
          statusCode = resGenerateQrCode.status_code
          isSuccessRes = true
          isCommitTrans = true
        } else {
          statusCode = resGenerateQrCode.status_code
          messageRes = 'Xảy ra lỗi. Vui lòng thử lại sau'
        }
      } else {
        statusCode = resGenerateQrCode.status_code
        messageRes = resGenerateQrCode.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err generate qr code for 2FA api(service-admin):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      if (conn) {
        if (isCommitTrans) {
          await conn.commit()
        } else {
          await conn.rollback()
        }
        conn.release()
      }
      let options = {
        qr_html: qrCodeHTML
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  removeQr2FA: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        id: 1, // user's id
      }
      - outputData: {
        success: {
          token: 'dsjfhsdjkfkj',
          info_acc: {
            id: 1,
            username: 'custom',
            full_name: 'Nguyen Van Yeah',
            tel: '123456',
            email: 'custom@gmail.com',
          },
          message: 'Bỏ xác thực 2 thành phần thành công'
        },
        error: {
          token: '',
          info_acc: null,
          message: 'Xảy ra lỗi. Vui lòng thử lại sau'
        }
      }
    */
    /**
     * Các bước xử lý:
     * 1. Update cột base32_2fa = null, status_2fa = 0
    */
    let body = req.body
      , idAcc = body.id
      , statusCode
      , resBodyApi
      , conn
      , messageRes = ''
      , isSuccessRes = false
      , isCommitTrans = false
      , token = ''
      , infoAccAdmin = null
    try {
      let resRemoveQrCode2FA
      conn = await dbs.getConnection()
      await conn.beginTransaction()
      resRemoveQrCode2FA = await adminController.removeQr2FA(conn, idAcc)
      if (resRemoveQrCode2FA.is_success) {
        messageRes = resRemoveQrCode2FA.message
        statusCode = resRemoveQrCode2FA.status_code
        token = resRemoveQrCode2FA.token
        infoAccAdmin = resRemoveQrCode2FA.info_acc
        isSuccessRes = true
        isCommitTrans = true
      } else {
        statusCode = resRemoveQrCode2FA.status_code
        messageRes = resRemoveQrCode2FA.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err remove qr code for 2FA api(service-admin):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      if (conn) {
        if (isCommitTrans) {
          await conn.commit()
        } else {
          await conn.rollback()
        }
        conn.release()
      }
      let options = {
        token,
        info_acc: infoAccAdmin
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  verifyOTP2FA: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        id: 1, // user's id
        otp: '0415923'
      }
      - outputData: {
        success: {
          token: 'dsjfhsdjkfkj',
          info_acc: {
            id: 1,
            username: 'custom',
            full_name: 'Nguyen Van Yeah',
            tel: '123456',
            email: 'custom@gmail.com',
          },
          message: 'Mã OTP xác thực hợp lệ'
        },
        error: {
          token: '',
          info_acc: null,
          message: 'Mã OTP xác thực không hợp lệ. Vui lòng kiểm tra lại mã OTP'
        }
      }
    */
    /**
     * Các bước xử lý:
     * 1. Verify mã OTP
     * 2. Trả về thông báo
    */
    let body = req.body
      , idAcc = body.id
      , otp2FA = body.otp
      , statusCode
      , resBodyApi
      , conn
      , messageRes = ''
      , isSuccessRes = false
      , token = ''
      , infoAccAdmin = null
    try {
      let resVerifyOTP
      conn = dbs
      resVerifyOTP = await adminController.verifyOTP2FA(conn, otp2FA, idAcc)
      if (resVerifyOTP.is_success) {
        messageRes = resVerifyOTP.message
        statusCode = resVerifyOTP.status_code
        token = resVerifyOTP.token
        infoAccAdmin = resVerifyOTP.info_acc
        isSuccessRes = true
      } else {
        statusCode = resVerifyOTP.status_code
        messageRes = resVerifyOTP.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err verify otp for 2FA api(service-admin):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      let options = {
        token,
        info_acc: infoAccAdmin
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  updateAccount: async (req, res, next) => {
    let body = req.body
      , statusCode
      , messageRes = ''
      , resBodyApi
      , conn
      , token = ''
      , infoAccAdmin = null
      , updAccRes
      , isSuccessRes = false
      , isCommitTrans = false
    try {
      let inputDataUpdateAccount = body
      conn = await dbs.getConnection()
      await conn.beginTransaction()
      updAccRes = await adminController.updateAccount(conn, inputDataUpdateAccount)
      if (updAccRes.is_success) {
        statusCode = STATUS_CODE.OK_REQ
        messageRes = updAccRes.message
        token = updAccRes.token
        infoAccAdmin = updAccRes.info_acc
        isSuccessRes = true
        isCommitTrans = true
      } else {
        statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
        messageRes = updAccRes.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err update account api(service-admin):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      if (conn) {
        if (isCommitTrans) {
          await conn.commit()
        } else {
          await conn.rollback()
        }
        conn.release()
      }
      let options = {
        token,
        info_acc: infoAccAdmin
      }
      resBodyApi = makeAdminResSuccErr(isSuccessRes, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  totalMoneyStats: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {}
      - outputData: {
        stats: 20000
      }
    */
    let statusCode
      , resBodyApi
      , conn
      , stats = 0
    try {
      conn = dbs
      let resStats
      resStats = await adminController.totalMoneyStats(conn)
      stats = resStats.stats
      statusCode = resStats.status_code
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err total money statistic api(service-admin):`, err) }
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      resBodyApi = {
        stats: stats
      }
      res.status(statusCode).send(resBodyApi)
    }
  },
  totalOrdersStats: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {}
      - outputData: {
        stats: 20000
      }
    */
    let statusCode
      , resBodyApi
      , conn
      , stats = 0
    try {
      conn = dbs
      let resStats
      resStats = await adminController.totalOrdersStats(conn)
      stats = resStats.stats
      statusCode = resStats.status_code
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err total orders statistic api(service-admin):`, err) }
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      resBodyApi = {
        stats: stats
      }
      res.status(statusCode).send(resBodyApi)
    }
  },
  totalProductsStats: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {}
      - outputData: {
        stats: 20000
      }
    */
    let statusCode
      , resBodyApi
      , conn
      , stats = 0
    try {
      conn = dbs
      let resStats
      resStats = await adminController.totalProductsStats(conn)
      stats = resStats.stats
      statusCode = resStats.status_code
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err total products statistic api(service-admin):`, err) }
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      resBodyApi = {
        stats: stats
      }
      res.status(statusCode).send(resBodyApi)
    }
  },
  totalCustomersStats: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {}
      - outputData: {
        stats: 20000
      }
    */
    let statusCode
      , resBodyApi
      , conn
      , stats = 0
    try {
      conn = dbs
      let resStats
      resStats = await adminController.totalCustomersStats(conn)
      stats = resStats.stats
      statusCode = resStats.status_code
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err total customers statistic api(service-admin):`, err) }
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      resBodyApi = {
        stats: stats
      }
      res.status(statusCode).send(resBodyApi)
    }
  },
  revenueStats: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        type: six_months | one_year
      }
      - outputData: {
        labels: [
          'thứ 2', 'thứ 3', ...
        ],
        data: [0, 10000, 0]
      }
    */
    let entryData = req.query
      , statusCode
      , resBodyApi
      , conn
      , labels = []
      , dataOfLabels = []
    try {
      conn = dbs
      let resStats
      resStats = await adminController.revenueStats(conn, entryData)
      labels = resStats.labels
      dataOfLabels = resStats.data
      statusCode = resStats.status_code
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err revenue statistic api(service-admin):`, err) }
      statusCode = STATUS_CODE.INTERNAL_SERVER_ERR
    } finally {
      resBodyApi = {
        labels: labels,
        data: dataOfLabels
      }
      res.status(statusCode).send(resBodyApi)
    }
  },
}

module.exports = adminService
