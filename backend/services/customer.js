'use strict'
const customerController = require('../controllers/customer')
const cusValidateService = require('../validate-services/customer')
const defineCode = require('../utils/define_code')
const config = require('../utils/config')
const util = require('../utils/util')
const dbs = require("../utils/dbs")

const DEFAULT_LANG_MESS = defineCode.LANG.VI
  , STATUS_CODE_RES = defineCode.STATUS_CODE_RES
  , IS_LOG_IN_SERVICE = config.is_log_in_service

const makeDataResApi = customerController.makeDataResApi
const makeCustomerResSuccErr = util.makeCustomerResSuccErr

const customerService = {
  login: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        username: 'abc_xyz',
        password: '123456',
      }
      - outputData: {
        success: {
          token: 'dsjfhsdjkfkj',
          info_acc: {
            id: 1,
            username: 'custom',
            full_name: 'Nguyen Van Yeah',
            tel: '123456',
            email: 'custom@gmail.com',
          },
          message: 'Logged in successfully'
        },
        error: {
          token: '',
          info_acc: null,
          message: 'Login failed. Please enter account and password'
        }
      }
    */
    let body = req.body
      , statusCode
      , resBodyApi
      , isSuccess = false
      , messageRes = ''
      , conn
      , token = ''
      , infoAccCustomer = null
    try {
      let validInfoLogin
        , infoAuth
        , messLang = DEFAULT_LANG_MESS
      const arrDataEmptyKeyObj = util.arr_data_empty_val
      infoAuth = body
      if (!arrDataEmptyKeyObj.includes(body.lang)) { messLang = body.lang }
      // STEP: Validate entry data
      validInfoLogin = cusValidateService.validLogin(infoAuth)
      if (!validInfoLogin.isValid) {
        messageRes = validInfoLogin.mesValid
        statusCode = STATUS_CODE_RES.BAD_REQ
      } else {
        let resLoginCus
        conn = dbs
        resLoginCus = await customerController.login(conn, infoAuth)
        if (resLoginCus.is_success) {
          // STEP: Return token login
          statusCode = resLoginCus.status_code
          messageRes = resLoginCus.message
          token = resLoginCus.token
          infoAccCustomer = resLoginCus.info_acc
          isSuccess = true
        } else {
          statusCode = resLoginCus.status_code
          messageRes = resLoginCus.message
        }
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err login api(service-customer):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    } finally {
      let options = {
        token,
        info_acc: infoAccCustomer
      }
      resBodyApi = makeCustomerResSuccErr(isSuccess, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  verify: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        token: 'dsjfhsdjkfkj',
      }
      - outputData: {
        success: {
          message: 'Logged in successfully'
        },
        error: {
          message: 'Please enter account and password'
        }
      }
    */
    let body = req.body
      , statusCode
      , resBodyApi
      , isSuccess = false
      , messageRes = ''
      , conn
      , token = ''
    try {
      let validInfoLogin
        , infoAuth
        , messLang = DEFAULT_LANG_MESS
      const arrDataEmptyKeyObj = util.arr_data_empty_val
      infoAuth = body
      if (!arrDataEmptyKeyObj.includes(body.lang)) { messLang = body.lang }
      // STEP: Validate entry data
      validInfoLogin = cusValidateService.validLogin(infoAuth)
      if (!validInfoLogin.isValid) {
        messageRes = validInfoLogin.mesValid
        statusCode = STATUS_CODE_RES.BAD_REQ
      } else {
        let resLoginCus
        conn = dbs
        resLoginCus = await customerController.login(conn, infoAuth)
        if (resLoginCus.is_success) {
          // STEP: Return token login
          statusCode = resLoginCus.status_code
          messageRes = resLoginCus.message
          token = resLoginCus.token
          isSuccess = true
        } else {
          statusCode = resLoginCus.status_code
          messageRes = resLoginCus.message
        }
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err verify token api(service-customer):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    } finally {
      let options = {
        token
      }
      resBodyApi = makeCustomerResSuccErr(isSuccess, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  decodeToken: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        token: 'dsjfhsdjkfkj',
      }
      - outputData: {
        success: {
          decode_token: {
            username: 'admin',
            fullname: 'Admin ABC',
            tel: '012312399'
            ...
          },
          message: 'Decode token successfully'
        },
        error: {
          decode_token: null,
          message: 'Failed to decode token'
        }
      }
    */
    let body = req.body
      , statusCode
      , resBodyApi
      , isSuccess = false
      , messageRes = ''
      , dataDecodeToken = null
    try {
      let token
        , messLang = DEFAULT_LANG_MESS
      const arrDataEmptyKeyObj = util.arr_data_empty_val
      token = body.token
      if (!arrDataEmptyKeyObj.includes(body.lang)) { messLang = body.lang }
      // STEP: Validate entry data
      let messDecodeFailed = 'Failed to decode token'
      if (!token) {
        messageRes = messDecodeFailed
        statusCode = STATUS_CODE_RES.BAD_REQ
      } else {
        let resDecodeTokenCus
        resDecodeTokenCus = await customerController.decodeToken(token)
        if (resDecodeTokenCus.is_success) {
          // STEP: Return token login
          statusCode = resDecodeTokenCus.status_code
          messageRes = resDecodeTokenCus.message
          dataDecodeToken = resDecodeTokenCus.decode_token
          isSuccess = true
        } else {
          statusCode = resDecodeTokenCus.status_code
          messageRes = resDecodeTokenCus.message
        }
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err decode token api(service-customer):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    } finally {
      let options = {
        decode_token: dataDecodeToken
      }
      resBodyApi = makeCustomerResSuccErr(isSuccess, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  searchProducts: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        name: 'Ao so',
      }
      - outputData: [{...}, {...}]
    */
    let entryData = req.query
      , statusCode
      , resBodyApi
      , conn
      , listProduct = []
      , listPagination = [1]
      , currPagination = entryData.curr_pagination || 1
    try {
      conn = dbs
      currPagination = parseInt(currPagination)
      let selectDataProduct = 'id, name, price, type, status, quantity_in_warehouse, picture'
        , whereDataProduct = {}
        , bindsDataProduct = {}
        , makeWhereAndBindsSearchProductsRes
      makeWhereAndBindsSearchProductsRes = customerController.makeWhereAndBindsSearchProducts(entryData)
      whereDataProduct = makeWhereAndBindsSearchProductsRes.where_data
      bindsDataProduct = makeWhereAndBindsSearchProductsRes.binds_data
      const start = util.makeStartRecordQuery(currPagination)
        , maxRecords = defineCode.MAX_RECORD
      let inputDataSearch = {
        select_data: selectDataProduct,
        where_data: whereDataProduct,
        binds_data: bindsDataProduct,
        start,
        max_records: maxRecords
      }
      let resSeaProduct
      resSeaProduct = await customerController.searchProducts(conn, inputDataSearch)
      listProduct = resSeaProduct.data
      statusCode = resSeaProduct.status_code
      listPagination = util.makeListPagination(currPagination, resSeaProduct.count_all_records)
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err search product api(service-customer):`, err) }
      statusCode = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    } finally {
      resBodyApi = {
        data: listProduct,
        list_pagination: listPagination,
        curr_pagination: currPagination
      }
      res.status(statusCode).send(resBodyApi)
    }
  },
  checkoutBasket: async (req, res, next) => {
    /*
      NOTE:
      *** Description: Tạo đơn hàng và giao dịch, cập nhật số lượng sản phẩm
      - inputData: {
        info_acc: {
          id: 1,
          username: 'abc_xyz',
          email: 'abc@gmail.com',
          ...
        },
        basket: [{...}, {...}],
        info_payment: {
          id: 'pi_3N2zRqDFg5koCdLG1JtJeMS5',
          created: '1682957974'
        },
        sum: 100,
        vat: 10,
        total: 110,
        status: 1 // trạng thái giao dịch: 1 - đã thanh toán
      }
      - outputData: {
        success: {
          message: 'Payment successful'
        },
        error: {
          message: 'Payment failed. Please try again later'
        }
      }
      p/s: status bảng order default là 0 (chưa xác nhận)
    */
    let body = req.body
      , statusCode
      , resBodyApi
      , isSuccess = false
      , messageRes = ''
      , conn
      , isCommitTrans = false
    try {
      let resCheckoutBasket
      conn = await dbs.getConnection()
      await conn.beginTransaction()
      resCheckoutBasket = await customerController.checkoutBasket(conn, body)
      if (resCheckoutBasket.is_success) {
        // CASE: Successful
        isSuccess = true
        isCommitTrans = true
        statusCode = resCheckoutBasket.status_code
        messageRes = resCheckoutBasket.message
      } else {
        statusCode = resCheckoutBasket.status_code
        messageRes = resCheckoutBasket.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err checkout basket api(service-customer):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    } finally {
      if (conn) {
        if (isCommitTrans) {
          await conn.commit()
        } else {
          await conn.rollback()
        }
        conn.release()
      }
      resBodyApi = makeCustomerResSuccErr(isSuccess, messageRes)
      res.status(statusCode).send(resBodyApi)
    }
  },
  searchOrders: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        id_acc: 1
      }
      - outputData: [{...}, {...}]
    */
    let entryData = req.query
      , statusCode
      , resBodyApi
      , conn
      , listOrder = []
    try {
      conn = dbs
      let selectDataProduct = 'orders.id id, orders.sum sum, orders.vat vat, orders.total total, orders.data data, orders.status status, orders.created_date created_date'
        , whereDataProduct = {}
        , bindsDataProduct = {}
        , makeWhereAndBindsSearchProductsRes
      makeWhereAndBindsSearchProductsRes = customerController.makeWhereAndBindsSearchOrders(entryData)
      whereDataProduct = makeWhereAndBindsSearchProductsRes.where_data
      bindsDataProduct = makeWhereAndBindsSearchProductsRes.binds_data
      const start = 0
        , maxRecords = 5
      let inputDataSearch = {
        select_data: selectDataProduct,
        where_data: whereDataProduct,
        binds_data: bindsDataProduct,
        start,
        max_records: maxRecords
      }
      let resSeaOrder
      resSeaOrder = await customerController.searchOrders(conn, inputDataSearch)
      listOrder = resSeaOrder.data
      statusCode = resSeaOrder.status_code
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err search product api(service-customer):`, err) }
      statusCode = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    } finally {
      resBodyApi = listOrder
      res.status(statusCode).send(resBodyApi)
    }
  },
  register: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        username: 'abc_xyz',
        password: '123456',
        full_name: 'Nguyen Van A'
      }
      - outputData: {
        success: {
          message: 'Sign up successfully'
        },
        error: {
          message: 'Sign up failed. Please try again'
        }
      }
      p/s: redirect về màn hình login
    */
    let body = req.body
      , statusCode
      , resBodyApi
      , isSuccess = false
      , messageRes = ''
      , conn
      , isCommitTrans = false
    try {
        let infoAuth
        infoAuth = body
        conn = await dbs.getConnection()
        await conn.beginTransaction()
        let resRegisterCus
        resRegisterCus = await customerController.register(conn, infoAuth)
        if (resRegisterCus.is_success) {
          statusCode = resRegisterCus.status_code
          messageRes = resRegisterCus.message
          isSuccess = true
          isCommitTrans = true
        } else {
          statusCode = resRegisterCus.status_code
          messageRes = resRegisterCus.message
        }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err register api(service-customer):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    } finally {
      if (conn) {
        if (isCommitTrans) {
          await conn.commit()
        } else {
          await conn.rollback()
        }
        conn.release()
      }
      resBodyApi = makeCustomerResSuccErr(isSuccess, messageRes)
      res.status(statusCode).send(resBodyApi)
    }
  },
  updateAccount: async (req, res, next) => {
    /*
      NOTE:
      - inputData: {
        id: 1,
        password: 'ad12323123', (default: '')
        full_name: 'Nguyen Tu A',
        tel: '01231210',
        email: 'sdfshb@gmail.com',
        address: 'So 12, ...'
      }
      - outputData: {
        success: {
          token: 'dsjfhsdjkfkj',
          info_acc: {...},
          message: 'Update account successfully'
        },
        error: {
          token: '',
          message: 'Update account failed'
        }
      }
    */
    let body = req.body
    , statusCode
    , messageRes = ''
    , resBodyApi
    , conn
    , token = ''
    , infoAccCustomer = null
    , updAccRes
    , isSuccess = false
    , isCommitTrans = false
    try {
      let inputDataUpdateAccount = body
      conn = await dbs.getConnection()
      await conn.beginTransaction()
      updAccRes = await customerController.updateAccount(conn, inputDataUpdateAccount)
      if (updAccRes.is_success) {
        statusCode = STATUS_CODE_RES.OK_REQ
        messageRes = updAccRes.message
        token = updAccRes.token
        infoAccCustomer = updAccRes.info_acc
        isSuccess = true
        isCommitTrans = true
      } else {
        statusCode = STATUS_CODE_RES.INTERNAL_SERVER_ERR
        messageRes = updAccRes.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err update account api(service-customer):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    } finally {
      if (conn) {
        if (isCommitTrans) {
          await conn.commit()
        } else {
          await conn.rollback()
        }
        conn.release()
      }
      let options = {
        token,
        info_acc: infoAccCustomer,
        status_code: statusCode
      }
      resBodyApi = makeCustomerResSuccErr(isSuccess, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  paymentsCreate: async (req, res, next) => {
    /*
      NOTE:
      *** Description: Tạo mã secret cho giao dịch
      - inputData: {
        total: 110,
        curr: 'VND'
      }
      - outputData: {
        success: {
          client_secret: 'sdfsdfsdfsd',
          message: 'Created payment successful'
        },
        error: {
          client_secret: '',
          message: 'Created payment failed. Please try again later'
        }
      }
    */
    let body = req.body
      , statusCode
      , resBodyApi
      , isSuccess = false
      , messageRes = ''
      , clientSecret = ''
    try {
      let resPaymentsCreate
      resPaymentsCreate = await customerController.paymentsCreate(body)
      if (resPaymentsCreate.is_success) {
        // CASE: Successful
        isSuccess = true
        statusCode = resPaymentsCreate.status_code
        messageRes = resPaymentsCreate.message
        clientSecret = resPaymentsCreate.client_secret
      } else {
        statusCode = resPaymentsCreate.status_code
        messageRes = resPaymentsCreate.message
      }
    } catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err payments create api(service-customer):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    } finally {
      let options = {
        client_secret: clientSecret
      }
      resBodyApi = makeCustomerResSuccErr(isSuccess, messageRes, options)
      res.status(statusCode).send(resBodyApi)
    }
  },
  validDataBasket: async (req, res, next) => {
    /*
      NOTE:
      *** Description: Kiểm tra xem thông tin trong giỏ hàng có hợp lệ trước khi thanh toán
      - inputData: {
        info_acc: {
          id: 1,
          username: 'abc_xyz',
          email: 'abc@gmail.com',
          ...
        },
        basket: [{...}, {...}],
      }
      - outputData: {
        success: {
          message: 'Validation success'
        },
        error: {
          message: 'Exceed the quantity in the warehouse' | 'Address and phone number are required'
        }
      }
    */
    let body = req.body
      , statusCode
      , resBodyApi
      , isSuccess = false
      , messageRes = ''
      , conn
    try {
      let resValidDataBasket
      conn = dbs
      resValidDataBasket = await customerController.validDataBasket(conn, body)
      if (resValidDataBasket.is_success) {
        // CASE: Successful
        isSuccess = true
        statusCode = resValidDataBasket.status_code
        messageRes = resValidDataBasket.message
      } else {
        statusCode = resValidDataBasket.status_code
        messageRes = resValidDataBasket.message
      }
    }  catch (err) {
      if (IS_LOG_IN_SERVICE) { console.log(`*Err valid data basket api(service-customer):`, err) }
      messageRes = err.message
      statusCode = STATUS_CODE_RES.INTERNAL_SERVER_ERR
    } finally {
      resBodyApi = makeCustomerResSuccErr(isSuccess, messageRes)
      res.status(statusCode).send(resBodyApi)
    }
  },
}

module.exports = customerService
