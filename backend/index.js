'use strict'

const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const config = require('./utils/config')
const customer = require('./routes/customer')
const admin = require('./routes/admin')

let api = express()
api.use(bodyParser.json({limit: '50mb'}))
api.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
api.use(express.json())
api.use(cors())

api.use('/api/customer', customer)
api.use('/api/admin', admin)

/* istanbul ignore next */
if (!module.parent) {
  api.listen(config.port);
  console.log(`API started on port ${config.port}`);
}

module.exports = api
