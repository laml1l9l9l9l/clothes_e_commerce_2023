'use strict'

const util = require('../utils/util')
const config = require('../utils/config')

const IS_LOG_IN_SERVICE = config.is_log_in_service
  , ARR_DATA_EMPTY_VAL = util.arr_data_empty_val

const customerValidate = {
  validLogin: (infoAuth) => {
    let resValid = {}
      , isValid = true
      , mesValid = ''
      , arrMesRes = []
      , arrDataEmptyVal = ARR_DATA_EMPTY_VAL
    try {
      if (arrDataEmptyVal.includes(infoAuth)
        || (Object.keys(infoAuth).length === 0 && Object.getPrototypeOf(infoAuth) === Object.prototype)) {
        isValid = false
        mesValid = 'Username and password are required'
      }
    } catch (errValidLogin) {
      if (IS_LOG_IN_SERVICE) { console.log("**Err validUpdateLogin:", errValidLogin) }
      throw errValidLogin
    }
    resValid = {
      isValid,
      mesValid
    }
    return resValid
  }
}

module.exports = customerValidate
