import { createStore } from 'redux'
import Config from 'src/utils/config'
import Util from 'src/utils/util'

const initialState = {
  sidebarShow: true,
  basket: [],
  user: null,
  info_acc_cus: null,
  user_admin: null,
  info_acc_admin: null,
}
// Selector
export const getBasketTotal = (basket) => {
  let moneyItemsInBasket = {}
    , sumItemsInBasket = 0
    , vatItemsInBasket = 0
    , totalItemsInBasket = 0
  sumItemsInBasket = basket?.reduce((amount, item) => (item.price * item.quantity) + amount, 0)
  // NOTE: Tính thuế 10%
  vatItemsInBasket = sumItemsInBasket * 0.1
  totalItemsInBasket = sumItemsInBasket + vatItemsInBasket
  moneyItemsInBasket.sum = sumItemsInBasket
  moneyItemsInBasket.vat = vatItemsInBasket
  moneyItemsInBasket.total = totalItemsInBasket
  return moneyItemsInBasket
}

export const getDataUserCustomer = (token) => {
  return new Promise((resolve, reject) => {
    try {
      fetch(`${Config.link_api_backend}/customer/decode-token`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({token})
      })
        .then(data => {
          let response = data.json()
            , dataSucc
          dataSucc = response.success
          if (Util.isNotEmptyObj(dataSucc) && Util.isNotEmptyObj(dataSucc.decode_token)) {
            resolve(dataSucc.decode_token)
          } else {
            resolve(null)
          }
        })
    } catch (err) {
      reject(err)
    }
  })
}

const changeState = (state = initialState, { type, ...rest }) => {
  let newBasket
    , curBasket = state.basket
  switch (type) {
    case 'set':
      return { ...state, ...rest }
    case 'ADD_TO_BASKET':
      newBasket = []
      let itemFounded
        , newItem = rest.item
      itemFounded = curBasket.find(itemInBasket => itemInBasket.id === newItem.id)
      if (!itemFounded) {
        // CASE: add new item to basket
        newBasket = [...curBasket, rest.item]
      } else {
        // CASE: modify item into basket
        let indexItem
          , currQuantityItem
          , newQuantityItem
        newBasket = [...curBasket]
        indexItem = curBasket.findIndex((item => item.id === itemFounded.id))
        currQuantityItem = itemFounded.quantity
        newQuantityItem = currQuantityItem + 1
        newBasket[indexItem].quantity = newQuantityItem
      }
      return {
        ...state,
        basket: newBasket,
      }

    case 'EMPTY_BASKET':
      return {
        ...state,
        basket: []
      }

    case 'REMOVE_FROM_BASKET':
      const index = curBasket.findIndex(basketItem => basketItem.id === rest.id)
      newBasket = [...curBasket]
      
      if (index >= 0) {
        newBasket.splice(index, 1)
      } else {
        console.warn(
          `Cant remove product (id: ${rest.id}) as its not in basket!`
        )
      }

      return {
        ...state,
        basket: newBasket,
      }

    case 'SET_USER':
      return {
        ...state,
        user: rest.user,
      }

    case 'SET_INFO_USER_CUS':
      return {
        ...state,
        info_acc_cus: rest.info_acc,
      }

    case 'SET_ADMIN':
      return {
        ...state,
        user_admin: rest.user,
      }

    case 'SET_INFO_USER_ADMIN':
      return {
        ...state,
        info_acc_admin: rest.info_acc,
      }
      
    default:
      return state
  }
}

const store = createStore(changeState)
export default store
