const Data = {
  STATUS_PRODUCT: {
    INACTIVE: 0,
    ACTIVE: 1,
    CANCEL: 2,
  },
  ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ: [null, undefined, ''],
  LIST_NAME_TYPE_PRODUCT: [
    {id: 1, name: 'Áo'},
    {id: 2, name: 'Quần'},
  ],
  LIST_NAME_STATUS_PRODUCT: [
    {id: 0, name: 'Không bán'},
    {id: 1, name: 'Đang bán'},
    {id: 2, name: 'Đã hủy'}, // Không select trạng thái này phía customers
  ],
  LIST_NAME_STATUS_ACCOUNT_CUSTOMER: [
    {id: 0, name: 'Đã vô hiệu hóa'},
    {id: 1, name: 'Đã kích hoạt'},
  ],
  STATUS_CUSTOMER: {
    INACTIVE: 0,
    ACTIVE: 1,
  },
  LIST_NAME_STATUS_ORDER: [
    {id: 0, name: 'Đơn chưa xác nhận'},
    {id: 1, name: 'Đơn đã xác nhận'},
    {id: 2, name: 'Đang chuẩn bị hàng'},
    {id: 3, name: 'Đang vận chuyển'},
    {id: 4, name: 'Đã nhận'},
  ],
  LIST_NAME_STATUS_TRANSACTION: [
    {id: 0, name: 'Chưa thanh toán'},
    {id: 1, name: 'Đã thanh toán'},
  ],
  STATUS_2FA_ACC_ADMIN: {
    INACTIVE: 0,
    ACTIVE: 1,
  },
  LIST_NAME_STATUS_PRODUCT_FOR_CUSTOMER: [
    {id: 0, name: 'Not For Sale'},
    {id: 1, name: 'For Sale'},
    {id: 2, name: 'Cancelled'}, // Không select trạng thái này phía customers
  ],
  LIST_NAME_STATUS_ORDER_FOR_CUSTOMER: [
    {id: 0, name: 'Unconfimred'},
    {id: 1, name: 'Confirmed'},
    {id: 2, name: 'Preparing to ship'},
    {id: 3, name: 'In transit'},
    {id: 4, name: 'Delivered'},
  ],
}

module.exports = Data
