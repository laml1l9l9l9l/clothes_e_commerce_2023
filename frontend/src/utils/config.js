const LINK_API_BACKEND = 'http://localhost:3030/api'
const DATE_FORMAT = "DD-MM-YYYY"
const DATE_TIME_FORMAT = "YYYY-MM-DD HH:mm:ss"

module.exports = {
  link_api_backend: LINK_API_BACKEND,
  date_format: DATE_FORMAT,
  date_time_format: DATE_TIME_FORMAT,
}
