const ARR_DATA_EMPTY_VAL = [null, undefined, '']

const Util = {
  arr_data_empty_val: ARR_DATA_EMPTY_VAL,
  getBase64: file => {
    return new Promise(resolve => {
      let baseURL = ""
      // let fileInfo
      // STEP: Make new FileReader
      let reader = new FileReader()

      if (file) {
        // STEP: Convert the file to base64 text
        reader.readAsDataURL(file)

        // STEP: on reader load somthing...
        reader.onload = () => {
          // STEP: Make a fileInfo Object
          // console.log("Called", reader)
          baseURL = reader.result
          // console.log(baseURL)
          resolve(baseURL)
        }
        // console.log(fileInfo)
      } else {
        resolve("")
      }
    })
  },
  isNotEmptyObj: (obj) => {
    let isNotEmptyObj = false
      , arrDataEmptyKeyObj = ARR_DATA_EMPTY_VAL
    if (!arrDataEmptyKeyObj.includes(obj) && Object.keys(obj).length !== 0 && Object.getPrototypeOf(obj) === Object.prototype) {
      isNotEmptyObj = true
    }
    return isNotEmptyObj
  },
  isEmptyObj: (obj) => {
    let isEmptyObj = false
    if (!ARR_DATA_EMPTY_VAL.includes(obj) && Object.keys(obj).length === 0 && typeof obj === "object") {
      isEmptyObj = true
    }
    return isEmptyObj
  },
}

module.exports = Util
