import React, { useState, useEffect } from 'react'
import './Header.css'
import SearchIcon from '@material-ui/icons/Search'
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket'
import { Link, createSearchParams, useNavigate, useLocation } from 'react-router-dom'
// import { useStateValue } from '../StateProvider'
import useToken from './useToken'
import { useSelector } from 'react-redux'
import Util from 'src/utils/util'

function Header() {
  const navigate = useNavigate()
  const location = useLocation()
  const { setToken } = useToken()
  const [nameProduct, setNameProduct] = useState('')
  // const [{ basket, user }, dispatch] = useStateValue()
  const basket = useSelector(state => state.basket)
  const tokenCustomer = useSelector(state => state.user)

  const ARR_DATA_EMPTY_VAL = Util.arr_data_empty_val
  
  let nameProductInput = new URLSearchParams(location.search).get('name')
  if (ARR_DATA_EMPTY_VAL.includes(nameProductInput)) {
    nameProductInput = ''
  }

  useEffect(() => {
    setNameProduct(nameProductInput)
  }, [nameProductInput, ARR_DATA_EMPTY_VAL])

  const handleAuthentication = () => {
    if (tokenCustomer) {
      setToken(null)
    }
  }
  const searchNameProduct = e => {
    e.preventDefault()
    navigate({
      pathname: "/",
      search: createSearchParams({
        name: nameProduct
      }).toString()
    })
  }

  return (
    <div className='header__customer'>
      <Link to='/'>
        <img
          className='header__logo__customer'
          src='/assets/pngfind.com-pixar-logo-png-2341535.png'
        />
      </Link>
      <div className='header__search__customer'>
        <input className='header__searchInput__customer' type='text' value={nameProduct} onChange={e => setNameProduct(e.target.value)} />
        <SearchIcon className='header__searchIcon__customer' onClick={searchNameProduct} />
      </div>
      <div className='header__nav__customer'>
        {/* NOTE: !user -> don't access to /login but it is still executed handleAuthentication function */}
        <Link to={!tokenCustomer && '/login'} style={{ textDecoration: 'none' }}>
          <div onClick={handleAuthentication} className='header__option__customer'>
            <span className='header__optionLineOne__customer'>Hi Customer</span>
            <span className='header__optionLineTwo__customer'>
              {tokenCustomer ? 'Sign out' : 'Sign in' }
            </span>
          </div>
        </Link>
        <Link to='/orders' style={{ textDecoration: 'none' }}>
          <div className='header__option__customer'>
            <span className='header__optionLineOne__customer'>Returns</span>
            <span className='header__optionLineTwo__customer'>& Order</span>
          </div>
        </Link>
        {
          tokenCustomer ?
            (
              <Link to='/account' style={{ textDecoration: 'none' }}>
                <div className='header__option__customer'>
                  <span className='header__optionLineOne__customer'>Your</span>
                  <span className='header__optionLineTwo__customer'>Account</span>
                </div>
              </Link>
            )
          : ''
        }
        
        <Link to='/checkout' style={{ textDecoration: 'none' }}>
          <div className='header_optionBasket__customer'>
            <ShoppingBasketIcon />
            <span className='header__optionLineTwo__customer header__basketCount__customer'>
              {basket?.length}
            </span>
          </div>
        </Link>
      </div>
    </div>
  )
}

export default Header