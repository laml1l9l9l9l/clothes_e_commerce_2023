import { useState } from 'react'
// import { useStateValue } from '../StateProvider'
import { useDispatch } from 'react-redux'
import Util from 'src/utils/util'

function useToken() {
  // const [ {}, dispatch ] = useStateValue()
  const dispatch = useDispatch()
  const ARR_DATA_EMPTY_VAL = Util.arr_data_empty_val
  const getToken = () => {
    const userToken = sessionStorage.getItem('token')
    return userToken
  }
  const getInfoAccCus = () => {
    const infoAcc = sessionStorage.getItem('info-acc-customer')
    return infoAcc
  }
  const [token, setToken] = useState(getToken())
  const [infoAccCus, setInfoAccCus] = useState(getInfoAccCus())
  
  const saveToken = (userToken, infoAccCustomer = null) => {
    if (userToken) {
      sessionStorage.setItem('token', userToken)
      dispatch({
        type: 'SET_USER',
        user: userToken
      })
      setToken(userToken)
    } else {
      sessionStorage.removeItem('token')
      dispatch({
        type: 'SET_USER',
        user: null
      })
      setToken(null)
    }

    if (infoAccCustomer) {
      sessionStorage.setItem('info-acc-customer', infoAccCustomer)
      dispatch({
        type: 'SET_INFO_USER_CUS',
        info_acc: infoAccCustomer
      })
      setInfoAccCus(infoAccCustomer)
    } else {
      sessionStorage.removeItem('info-acc-customer')
      dispatch({
        type: 'SET_INFO_USER_CUS',
        info_acc: null
      })
      setInfoAccCus(null)
    }
  }

  return {
    setToken: saveToken,
    token,
    info_acc_cus: infoAccCus
  }
}

export default useToken