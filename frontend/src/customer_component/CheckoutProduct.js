import React from 'react'
import './CheckoutProduct.css'
import CurrencyFormat from 'react-currency-format'
import { useDispatch } from 'react-redux'

function CheckoutProduct({ id, image, title, price, quantity, hideButton }) {
  const dispatch = useDispatch()

  const removeFromBasket = () => {
    // remove the item from the basket
    dispatch({
      type: 'REMOVE_FROM_BASKET',
      id: id,
    })
  }

  return (
    <div className='checkoutProduct'>
      <img className='checkoutProduct__image' src={image}/>
      <div className='checkoutProduct__info'>
        <p className='checkoutProduct__title'>{title}</p>
        <p className='checkoutProduct__price'>
          <small>Price: </small>
          <CurrencyFormat
            renderText={(value) => (
              <strong>{value}</strong>
            )}
            decimalScale={2}
            value={price}
            displayType={'text'}
            thousandSeparator={true}
            suffix={" VNĐ"}
          />
        </p>
        <p className='checkoutProduct__quantity'>
          Quantity: {quantity}
        </p>
        {!hideButton && (
          <button onClick={removeFromBasket}>Remove from Basket</button>
        )}
      </div>
    </div>
  )
}

export default CheckoutProduct