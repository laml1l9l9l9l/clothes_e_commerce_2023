import React, { useState, useEffect } from 'react'
import './Payment.css'
import CheckoutProduct from "./CheckoutProduct"
import { Link, useNavigate } from "react-router-dom"
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js"
import CurrencyFormat from "react-currency-format"
import { getBasketTotal } from 'src/store'
import { useSelector, useDispatch } from 'react-redux'
import { toast } from 'react-toastify'
import Config from 'src/utils/config'
import Util from 'src/utils/util'

async function validDataBasket(infoDataBasket) {
  return fetch(`${Config.link_api_backend}/customer/valid-data-basket`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(infoDataBasket)
  })
  .then(data => data.json())
}

async function checkoutBasket(infoCheckoutBasket) {
  return fetch(`${Config.link_api_backend}/customer/checkout-basket`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(infoCheckoutBasket)
  })
  .then(data => data.json())
}

function Payment() {
  const basket = useSelector(state => state.basket)
  const infoAcc = JSON.parse(useSelector(state => state.info_acc_cus))
  const tokenCustomer = useSelector(state => state.user)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const stripe = useStripe()
  const elements = useElements()

  const [succeeded, setSucceeded] = useState(false)
  const [processing, setProcessing] = useState('')
  const [error, setError] = useState(null)
  const [disabled, setDisabled] = useState(true)
  const [clientSecret, setClientSecret] = useState(true)
  const [sumBasket] = useState(getBasketTotal(basket).sum)
  const [vatBasket] = useState(getBasketTotal(basket).vat)
  const [totalBasket] = useState(getBasketTotal(basket).total)
  const DEFAULT_CURR_BAKSET = 'VND'
  const isNotEmptyObj = Util.isNotEmptyObj

  useEffect(() => {
    if (!tokenCustomer) {
      toast.error("Please login to website", {
        position: toast.POSITION.TOP_RIGHT
      })
      navigate('/login', {replace: true})
    } else {
      try {
        // STEP: Check exceed the number of products
        if (basket.length > 0) {
          validDataBasket({
            info_acc: infoAcc,
            basket: basket
          }).then((resValidDataBasket) => {
            // NOTE: nếu valid giỏ hàng lỗi thì xóa giỏ hàng và redirect về trang chủ
            if (isNotEmptyObj(resValidDataBasket.error)) {
              let childRes = resValidDataBasket.error
              toast.error(childRes.message, {
                position: toast.POSITION.TOP_RIGHT
              })
              // STEP: Clear basket
              dispatch({
                type: 'EMPTY_BASKET'
              })
              navigate('/', {replace: true})
            } else {
              // STEP: generate the special stripe secret which allows us to charge a customer
              fetch(`${Config.link_api_backend}/customer/payments-create`, {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  total: totalBasket,
                  curr: DEFAULT_CURR_BAKSET
                })
              })
              .then(data => data.json())
              .then(dataJson => {
                let childRes
                if (isNotEmptyObj(dataJson.success)) {
                  let clientSecret
                  childRes = dataJson.success
                  clientSecret = childRes.client_secret
                  setClientSecret(clientSecret)
                } else {
                  childRes = dataJson.error
                  toast.error(childRes.message, {
                    position: toast.POSITION.TOP_RIGHT
                  })
                  navigate('/', {replace: true})
                }
              })
            }
          }).catch((errValidDataBasket) => {
            console.log("errValidDataBasket:", errValidDataBasket)
            setError(errValidDataBasket)
            setProcessing(false)
            let messErr = errValidDataBasket.message
            toast.error(messErr, {
              position: toast.POSITION.TOP_RIGHT
            })
            navigate('/')
          })
        }
      } catch (errPaymentsCreate) {
        console.log("Error Payments Create(customer):", errPaymentsCreate)
      }
    }
  }, [basket, dispatch, isNotEmptyObj, navigate, tokenCustomer, totalBasket])
  // }, [basket, dispatch, infoAcc, isNotEmptyObj, navigate, tokenCustomer, totalBasket])

  // console.log('THE SECRET IS >>>', clientSecret)
  // console.log('👱', tokenCustomer)

  const handleSubmit = async (event) => {
    // do all the fancy stripe stuff...
    event.preventDefault()

    if(error) {
      // CASE: basket is empty
      let messErr = error
      toast.error(messErr, {
        position: toast.POSITION.TOP_RIGHT
      })
    } else if (basket.length > 0) {
      // CASE: available item in basket
      setProcessing(true)
      stripe.confirmCardPayment(clientSecret, {
        payment_method: {
          card: elements.getElement(CardElement)
        }
      }).then(async ({ paymentIntent }) => {
        // STEP: Call api create order and transaction
        let resCheckoutBasket
        try {
          const DEFAULT_STATUS_TRANSACTION = 1
          resCheckoutBasket = await checkoutBasket({
            info_acc: infoAcc,
            basket: basket,
            info_payment: {
              id: paymentIntent.id,
              created: paymentIntent.created
            },
            sum: sumBasket,
            vat: vatBasket,
            total: totalBasket,
            status: DEFAULT_STATUS_TRANSACTION
          })
          
          let childRes
          if (isNotEmptyObj(resCheckoutBasket.success)) {
            childRes = resCheckoutBasket.success
            toast.success(childRes.message, {
              position: toast.POSITION.TOP_RIGHT
            })

            setSucceeded(true)
            setError(null)
            setProcessing(false)

            dispatch({
              type: 'EMPTY_BASKET'
            })

            navigate('/orders')
          } else {
            childRes = resCheckoutBasket.error
            toast.error(childRes.message, {
              position: toast.POSITION.TOP_RIGHT
            })
          }
        } catch (err) {
          setSucceeded(false)
          setError(err)
        }
      }).catch((errConfirmCardPayment) => {
        console.log("errConfirmCardPayment:", errConfirmCardPayment)
        setError(errConfirmCardPayment)
        setProcessing(false)
      })
    } else {
      // CASE: basket is empty
      let messErr = 'Basket is empty. Please add item to the basket'
      toast.error(messErr, {
        position: toast.POSITION.TOP_RIGHT
      })
      navigate('/')
    }
  }

  const handleChange = event => {
    // Listen for changes in the CardElement
    // and display any errors as the customer types their card details
    // setDisabled(event.empty)
    console.log("event.complete:",event.complete)
    if (event.error) {
      setError(event.error.message)
      setProcessing(false)
      setDisabled(true)
    } else {
      setError(null)
      // NOTE: Nhập đủ thông tin thẻ mới được nhấn thanh toán
      if (event.complete) {
        setDisabled(false)
      } else {
        setDisabled(true)
      }
    }
  }

  return (
    <div className='payment'>
      <div className='payment__container'>
        <h1>
          {/* Checkout ( */}
            <Link to="/checkout">{basket?.length} items</Link>
          {/* ) */}
        </h1>


        {/* Payment section - delivery address */}
        <div className='payment__section'>
          <div className='payment__title'>
            <h3>Delivery Address</h3>
          </div>
          <div className='payment__address'>
            <p><strong>Username: </strong>{infoAcc?.username}</p>
            <p><strong>Telephone: </strong>{infoAcc?.tel}</p>
            <p><strong>Address: </strong>{infoAcc?.address}</p>
          </div>
        </div>

        {/* Payment section - Review Items */}
        <div className='payment__section'>
          <div className='payment__title'>
            <h3>Review items and delivery</h3>
          </div>
          <div className='payment__items'>
            {basket.map(item => (
              <CheckoutProduct
                id={item.id}
                title={item.title}
                image={item.image}
                price={item.price}
                rating={item.rating}
                quantity={item.quantity}
                hideButton
              />
            ))}
          </div>
        </div>
      
        <div className='payment__section'>
          <div className="payment__title">
            <h3>Price</h3>
          </div>
          <div className="payment__details">
            <CurrencyFormat
              renderText={(value) => (
                <h3>Total Excluding Vat: <i>{value}</i></h3>
              )}
              decimalScale={2}
              value={sumBasket}
              displayType={"text"}
              thousandSeparator={true}
              suffix={" VNĐ"}
            />
            <CurrencyFormat
              renderText={(value) => (
                <h3>VAT: <i>{value}</i></h3>
              )}
              decimalScale={2}
              value={vatBasket}
              displayType={"text"}
              thousandSeparator={true}
              suffix={" VNĐ"}
            />
            <CurrencyFormat
              renderText={(value) => (
                <h3>Total: <i>{value}</i></h3>
              )}
              decimalScale={2}
              value={totalBasket}
              displayType={"text"}
              thousandSeparator={true}
              suffix={" VNĐ"}
            />
          </div>
        </div>

        {/* Payment section - Payment method */}
        <div className='payment__section'>
          <div className="payment__title">
            <h3>Payment Method</h3>
          </div>
          <div className="payment__details">
            {/* Stripe magic will go */}

            <form onSubmit={handleSubmit}>
              <CardElement onChange={handleChange}/>

              <div className='payment__priceContainer'>
                <button disabled={processing || disabled || succeeded} style={disabled ? {backgroundColor: "gray"}: {}}>
                  <span>{processing ? <p>Processing</p> : "Buy Now"}</span>
                </button>
              </div>

              {/* Errors */}
              {error && <div>{error}</div>}
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Payment
