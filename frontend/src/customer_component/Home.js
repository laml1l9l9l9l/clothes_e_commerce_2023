import React, { useState, useEffect } from 'react'
import {
  CPagination,
  CPaginationItem,
} from '@coreui/react'
import { useLocation } from 'react-router-dom'
import './Home.css'
import Product from './Product'
import Config from 'src/utils/config'
import { ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ } from 'src/utils/define_code'

async function searchProduct(entryDataSearchProduct = {}) {
  // NOTE: Call api and return data to another function
  let linkSearchProduct = `${Config.link_api_backend}/customer/search-products?` + new URLSearchParams(entryDataSearchProduct)
  return fetch(linkSearchProduct)
  .then(data => data.json())
}

function Home() {
  const location = useLocation()
  // [START] INPUT SEARCH DATA
  const nameProductInput = (new URLSearchParams(location.search).get('name')) || ''
  // [END] INPUT SEARCH DATA
  const [listProduct, setListProduct] = useState([])
  const [listPagination, setListPagination] = useState([])
  const [currPagination, setCurrPagination] = useState(1)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    searchProduct({
      name: nameProductInput
    })
    .then(resDataSearchProduct => {
      setListProduct(resDataSearchProduct.data)
      setListPagination(resDataSearchProduct.list_pagination)
      setCurrPagination(resDataSearchProduct.curr_pagination)
      setLoading(false)
    })
    .catch((err) => {
      console.log("**Error search product in home:", err)
      setLoading(true)
    })
  }, [nameProductInput])

  const submitSearchByNumPagination = async e => {
    e.preventDefault()
    let resDataSearchProduct
      , valBtn = parseInt(e.target.text)
    resDataSearchProduct = await searchProduct({
      name: nameProductInput,
      curr_pagination: valBtn
    })
    setListProduct(resDataSearchProduct.data)
    setListPagination(resDataSearchProduct.list_pagination)
    setCurrPagination(resDataSearchProduct.curr_pagination)
  }
  const submitSearchByPrevPaginationBtn = async e => {
    e.preventDefault()
    let resDataSearchProduct
      , valBtn = currPagination - 1
    resDataSearchProduct = await searchProduct({
      name: nameProductInput,
      curr_pagination: valBtn
    })
    setListProduct(resDataSearchProduct.data)
    setListPagination(resDataSearchProduct.list_pagination)
    setCurrPagination(resDataSearchProduct.curr_pagination)
  }
  const submitSearchByNextPaginationBtn = async e => {
    e.preventDefault()
    let resDataSearchProduct
      , valBtn = currPagination + 1
    resDataSearchProduct = await searchProduct({
      name: nameProductInput,
      curr_pagination: valBtn
    })
    setListProduct(resDataSearchProduct.data)
    setListPagination(resDataSearchProduct.list_pagination)
    setCurrPagination(resDataSearchProduct.curr_pagination)
  }

  let listItemsRow1 = []
    , listItemsRow2 = []
    , listItemsRow3 = []

  if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(listProduct[0])) {
    listItemsRow1.push(listProduct[0])
  }
  if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(listProduct[1])) {
    listItemsRow1.push(listProduct[1])
  }
  if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(listProduct[2])) {
    listItemsRow2.push(listProduct[2])
  }
  if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(listProduct[3])) {
    listItemsRow3.push(listProduct[3])
  }
  if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(listProduct[4])) {
    listItemsRow3.push(listProduct[4])
  }

  if (!loading) {
    return (
      <div className='home__customer'>
        <div className='home__container__customer'>
          <img
            className='home__image__customer'
            // src="https://images-eu.ssl-images-amazon.com/images/G/02/digital/video/merch2016/Hero/Covid19/Generic/GWBleedingHero_ENG_COVIDUPDATE__XSite_1500x600_PV_en-GB._CB428684220_.jpg"
            // src={require('./images/61DUO0NqyyL._SX3000_.jpg')}
            src='/assets/61DUO0NqyyL._SX3000_.jpg'
            alt=''
          />
          <div className='home__row__customer'>
            {
              listItemsRow1.length > 0 && listItemsRow1.map((product, index) => (
                <>
                  {
                    (index === 0 || index === 1) && (
                      <Product
                        id={product.id}
                        title={product.name}
                        price={parseFloat(product.price)}
                        image={product.picture}
                        quantityInWarehouse={product.quantity_in_warehouse}
                        status={product.status}
                        description={product.description}
                      />
                    )
                  }
                </>
              ))
            }
          </div>
          <div className='home__row__customer'>
            {
             listItemsRow2.length > 0 && listItemsRow2.map((product) => (
                <Product
                  id={product.id}
                  title={product.name}
                  price={parseFloat(product.price)}
                  image={product.picture}
                  quantityInWarehouse={product.quantity_in_warehouse}
                  status={product.status}
                  description={product.description}
                />
              ))
            }
          </div>
          <div className='home__row__customer'>
            {
              listItemsRow3.length > 0 && listItemsRow3.map((product, index) => (
                <>
                  {
                    (index === 0 || index === 1) && (
                      <Product
                        id={product.id}
                        title={product.name}
                        price={parseFloat(product.price)}
                        image={product.picture}
                        quantityInWarehouse={product.quantity_in_warehouse}
                        status={product.status}
                        description={product.description}
                      />
                    )
                  }
                </>
              ))
            }
          </div>
          { listProduct.length > 0 && (
            <CPagination align="center" aria-label="Page navigation example">
              <CPaginationItem aria-label="Previous" disabled={listProduct.length === 0 || currPagination === 1 ? true : false} onClick={submitSearchByPrevPaginationBtn}>
                <span aria-hidden="true">&laquo;</span>
              </CPaginationItem>
              {
                listPagination.map((numPagination) => (
                  <CPaginationItem active={currPagination === numPagination ? true : false} onClick={submitSearchByNumPagination}>{numPagination}</CPaginationItem>
                ))
              }
              <CPaginationItem aria-label="Next" disabled={listProduct.length === 0 || currPagination === listPagination[listPagination.length - 1] ? true : false} onClick={submitSearchByNextPaginationBtn}>
                <span aria-hidden="true">&raquo;</span>
              </CPaginationItem>
            </CPagination>
          ) }
        </div>
      </div>
    )
  }
}

export default Home