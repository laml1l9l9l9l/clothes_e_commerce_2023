import React from 'react'
import './Subtotal.css'
import CurrencyFormat from 'react-currency-format'
// import { useStateValue } from '../StateProvider'
// import { getBasketTotal } from '../reducer'
import { getBasketTotal } from 'src/store'
import { useSelector } from 'react-redux'
import { useNavigate } from "react-router-dom"
import { toast } from 'react-toastify'

function Subtotal() {
  const navigate = useNavigate()
  // const [{ basket }, dispatch] = useStateValue()
  const basket = useSelector(state => state.basket)

  const handlePayment = e => {
    e.preventDefault()
    if (basket.length > 0) {
      // CASE: available item in basket
      navigate('/payment')
    } else {
      // CASE: basket is empty
      let messErr = 'Basket is empty. Please add item to the basket'
      toast.error(messErr, {
        position: toast.POSITION.TOP_RIGHT
      })
    }
  }
  
  return (
    <div className='subtotal'>
      <CurrencyFormat
        renderText={(value) => (
          <>
            <p>
              Subtotal ({basket.length} items): <strong>{value}</strong>
            </p>
            {/* <small className='subtotal__gift'>
              <input type='checkbox'/> This order contains a gift
            </small> */}
          </>
        )}
        decimalScale={2}
        value={getBasketTotal(basket).total}
        displayType={'text'}
        thousandSeparator={true}
        suffix={" VNĐ"}
      />
      <button onClick={handlePayment}>Proceed to Checkout</button>
    </div>
  )
}

export default Subtotal