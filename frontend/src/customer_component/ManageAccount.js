import React, { useState, useEffect } from 'react'
import './ManageAccount.css'
import { Link, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import PropTypes from 'prop-types'
import Config from 'src/utils/config'
import Util from 'src/utils/util'
import { useSelector } from 'react-redux'

async function updateUser(infoAcc) {
  return fetch(`${Config.link_api_backend}/customer/update-account`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(infoAcc)
  })
    .then(data => data.json())
}

function ManageAccount({ setToken }) {
  const navigate = useNavigate()
  const tokenCustomer = useSelector(state => state.user)
  let infoAcc = JSON.parse(useSelector(state => state.info_acc_cus))
  if (!infoAcc) { infoAcc = {} }
  const [idAcc] = useState(infoAcc.id || null)
  const [username] = useState(infoAcc.username || '')
  const [password, setPassword] = useState('')
  const [fullName, setFullName] = useState(infoAcc.full_name || '')
  const [tel, setTel] = useState(infoAcc.tel || '')
  const [address, setAddress] = useState(infoAcc.address || '')
  const [email, setEmail] = useState(infoAcc.email || '')
  const isNotEmptyObj = Util.isNotEmptyObj

  useEffect(() => {
    if (!tokenCustomer) {
      // CASE: Don't login
      navigate('/login', {replace: true})
    }
  }, [tokenCustomer])
  
  const update = async e => {
    e.preventDefault()
    let resDataUpdateUser
    try {
      resDataUpdateUser = await updateUser({
        id: idAcc,
        password,
        full_name: fullName,
        tel: tel,
        email: email,
        address: address
      })
      if (resDataUpdateUser) {
        let childRes
        if (isNotEmptyObj(resDataUpdateUser.success)) {
          let tokenUserCustomer
            , infoUserCustomer
          childRes = resDataUpdateUser.success
          tokenUserCustomer = childRes.token
          infoUserCustomer = childRes.info_acc
          if (tokenUserCustomer) {
            toast.success(childRes.message, {
              position: toast.POSITION.TOP_RIGHT
            })
            let strInfoUserCustomer = JSON.stringify(infoUserCustomer)
            setToken(tokenUserCustomer, strInfoUserCustomer)
            navigate('/', {replace: true})
          } else {
            throw new Error('Failed to update account')
          }
        } else {
          // CASE: resDataUpdateUser.error
          childRes = resDataUpdateUser.error
          console.log("Case - Error update account(customer):", childRes)
          toast.error(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      } else {
        throw new Error('Failed to update account')
      }
    } catch (errUpdAcc) {
      console.log("Error update account(customer):", errUpdAcc)
      toast.error(errUpdAcc.message, {
        position: toast.POSITION.TOP_RIGHT
      })
    }
  }

  return (
    <div className='manage_account'>
      <div className='manage_account__container'>
        <h1>Info Account</h1>
        <form>
          <h5>Username</h5>
          <input type='text' value={username} disabled />
          
          <h5>Password</h5>
          <input type='password' value={password} onChange={e => setPassword(e.target.value)} />

          <h5>Full Name</h5>
          <input type='text' value={fullName} onChange={e => setFullName(e.target.value)} />

          <h5>Telephone</h5>
          <input type='text' value={tel} onChange={e => setTel(e.target.value)} />

          <h5>Address</h5>
          <input type='text' value={address} onChange={e => setAddress(e.target.value)} />

          <h5>Email</h5>
          <input type='text' value={email} onChange={e => setEmail(e.target.value)} />

          <button type='submit' onClick={update} className='manage_account__updateButton'>Update</button>
        </form>
      </div>
    </div>
  )
}

ManageAccount.propTypes = {
  setToken: PropTypes.func.isRequired,
}

export default ManageAccount