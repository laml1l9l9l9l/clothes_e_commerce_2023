import React from 'react'
import './Product.css'
import CurrencyFormat from 'react-currency-format'
import { useDispatch } from 'react-redux'
import { STATUS_PRODUCT, LIST_NAME_STATUS_PRODUCT_FOR_CUSTOMER } from 'src/utils/define_code'
import { toast } from 'react-toastify'

function Product({ id, title, image, price, quantityInWarehouse, status }) {
  const dispatch = useDispatch()
  const addToBasket = () => {
    if (status === STATUS_PRODUCT.INACTIVE) {
      toast.error(`Can't add a not-for-sale product to the basket`, {
        position: toast.POSITION.TOP_RIGHT
      })
    } else {
      // NOTE: dispatch the item into the data layer
      const DEFAULT_QUANTITY_ITEM = 1
      dispatch({
        type: 'ADD_TO_BASKET',
        item: {
          id: id,
          title: title,
          image: image,
          price: price,
          quantity: DEFAULT_QUANTITY_ITEM
        }
      })
      toast.success('Add product to basket successfully', {
        position: toast.POSITION.TOP_RIGHT
      })
    }
  }
  let styleAdd2BasketBtn = {}
    , statusDisabledAdd2BasketBtn = false
  if (quantityInWarehouse <= 0) {
    statusDisabledAdd2BasketBtn = true
    styleAdd2BasketBtn = {backgroundColor: "gray", cursor: "not-allowed"}
  } else {
    if (status === STATUS_PRODUCT.INACTIVE) {
      styleAdd2BasketBtn = {backgroundColor: "red"}
    }
  }

  return (
    <div className='product'>
      <div className='product__info'>
        <p>{title}</p>
        <p className='product__price'>
          {/* <small>$</small>
          <strong>{price}</strong> */}
          <CurrencyFormat
            renderText={(value) => (
              <p>
                <strong>{value}</strong>
              </p>
            )}
            decimalScale={2}
            value={price}
            displayType={'text'}
            thousandSeparator={true}
            suffix={" VNĐ"}
          />
        </p>
        <div className='product__quantity'>
          <p>{quantityInWarehouse} <small><i>{quantityInWarehouse === 0 ? 'piece available' : 'pieces available'}</i></small></p>
        </div>
        <div className='product__status'>
          <mark>
            { LIST_NAME_STATUS_PRODUCT_FOR_CUSTOMER.find(data => data.id === status).name }
          </mark>
        </div>
      </div>
      <img
        src={image}
        alt=''
      />
      <button onClick={addToBasket} disabled={statusDisabledAdd2BasketBtn} style={styleAdd2BasketBtn}>Add to Basket</button>
    </div>
  )
}

export default Product