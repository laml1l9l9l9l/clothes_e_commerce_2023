import React, { useState, useEffect } from 'react'
import './Orders.css'
import Order from './Order'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import Config from 'src/utils/config'

function Orders() {
  const navigate = useNavigate()
  const tokenCustomer = useSelector(state => state.user)
  const [orders, setOrders] = useState([])
  const infoAcc = JSON.parse(useSelector(state => state.info_acc_cus))

  useEffect(() => {
    if(tokenCustomer) {
      // CASE: Logined
      try {
        fetch(`${Config.link_api_backend}/customer/search-orders?id_acc=${infoAcc.id}`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json'
          },
        })
        .then(data => data.json())
        .then(orders => {
          setOrders(orders)
        })
        .catch((err) => {
          console.log("**Error search order(customer):", err)
        })
      } catch (errOrders) {
        console.log("Error Orders(customer):", errOrders)
      }
    } else {
      // CASE: Don't login
      setOrders([])
      navigate('/login', {replace: true})
    }

  }, [tokenCustomer])

  return (
    <div className='orders'>
      <h1>Your Orders</h1>

      <div className='orders__order'>
        {orders?.map(order => (
          <Order order={order} />
        ))}
      </div>
    </div>
  )
}

export default Orders
