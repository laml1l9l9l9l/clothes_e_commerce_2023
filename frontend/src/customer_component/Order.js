import React from 'react'
import './Order.css'
import moment from "moment"
import CheckoutProduct from "./CheckoutProduct"
import CurrencyFormat from "react-currency-format"
import { LIST_NAME_STATUS_ORDER_FOR_CUSTOMER } from 'src/utils/define_code'

function Order({ order }) {
  return (
    <div className='order'>
      <h2>Order</h2>
      <p>{moment(order.created_date).format("MMMM Do YYYY, h:mma")}</p>
      <p className="order__id">
        <small><b>ID Order:</b> {order.id}</small>
      </p>
      <p className="order__status">
        <small>
          <b>Status: </b>
          { LIST_NAME_STATUS_ORDER_FOR_CUSTOMER.find(data => data.id === order.status).name }
        </small>
      </p>
      {order.data?.map(item => (
        <CheckoutProduct
          id={item.id}
          title={item.title}
          image={item.image}
          price={item.price}
          rating={item.rating}
          quantity={item.quantity}
          hideButton
        />
      ))}
      <CurrencyFormat
        renderText={(value) => (
          <h3 className="order__total">Order Total: {value}</h3>
        )}
        decimalScale={2}
        value={order.total}
        displayType={"text"}
        thousandSeparator={true}
        suffix={" VNĐ"}
      />
    </div>
  )
}

export default Order
