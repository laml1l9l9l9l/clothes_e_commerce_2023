import axios from "axios"
import Config from 'src/utils/config'

const instance = axios.create({
  // THE API (cloud function) URL
  baseURL: `${Config.link_api_backend}/customer`
})

export default instance


