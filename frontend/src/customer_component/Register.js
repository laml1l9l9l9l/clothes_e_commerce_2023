import React, { useState } from 'react'
import './Register.css'
import { Link, useNavigate } from 'react-router-dom'
import PropTypes from 'prop-types'
import { toast } from 'react-toastify'
import Config from '../utils/config'
import { isNotEmptyObj } from 'src/utils/util'
import { ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ } from 'src/utils/define_code'

async function registerUser(credentials) {
  return fetch(`${Config.link_api_backend}/customer/register`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  })
  .then(data => data.json())
}

function Register() {
  const navigate = useNavigate()
  const [username, setUserName] = useState('')
  const [password, setPassword] = useState('')
  const [rePassword, setRePassword] = useState('')
  const [fullName, setFullName] = useState('')
  

  const register = async e => {
    e.preventDefault()
    let resDataRegister
    if (ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(username)) {
      toast.error('Required username', {
        position: toast.POSITION.TOP_RIGHT
      })
    } else if (ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(password)) {
      toast.error('Required password', {
        position: toast.POSITION.TOP_RIGHT
      })
    } else if (ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(rePassword)) {
      toast.error('Required re-password', {
        position: toast.POSITION.TOP_RIGHT
      })
    } else if (ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(fullName)) {
      toast.error('Required full name', {
        position: toast.POSITION.TOP_RIGHT
      })
    } else if (password !== rePassword) {
      toast.error('The passwords do not match. please enter the password again', {
        position: toast.POSITION.TOP_RIGHT
      })
    } else {
      // CASE: Valid information
      try {
        resDataRegister = await registerUser({
          username,
          password,
          full_name: fullName
        })
        let childRes
        if (isNotEmptyObj(resDataRegister.success)) {
          childRes = resDataRegister.success
          toast.success(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
          navigate('/login')
        } else if (isNotEmptyObj(resDataRegister.error)) {
          childRes = resDataRegister.error
          toast.error(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        } else {
          throw new Error('Failed to register')
        }
      } catch (errAuth) {
        toast.error(errAuth.message, {
          position: toast.POSITION.TOP_RIGHT
        })
        console.log("Error authenticating (register-customer):", errAuth)
      }
    }
  }

  return (
    <div className='register'>
      <Link to='/'>
        <img
          className='register__logo'
          // src={require('./images/PngItem_2317287.png')}
          src='/assets/PngItem_2317287.png'
        />
      </Link>

      <div className='register__container'>
        <h1>Register</h1>
        <form>
          <h5>Username</h5>
          <input type='text' value={username} onChange={e => setUserName(e.target.value)} />
          
          <h5>Password</h5>
          <input type='password' value={password} onChange={e => setPassword(e.target.value)} />
          
          <h5>Re-Password</h5>
          <input type='password' value={rePassword} onChange={e => setRePassword(e.target.value)} />

          <h5>Full name</h5>
          <input type='text' value={fullName} onChange={e => setFullName(e.target.value)} />

          <button  type='submit' onClick={register} className='register__registerButton'>Register</button>
        </form>

        <Link to='/login'>
          <button className='register__signInButton'>Sign In</button>
        </Link>
      </div>
    </div>
  )
}

Register.propTypes = {
  setToken: PropTypes.func.isRequired,
}

export default Register