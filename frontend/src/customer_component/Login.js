import React, { useState, useEffect } from 'react'
import './Login.css'
import { useSelector } from 'react-redux'
import { Link, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import PropTypes from 'prop-types'
import Config from 'src/utils/config'
import Util from 'src/utils/util'

async function loginUser(credentials) {
  return fetch(`${Config.link_api_backend}/customer/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  })
    .then(data => data.json())
}

function Login({ setToken }) {
  const navigate = useNavigate()
  const [username, setUserName] = useState('')
  const [password, setPassword] = useState('')
  const isNotEmptyObj = Util.isNotEmptyObj
  const tokenCustomer = useSelector(state => state.user)
  
  useEffect(() => {
    if(tokenCustomer) {
      navigate('/', {replace: true})
    }
  }, [tokenCustomer, navigate])

  const signIn = async e => {
    e.preventDefault()
    let resDataLogin
    try {
      resDataLogin = await loginUser({
        username,
        password
      })
      if (resDataLogin) {
        let childRes
        if (isNotEmptyObj(resDataLogin.success)) {
          let tokenUserCustomer
            , infoUserCustomer
          childRes = resDataLogin.success
          tokenUserCustomer = childRes.token
          infoUserCustomer = childRes.info_acc
          if (tokenUserCustomer) {
            toast.success(childRes.message, {
              position: toast.POSITION.TOP_RIGHT
            })
            let strInfoUserCustomer = JSON.stringify(infoUserCustomer)
            setToken(tokenUserCustomer, strInfoUserCustomer)
            navigate('/', {replace: true})
          } else {
            throw new Error('Failed to login')
          }
        } else {
          // CASE: resDataLogin.error
          childRes = resDataLogin.error
          toast.error(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      } else {
        throw new Error('Failed to login')
      }
    } catch (errAuth) {
      console.log("Error authenticating(login-customer):", errAuth)
      toast.error(errAuth.message, {
        position: toast.POSITION.TOP_RIGHT
      })
    }
  }

  return (
    <div className='login'>
      <Link to='/'>
        <img
          className='login__logo'
          // src={require('./images/PngItem_2317287.png')}
          src='/assets/PngItem_2317287.png'
        />
      </Link>

      <div className='login__container'>
        <h1>Sign-in</h1>
        <form>
          <h5>Username</h5>
          <input type='text' value={username} onChange={e => setUserName(e.target.value)} />
          
          <h5>Password</h5>
          <input type='password' value={password} onChange={e => setPassword(e.target.value)} />

          <button type='submit' onClick={signIn} className='login__signInButton'>Sign In</button>
        </form>

        <Link to='/register'>
          <button className='login__registerButton'>Create your New Account</button>
        </Link>
      </div>
    </div>
  )
}

Login.propTypes = {
  setToken: PropTypes.func.isRequired,
}

export default Login