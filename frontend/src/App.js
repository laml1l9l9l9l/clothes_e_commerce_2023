import React, { useEffect } from 'react'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom'
import './App.css'
import Header from './customer_component/Header'
import Home from './customer_component/Home'
import Checkout from './customer_component/Checkout'
import Payment from './customer_component/Payment'
import Orders from "./customer_component/Orders"
import Login from './customer_component/Login'
import ManageAccount from './customer_component/ManageAccount'
import Register from './customer_component/Register'
import useToken from './customer_component/useToken'
import useTokenAdmin from './admin_component/useToken'
import { loadStripe } from '@stripe/stripe-js'
import { Elements } from '@stripe/react-stripe-js'
import './scss/style.scss'
// import { useStateValue } from './StateProvider'

const promise = loadStripe(
  "pk_test_51HPvU9DFg5koCdLGJJbNo60QAU99BejacsvnKvT8xnCu1wFLCuQP3WBArscK3RvSQmSIB3N0Pbsc7TtbQiJ1vaOi00X9sIbazL"
)

// AdminComponent
const DefaultAdmin = React.lazy(() => import('./admin_component/layout/DefaultLayout'))
const LoginAdmin = React.lazy(() => import('./admin_component/views/auth/Login'))
const Page404Admin = React.lazy(() => import('./admin_component/views/pages/page404/Page404'))
const Page500Admin = React.lazy(() => import('./admin_component/views/pages/page500/Page500'))

function App() {
  const { token, info_acc_cus: infoAccCus, setToken } = useToken()
  const { token_admin: tokenAdmin, info_acc_admin: infoAccAdmin, setTokenAdmin } = useTokenAdmin()
  // const [ {}, dispatch ] = useStateValue()

  useEffect(() => {
    // will only run once when the app component loads...
    // console.log(">>>This token customer:", token)
    // console.log(">>>This token admin:", tokenAdmin)
    /**
     * NOTE: Đoạn này để khi refresh trang web thì store đc set lại các giá trị lấy ra từ session
     * START 101
    */
    if (token) {
      // the user just logged in / the user was logged in
      setToken(token, infoAccCus)
    } else {
      // the user is logged out
      setToken(null)
    }
    if (tokenAdmin) {
      setTokenAdmin(tokenAdmin, infoAccAdmin)
    } else {
      setTokenAdmin(null)
    }
    /**
     * END 101
    */
  }, [token, tokenAdmin, infoAccCus, infoAccAdmin])

  return (
    <Router>
        <div className="app">
          <ToastContainer />
          <Routes>
            <Route path='/login' element={<Login setToken={setToken}/>} />
            <Route path='/register' element={<Register/>} />
            <Route path='/checkout' element={<><Header /><Checkout /></>} />
            <Route path='/payment' element={
              <>
                <Header />
                <Elements stripe={promise}>
                  <Payment />
                </Elements>
              </>
            } />
            <Route path='/orders' element={<><Header /><Orders /></>} />
            <Route path='/account' element={<><Header /><ManageAccount setToken={setToken}/></>} />
            <Route path='/' element={<><Header /><Home /></>} />
            {/* Default admin route */}
            <Route path='/*' name="Admin" element={tokenAdmin ? <DefaultAdmin /> : <Navigate to="/admin/login" replace />} />
            <Route exact path="/admin/login" name="Login Admin Page" element={<LoginAdmin setToken={setTokenAdmin}/>} />
            <Route exact path="/admin/404" name="Page 404" element={<Page404Admin />} />
            <Route exact path="/admin/500" name="Page 500" element={<Page500Admin />} />
          </Routes>
        </div>
    </Router>
  );
}

export default App
