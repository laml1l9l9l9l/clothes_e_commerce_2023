import React from 'react'

/*
  NOTE:
  - Mapping route in _nav to *.js file
*/

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'))
const EditAccount = React.lazy(() => import('./views/setup/EditAccount'))
const Setup2FA = React.lazy(() => import('./views/setup/2FA'))
const SearchProducts = React.lazy(() => import('./views/products/Search'))
const AddProducts = React.lazy(() => import('./views/products/Add'))
const EditProducts = React.lazy(() => import('./views/products/Edit'))
const SearchAccCustomers = React.lazy(() => import('./views/customers/Search'))
const AddAccCustomers = React.lazy(() => import('./views/customers/Add'))
const EditAccCustomers = React.lazy(() => import('./views/customers/Edit'))
const Login = React.lazy(() => import('./views/auth/Login'))
const SearchOrders = React.lazy(() => import('./views/orders/Search'))
const EditOrders = React.lazy(() => import('./views/orders/Edit'))
const SearchTransactions = React.lazy(() => import('./views/transactions/Search'))

const routes = [
  { path: '/admin/', exact: true, name: 'Home' },
  { path: '/admin/dashboard', name: 'Thống kê', element: Dashboard },
  { path: '/admin/setup/', exact: true, name: 'Cấu hình' },
  { path: '/admin/setup/edit-admin', name: 'Cập nhật tài khoản', element: EditAccount },
  { path: '/admin/setup/2fa', name: 'Xác thực 2 thành phần', element: Setup2FA },
  { path: '/admin/products', name: 'Sản phẩm', element: SearchProducts, exact: true },
  { path: '/admin/products/search', name: 'Tra cứu sản phẩm', element: SearchProducts },
  { path: '/admin/products/add', name: 'Thêm mới sản phẩm', element: AddProducts },
  { path: '/admin/products/edit/:id', name: 'Sửa sản phẩm', element: EditProducts },
  { path: '/admin/orders', name: 'Đơn hàng', element: SearchOrders, exact: true },
  { path: '/admin/orders/search', name: 'Tra cứu đơn hàng', element: SearchOrders },
  { path: '/admin/orders/edit/:id', name: 'Sửa đơn hàng', element: EditOrders },
  { path: '/admin/orders', name: 'Giao dịch', element: SearchTransactions, exact: true },
  { path: '/admin/transactions/search', name: 'Tra cứu giao dịch', element: SearchTransactions },
  { path: '/admin/customers', name: 'tài khoản khách hàng', element: SearchAccCustomers, exact: true },
  { path: '/admin/customers/search', name: 'Quản lý tài khoản khách hàng', element: SearchAccCustomers },
  { path: '/admin/customers/add', name: 'Thêm mới tài khoản khách hàng', element: AddAccCustomers },
  { path: '/admin/customers/edit/:id', name: 'Sửa tài khoản khách hàng', element: EditAccCustomers },
  { path: '/admin/login', name: 'Đăng nhập', element: Login },
]

export default routes
