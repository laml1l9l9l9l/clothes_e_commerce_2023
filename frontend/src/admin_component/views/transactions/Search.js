import React, { useState, useEffect } from 'react'
import {
  CCard,
  CCardBody,
  CCardFooter,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CFormLabel,
  CFormSelect,
  CButton,
  CPagination,
  CPaginationItem,
} from '@coreui/react'
import DatePicker from 'react-datepicker'
import CurrencyFormat from 'react-currency-format'
import Moment from 'moment'
import 'react-datepicker/dist/react-datepicker.css'
import Config from 'src/utils/config'
import DefineCode from 'src/utils/define_code'

async function searchTransaction(entryDataSearchTransaction) {
  // NOTE: Call api and return data to another function
  return fetch(`${Config.link_api_backend}/admin/transaction/search?` + new URLSearchParams(entryDataSearchTransaction))
  .then(data => data.json())
}

const Search = () => {
  const SEVEN_DAYS_AGO = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)
  const CURRENT_DATE = new Date()
  // [START] INPUT SEARCH DATA
  const [statusTransactionInput, setStatusTransactionInput] = useState('')
  const [startDateInput, setStartDateInput] = useState(SEVEN_DAYS_AGO)
  const [endDateInput, setEndDateInput] = useState(CURRENT_DATE)
  // [END] INPUT SEARCH DATA
  const [listTransaction, setListTransaction] = useState([])
  const [listPagination, setListPagination] = useState([])
  const [currPagination, setCurrPagination] = useState(1)
  const [loading, setLoading] = useState(true)

  const LIST_NAME_STATUS_TRANSACTION = DefineCode.LIST_NAME_STATUS_TRANSACTION
    , DATE_TIME_FORMAT = Config.date_time_format

  // Make api get transactions and apply for view
  useEffect(() => {
    searchTransaction({
      status: statusTransactionInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT)
    })
    .then(resDataSearchTransaction => {
      setListTransaction(resDataSearchTransaction.data)
      setListPagination(resDataSearchTransaction.list_pagination)
      setCurrPagination(resDataSearchTransaction.curr_pagination)
      setLoading(false)
    })
    .catch((err) => {
      console.log("**Error search transaction:", err)
      setLoading(true)
    })
  },[statusTransactionInput, startDateInput, endDateInput, DATE_TIME_FORMAT])

  
  const submitSearchTransaction = async e => {
    e.preventDefault()
    let resDataSearchTransaction
    resDataSearchTransaction = await searchTransaction({
      status: statusTransactionInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT)
    })
    setListTransaction(resDataSearchTransaction.data)
    setListPagination(resDataSearchTransaction.list_pagination)
    setCurrPagination(resDataSearchTransaction.curr_pagination)
  }
  const submitSearchByNumPagination = async e => {
    e.preventDefault()
    let resDataSearchTransaction
      , valBtn = parseInt(e.target.text)
    resDataSearchTransaction = await searchTransaction({
      status: statusTransactionInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      curr_pagination: valBtn
    })
    setListTransaction(resDataSearchTransaction.data)
    setListPagination(resDataSearchTransaction.list_pagination)
    setCurrPagination(resDataSearchTransaction.curr_pagination)
  }
  const submitSearchByPrevPaginationBtn = async e => {
    e.preventDefault()
    let resDataSearchTransaction
      , valBtn = currPagination - 1
    resDataSearchTransaction = await searchTransaction({
      status: statusTransactionInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      curr_pagination: valBtn
    })
    setListTransaction(resDataSearchTransaction.data)
    setListPagination(resDataSearchTransaction.list_pagination)
    setCurrPagination(resDataSearchTransaction.curr_pagination)
  }
  const submitSearchByNextPaginationBtn = async e => {
    e.preventDefault()
    let resDataSearchTransaction
      , valBtn = currPagination + 1
    resDataSearchTransaction = await searchTransaction({
      status: statusTransactionInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      curr_pagination: valBtn
    })
    setListTransaction(resDataSearchTransaction.data)
    setListPagination(resDataSearchTransaction.list_pagination)
    setCurrPagination(resDataSearchTransaction.curr_pagination)
  }
  
  if (!loading) {
    return (
      <CRow>
        <CCol xs={12}>
          <CCard className="mb-4">
            <CCardBody>
              <CRow className="mb-3">
                <CCol>
                  <CRow className="mb-3">
                    <CFormLabel className="col-sm-2 col-form-label">
                      Trạng thái
                    </CFormLabel>
                    <div className="col-sm-10">
                      <CFormSelect
                        aria-label="Default select example"
                        onChange={e => setStatusTransactionInput(e.target.value)}
                      >
                        <option>Tất cả</option>
                        {
                          LIST_NAME_STATUS_TRANSACTION.map(statusTransaction => (
                            <option value={statusTransaction.id}>{statusTransaction.name}</option>
                          ))
                        }
                      </CFormSelect>
                    </div>
                  </CRow>
                </CCol>
                <CCol>
                  <CRow className="mb-3">
                    <CFormLabel className="col-sm-2 col-form-label">
                      Ngày tạo
                    </CFormLabel>
                    <div className="col-sm-10 d-md-flex">
                      <DatePicker
                        sm="auto"
                        selected={startDateInput}
                        onChange={(date) => setStartDateInput(date)}
                        className="col-form-label justify-content-md-start w-100"
                      />
                      <div sm="auto" className="col-form-label justify-content-md-center">
                        -
                      </div>
                      <DatePicker
                        sm="auto"
                        selected={endDateInput}
                        onChange={(date) => setEndDateInput(date)}
                        className="col-form-label justify-content-md-end w-100"
                      />
                    </div>
                  </CRow>
                </CCol>
              </CRow>
              <CRow className="mb-3">
                <CCol className='d-md-flex justify-content-md-end'>
                  <CButton type="submit" onClick={submitSearchTransaction}>Tìm kiếm</CButton>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>

        <CCol xs={12}>
          <CCard className="mb-4">
            <CCardBody>
              <CTable hover>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">STT</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Tài khoản khách hàng</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Tên khách hàng</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Tổng tiền thanh toán</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Trạng thái giao dịch</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Ngày tạo</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {
                    listTransaction.map((transaction, index) => (
                      <CTableRow>
                        <CTableHeaderCell scope="row">{index+1}</CTableHeaderCell>
                        <CTableDataCell>{transaction.user_name}</CTableDataCell>
                        <CTableDataCell>{transaction.user_full_name}</CTableDataCell>
                        <CTableDataCell>
                          <CurrencyFormat
                            decimalScale={2}
                            value={parseFloat(transaction.amount)}
                            displayType={'text'}
                            thousandSeparator={true}
                          />
                        </CTableDataCell>
                        <CTableDataCell>{LIST_NAME_STATUS_TRANSACTION.find(data => data.id === transaction.status).name}</CTableDataCell>
                        <CTableDataCell>{Moment(transaction.created_date).format(DATE_TIME_FORMAT)}</CTableDataCell>
                      </CTableRow>
                    ))
                  }
                </CTableBody>
              </CTable>
            </CCardBody>
            <CCardFooter className='d-md-flex'>
              <CCol className='d-md-flex justify-content-md-start'>
                <CPagination aria-label="Page navigation example">
                  <CPaginationItem aria-label="Previous" disabled={listTransaction.length === 0 || currPagination === 1 ? true : false} onClick={submitSearchByPrevPaginationBtn}>
                    <span aria-hidden="true">&laquo;</span>
                  </CPaginationItem>
                  {
                    listPagination.map((numPagination) => (
                      <CPaginationItem active={currPagination === numPagination ? true : false} onClick={submitSearchByNumPagination}>{numPagination}</CPaginationItem>
                    ))
                  }
                  <CPaginationItem aria-label="Next" disabled={listTransaction.length === 0 || currPagination === listPagination[listPagination.length - 1] ? true : false} onClick={submitSearchByNextPaginationBtn}>
                    <span aria-hidden="true">&raquo;</span>
                  </CPaginationItem>
                </CPagination>
              </CCol>
              <CCol className='d-md-flex justify-content-md-end'>
              </CCol>
            </CCardFooter>
          </CCard>
        </CCol>
      </CRow>
    )
  } else {
    return null
  }
}

export default Search
