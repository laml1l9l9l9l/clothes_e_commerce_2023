import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import {
  CCard,
  CCardBody,
  CCardFooter,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CFormLabel,
  CFormInput,
  CFormSelect,
  CButton,
  CPagination,
  CPaginationItem,
  CNavLink,
} from '@coreui/react'
import { toast } from 'react-toastify'
import DatePicker from 'react-datepicker'
import CurrencyFormat from 'react-currency-format'
import Moment from 'moment'
import 'react-datepicker/dist/react-datepicker.css'
import Config from 'src/utils/config'
import DefineCode from 'src/utils/define_code'
import Util from 'src/utils/util'

async function searchProduct(entryDataSearchProduct = {}) {
  // NOTE: Call api and return data to another function
  const isNotEmptyObj = Util.isNotEmptyObj
  let linkSearchProduct
  if (isNotEmptyObj(entryDataSearchProduct)) {
    linkSearchProduct = `${Config.link_api_backend}/admin/product/search?` + new URLSearchParams(entryDataSearchProduct)
  } else {
    linkSearchProduct = `${Config.link_api_backend}/admin/product/search`
  }
  return fetch(linkSearchProduct)
  .then(data => data.json())
}

const Search = () => {
  const SEVEN_DAYS_AGO = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)
  const CURRENT_DATE = new Date()
  // [START] INPUT SEARCH DATA
  const [nameProductInput, setNameProductInput] = useState('')
  const [statusProductInput, setStatusProductInput] = useState('')
  const [typeProductInput, setTypeProductInput] = useState('')
  const [startDateInput, setStartDateInput] = useState(SEVEN_DAYS_AGO)
  const [endDateInput, setEndDateInput] = useState(CURRENT_DATE)
  // [END] INPUT SEARCH DATA
  const [listProduct, setListProduct] = useState([])
  const [listPagination, setListPagination] = useState([])
  const [currPagination, setCurrPagination] = useState(1)
  const [loading, setLoading] = useState(true)

  const LIST_NAME_TYPE_PRODUCT = DefineCode.LIST_NAME_TYPE_PRODUCT
    , LIST_NAME_STATUS_PRODUCT = DefineCode.LIST_NAME_STATUS_PRODUCT
    , DATE_TIME_FORMAT = Config.date_time_format
  
  const isNotEmptyObj = Util.isNotEmptyObj

  // Make api get products and apply for view
  useEffect(() => {
    searchProduct({
      status: statusProductInput,
      name: nameProductInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      type: typeProductInput
    })
    .then(resDataSearchProduct => {
      setListProduct(resDataSearchProduct.data)
      setListPagination(resDataSearchProduct.list_pagination)
      setCurrPagination(resDataSearchProduct.curr_pagination)
      setLoading(false)
    })
    .catch ((err) => {
      console.log("**Error search product:", err)
      setLoading(true)
    })
  },[statusProductInput, nameProductInput, startDateInput, endDateInput, typeProductInput, DATE_TIME_FORMAT])

  
  const submitSearchProduct = async e => {
    e.preventDefault()
    let resDataSearchProduct
    resDataSearchProduct = await searchProduct({
      status: statusProductInput,
      name: nameProductInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      type: typeProductInput
    })
    setListProduct(resDataSearchProduct.data)
    setListPagination(resDataSearchProduct.list_pagination)
    setCurrPagination(resDataSearchProduct.curr_pagination)
  }
  const editProduct = async e => {
    e.preventDefault()
  }
  const deleteProduct = async e => {
    e.preventDefault()
    let idProduct = e.currentTarget.id
      , paramDeleteProduct = { id: idProduct }
    fetch(`${Config.link_api_backend}/admin/product/delete`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(paramDeleteProduct)
    })
    .then(data => data.json())
    .then(async resDataDelProduct => {
      let childRes
      if (isNotEmptyObj(resDataDelProduct.success)) {
        childRes = resDataDelProduct.success
        toast.success(childRes.message, {
          position: toast.POSITION.TOP_RIGHT
        })
      } else {
        // CASE: resDataDelProduct.error
        childRes = resDataDelProduct.error
        toast.error(childRes.message, {
          position: toast.POSITION.TOP_RIGHT
        })
      }
      // STEP: Refresh data
      let resDataSearchProduct = await searchProduct({
        status: statusProductInput,
        name: nameProductInput,
        from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
        to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
        type: typeProductInput,
        curr_pagination: currPagination
      })
      setListProduct(resDataSearchProduct.data)
      setListPagination(resDataSearchProduct.list_pagination)
      setCurrPagination(resDataSearchProduct.curr_pagination)
    })
  }
  const submitSearchByNumPagination = async e => {
    e.preventDefault()
    let resDataSearchProduct
      , valBtn = parseInt(e.target.text)
    resDataSearchProduct = await searchProduct({
      status: statusProductInput,
      name: nameProductInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      type: typeProductInput,
      curr_pagination: valBtn
    })
    setListProduct(resDataSearchProduct.data)
    setListPagination(resDataSearchProduct.list_pagination)
    setCurrPagination(resDataSearchProduct.curr_pagination)
  }
  const submitSearchByPrevPaginationBtn = async e => {
    e.preventDefault()
    let resDataSearchProduct
      , valBtn = currPagination - 1
    resDataSearchProduct = await searchProduct({
      status: statusProductInput,
      name: nameProductInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      type: typeProductInput,
      curr_pagination: valBtn
    })
    setListProduct(resDataSearchProduct.data)
    setListPagination(resDataSearchProduct.list_pagination)
    setCurrPagination(resDataSearchProduct.curr_pagination)
  }
  const submitSearchByNextPaginationBtn = async e => {
    e.preventDefault()
    let resDataSearchProduct
      , valBtn = currPagination + 1
    resDataSearchProduct = await searchProduct({
      status: statusProductInput,
      name: nameProductInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      type: typeProductInput,
      curr_pagination: valBtn
    })
    setListProduct(resDataSearchProduct.data)
    setListPagination(resDataSearchProduct.list_pagination)
    setCurrPagination(resDataSearchProduct.curr_pagination)
  }
  
  if (!loading) {
    return (
      <CRow>
        <CCol xs={12}>
          <CCard className="mb-4">
            <CCardBody>
              <CRow className="mb-3">
                <CCol>
                  <CRow className="mb-3">
                    <CFormLabel className="col-sm-2 col-form-label">
                      Trạng thái
                    </CFormLabel>
                    <div className="col-sm-10">
                      <CFormSelect
                        aria-label="Default select example"
                        onChange={e => setStatusProductInput(e.target.value)}
                      >
                        <option>Tất cả</option>
                        {
                          LIST_NAME_STATUS_PRODUCT.map(statusProduct => (
                            <option value={statusProduct.id}>{statusProduct.name}</option>
                          ))
                        }
                      </CFormSelect>
                    </div>
                  </CRow>
                </CCol>
                <CCol>
                  <CRow className="mb-3">
                      <CFormLabel htmlFor="nameProducts" className="col-sm-2 col-form-label">
                        Tên sp
                      </CFormLabel>
                      <div className="col-sm-10">
                        <CFormInput
                          type="text"
                          id="nameProducts"
                          onChange={e => setNameProductInput(e.target.value)}
                        />
                      </div>
                    </CRow>
                </CCol>
              </CRow>
              <CRow className="mb-3">
                <CCol>
                  <CRow className="mb-3">
                    <CFormLabel className="col-sm-2 col-form-label">
                      Ngày tạo
                    </CFormLabel>
                    <div className="col-sm-10 d-md-flex">
                      <DatePicker
                        sm="auto"
                        selected={startDateInput}
                        onChange={(date) => setStartDateInput(date)}
                        className="col-form-label justify-content-md-start w-100"
                      />
                      <div sm="auto" className="col-form-label justify-content-md-center">
                        -
                      </div>
                      <DatePicker
                        sm="auto"
                        selected={endDateInput}
                        onChange={(date) => setEndDateInput(date)}
                        className="col-form-label justify-content-md-end w-100"
                      />
                    </div>
                  </CRow>
                </CCol>
                <CCol>
                  <CRow className="mb-3">
                    <CFormLabel className="col-sm-2 col-form-label">
                      Loại sp
                    </CFormLabel>
                    <div className="col-sm-10">
                      <CFormSelect
                        aria-label="Default select example"
                        onChange={e => setTypeProductInput(e.target.value)}
                      >
                        <option>Tất cả</option>
                        {
                          LIST_NAME_TYPE_PRODUCT.map(typeProduct => (
                            <option value={typeProduct.id}>{typeProduct.name}</option>
                          ))
                        }
                      </CFormSelect>
                    </div>
                  </CRow>
                </CCol>
              </CRow>
              <CRow className="mb-3">
                <CCol className='d-md-flex justify-content-md-end'>
                  <CButton type="submit" onClick={submitSearchProduct}>Tìm kiếm</CButton>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>

        <CCol xs={12}>
          <CCard className="mb-4">
            <CCardBody>
              <CTable hover>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">STT</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Tên sản phẩm</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Loại sản phẩm</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Giá sản phẩm</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Số lượng sản phẩm</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Trạng thái sản phẩm</CTableHeaderCell>
                    <CTableHeaderCell scope="col">#</CTableHeaderCell>
                    <CTableHeaderCell scope="col">#</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {
                    listProduct.map((product, index) => (
                      <CTableRow>
                        <CTableHeaderCell scope="row">{index+1}</CTableHeaderCell>
                        <CTableDataCell>{product.name}</CTableDataCell>
                        <CTableDataCell>{LIST_NAME_TYPE_PRODUCT.find(data => data.id === product.type).name}</CTableDataCell>
                        <CTableDataCell>
                          <CurrencyFormat
                            decimalScale={2}
                            value={parseFloat(product.price)}
                            displayType={'text'}
                            thousandSeparator={true}
                          />
                        </CTableDataCell>
                        <CTableDataCell>
                          <CurrencyFormat
                            decimalScale={2}
                            value={parseInt(product.quantity_in_warehouse)}
                            displayType={'text'}
                            thousandSeparator={true}
                          />
                        </CTableDataCell>
                        <CTableDataCell>{LIST_NAME_STATUS_PRODUCT.find(data => data.id === product.status).name}</CTableDataCell>
                        <CTableHeaderCell scope="row">
                          <CButton type="button" onClick={editProduct} color="warning" variant="outline" className="m-1">
                            <CNavLink to={`/admin/products/edit/${product.id}`} component={NavLink}>Sửa</CNavLink>
                          </CButton>
                        </CTableHeaderCell>
                        <CTableHeaderCell scope="row">
                          <CButton type="button" id={product.id} onClick={deleteProduct} color="danger" variant="outline" className="m-1">Xóa</CButton>
                        </CTableHeaderCell>
                      </CTableRow>
                    ))
                  }
                </CTableBody>
              </CTable>
            </CCardBody>
            <CCardFooter className='d-md-flex'>
              <CCol className='d-md-flex justify-content-md-start'>
                <CPagination aria-label="Page navigation example">
                  <CPaginationItem aria-label="Previous" disabled={listProduct.length === 0 || currPagination === 1 ? true : false} onClick={submitSearchByPrevPaginationBtn}>
                    <span aria-hidden="true">&laquo;</span>
                  </CPaginationItem>
                  {
                    listPagination.map((numPagination) => (
                      <CPaginationItem active={currPagination === numPagination ? true : false} onClick={submitSearchByNumPagination}>{numPagination}</CPaginationItem>
                    ))
                  }
                  <CPaginationItem aria-label="Next" disabled={listProduct.length === 0 || currPagination === listPagination[listPagination.length - 1] ? true : false} onClick={submitSearchByNextPaginationBtn}>
                    <span aria-hidden="true">&raquo;</span>
                  </CPaginationItem>
                </CPagination>
              </CCol>
              <CCol className='d-md-flex justify-content-md-end'>
              </CCol>
            </CCardFooter>
          </CCard>
        </CCol>
      </CRow>
    )
  } else {
    return null
  }
}

export default Search
