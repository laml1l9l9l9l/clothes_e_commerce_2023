import React, { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom';
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CForm,
  CFormInput,
  CFormLabel,
  CFormTextarea,
  CFormSelect,
  CRow,
  CImage,
} from '@coreui/react'
import { toast } from 'react-toastify'
import Config from 'src/utils/config'
import DefineCode from 'src/utils/define_code'
import Util from 'src/utils/util'

async function editProduct(infoEditProduct) {
  return fetch(`${Config.link_api_backend}/admin/product/edit`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(infoEditProduct)
  })
  .then(data => data.json())
}

const Edit = () => {
  const navigate = useNavigate()
  const { id: idProduct } = useParams()

  const ARR_DATA_EMPTY_VAL = Util.arr_data_empty_val
  const LIST_NAME_STATUS_PRODUCT = DefineCode.LIST_NAME_STATUS_PRODUCT
  const getBase64 = Util.getBase64
  const isNotEmptyObj = Util.isNotEmptyObj

  // NOTE: Load data product
  useEffect(() => {
    fetch(`${Config.link_api_backend}/admin/product/${idProduct}`)
    .then(data => data.json())
    .then(product => {
      let childRes
      if (isNotEmptyObj(product.success)) {
        let infoProduct
        childRes = product.success
        infoProduct = childRes.data
        setNameProduct(infoProduct.name)
        setTypeProduct(infoProduct.type)
        setStatusProduct(infoProduct.status)
        setPriceProduct(infoProduct.price)
        setDiscountProduct(infoProduct.discount)
        setQuantityInWarehouseProduct(infoProduct.quantity)
        if (!ARR_DATA_EMPTY_VAL.includes(infoProduct.description)) { setDescriptionProduct(infoProduct.description) }
        setBase64URLPicture(infoProduct.picture)
      } else {
        // NOTE: Redirect and toast notification
        navigate('/admin/products/search', { replace: true })
      }
    })
  },[])

  const [nameProduct, setNameProduct] = useState('')
  const [typeProduct, setTypeProduct] = useState(1)
  const [statusProduct, setStatusProduct] = useState(1)
  const [priceProduct, setPriceProduct] = useState(0)
  const [discountProduct, setDiscountProduct] = useState(0)
  const [quantityInWarehouseProduct, setQuantityInWarehouseProduct] = useState(0)
  const [descriptionProduct, setDescriptionProduct] = useState('')
  const [filePicture, setFilePicture] = useState(null)
  const [base64URLPicture, setBase64URLPicture] = useState('')

  const submitEditProduct = async e => {
    e.preventDefault()
    let resDataEditProduct
    try {
      const STATUS_PRODUCT = DefineCode.STATUS_PRODUCT
      resDataEditProduct = await editProduct({
        id: idProduct,
        name: nameProduct,
        status: statusProduct,
        type: typeProduct,
        price: priceProduct,
        discount: discountProduct,
        quantity_in_warehouse: quantityInWarehouseProduct,
        description: descriptionProduct,
        picture: base64URLPicture
      })
      if (resDataEditProduct) {
        let childRes
        if (isNotEmptyObj(resDataEditProduct.success)) {
          childRes = resDataEditProduct.success
          toast.success(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        } else {
          // CASE: resDataEditProduct.error
          childRes = resDataEditProduct.error
          toast.error(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
        // setToken(resDataLogin.token)
        // navigate('/')
      } else {
        throw new Error('Failed to edit product')
      }
    } catch (errEditProduct) {
      console.log("Error edit product:", errEditProduct)
      toast.error('Xảy ra lỗi hệ thống', {
        position: toast.POSITION.TOP_RIGHT
      })
      navigate('/admin/products/search', { replace: true })
    }
  }

  const handleFileInputChange = e => {
    // console.log(e.target.files[0])
    let file = filePicture

    file = e.target.files[0]

    getBase64(file)
      .then(result => {
        file["base64"] = result
        // console.log("File Is", file)
        setBase64URLPicture(result)
        setFilePicture(file)
      })
      .catch(err => {
        console.log("**Err convert file to base64:", err)
      })

    setFilePicture(e.target.files[0])
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardBody>
            <CForm>
              <div className="mb-3">
                <CFormLabel htmlFor="nameProduct">Tên sản phẩm</CFormLabel>
                <CFormInput
                  type="text"
                  id="nameProduct"
                  placeholder="Áo sơ mi"
                  value={nameProduct}
                  onChange={e => setNameProduct(e.target.value)}
                />
              </div>
              <div className="mb-3">
                {/* Selected value */}
                <CFormLabel htmlFor="typeProduct">Loại sản phẩm</CFormLabel>
                <CFormSelect
                  aria-label="Default select example" 
                  value={typeProduct}
                  onChange={e => setTypeProduct(e.target.value)}
                >
                  <option value="1">Áo</option>
                  <option value="2">Quần</option>
                </CFormSelect>
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="priceProduct">Giá sản phẩm</CFormLabel>
                <CFormInput
                  type="text"
                  id="priceProduct"
                  placeholder="50000"
                  value={priceProduct}
                  onChange={e => setPriceProduct(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel className="col-sm-2 col-form-label">
                  Trạng thái
                </CFormLabel>
                <CFormSelect
                  aria-label="Default select example"
                  value={statusProduct}
                  onChange={e => setStatusProduct(e.target.value)}
                >
                  {
                    LIST_NAME_STATUS_PRODUCT.map((infoStatus) => (
                      <option value={infoStatus.id}>{infoStatus.name}</option>
                    ))
                  }
                </CFormSelect>
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="discountProduct">Giá giảm</CFormLabel>
                <CFormInput
                  type="text"
                  id="discountProduct"
                  placeholder="0"
                  value={discountProduct}
                  onChange={e => setDiscountProduct(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="quantityProduct">Số lượng sản phẩm</CFormLabel>
                <CFormInput
                  type="text"
                  id="quantityProduct"
                  placeholder="1000"
                  value={quantityInWarehouseProduct}
                  onChange={e => setQuantityInWarehouseProduct(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="descriptionProduct">Mô tả</CFormLabel>
                <CFormTextarea
                  id="descriptionProduct"
                  rows="3"
                  placeholder="Sang trọng, ôm sát"
                  value={descriptionProduct}
                  onChange={e => setDescriptionProduct(e.target.value)}
                ></CFormTextarea>
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="formFile">Ảnh sản phẩm</CFormLabel>
                <CFormInput
                  type="file"
                  id="formFile"
                  onChange={handleFileInputChange}
                />
              </div>
              {
                !ARR_DATA_EMPTY_VAL.includes(base64URLPicture) ? (
                  <div className="mb-3">
                    <CImage align="center" rounded src={base64URLPicture} width={200} height={220} />
                  </div>
                ) : ''
              }
              <div className="mb-3">
                <CCol className='d-md-flex justify-content-md-end'>
                  <CButton type="submit" onClick={submitEditProduct}>Sửa</CButton>
                </CCol>
              </div>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Edit
