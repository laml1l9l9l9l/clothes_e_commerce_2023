import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CForm,
  CFormInput,
  CFormLabel,
  CFormTextarea,
  CFormSelect,
  CRow,
  CImage,
} from '@coreui/react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import Config from 'src/utils/config'
import DefineCode from 'src/utils/define_code'
import Util from 'src/utils/util'

async function addProduct(infoAddProduct) {
  // NOTE: Call api and return data to another function
  return fetch(`${Config.link_api_backend}/admin/product/add`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(infoAddProduct)
  })
  .then(data => data.json())
}

const FormControl = () => {
  const navigate = useNavigate()
  const DEFAULT_NAME_PRODUCT = ''
  const DEFAULT_TYPE_PRODUCT = 1
  const DEFAULT_PRICE_PRODUCT = null
  const DEFAULT_DISCOUNT_PRODUCT = 0
  const DEFAULT_QUANTITY_PRODUCT = 0
  const DEFAULT_DESCRIPTION_PRODUCT = ''
  const DEFAULT_BASE64URL_PICTURE_PRODUCT = ''

  const [nameProduct, setNameProduct] = useState(DEFAULT_NAME_PRODUCT)
  const [typeProduct, setTypeProduct] = useState(DEFAULT_TYPE_PRODUCT)
  const [priceProduct, setPriceProduct] = useState(DEFAULT_PRICE_PRODUCT)
  const [discountProduct, setDiscountProduct] = useState(DEFAULT_DISCOUNT_PRODUCT)
  const [quantityInWarehouseProduct, setQuantityInWarehouseProduct] = useState(DEFAULT_QUANTITY_PRODUCT)
  const [descriptionProduct, setDescriptionProduct] = useState(DEFAULT_DESCRIPTION_PRODUCT)
  const [base64URLPicture, setBase64URLPicture] = useState(DEFAULT_BASE64URL_PICTURE_PRODUCT)

  const ARR_DATA_EMPTY_VAL = Util.arr_data_empty_val
  const isNotEmptyObj = Util.isNotEmptyObj

  const submitAddProduct = async e => {
    e.preventDefault()
    let resDataAddProduct
    try {
      const STATUS_PRODUCT = DefineCode.STATUS_PRODUCT
      let DEFAULT_STATUS_PRODUCT = STATUS_PRODUCT.ACTIVE
      resDataAddProduct = await addProduct({
        name: nameProduct,
        status: DEFAULT_STATUS_PRODUCT,
        type: typeProduct,
        price: priceProduct,
        discount: discountProduct,
        quantity_in_warehouse: quantityInWarehouseProduct,
        description: descriptionProduct,
        picture: base64URLPicture
      })
      if (resDataAddProduct) {
        let childRes
        if (isNotEmptyObj(resDataAddProduct.success)) {
          childRes = resDataAddProduct.success
          toast.success(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        } else {
          // CASE: resDataAddProduct.error
          childRes = resDataAddProduct.error
          toast.error(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
        // setToken(resDataLogin.token)
        navigate('/admin/products/search', { replace: true })
      } else {
        throw new Error('Failed to add product')
      }
    } catch (errAddProduct) {
      console.log("Error add product:", errAddProduct)
      toast.error('Xảy ra lỗi hệ thống', {
        position: toast.POSITION.TOP_RIGHT
      })
    }
  }

  const getBase64 = Util.getBase64

  const handleFileInputChange = e => {
    // console.log(e.target.files[0])
    let file

    file = e.target.files[0]

    getBase64(file)
      .then(result => {
        file["base64"] = result
        // console.log("File Is", file)
        setBase64URLPicture(result)
      })
      .catch(err => {
        console.log("**Err convert file to base64:", err)
      })
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardBody>
            <CForm>
              <div className="mb-3">
                <CFormLabel htmlFor="nameProduct">Tên sản phẩm</CFormLabel>
                <CFormInput
                  type="text"
                  id="nameProduct"
                  placeholder="Áo sơ mi"
                  value={nameProduct}
                  onChange={e => setNameProduct(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="typeProduct">Loại sản phẩm</CFormLabel>
                <CFormSelect
                  aria-label="Default select example" 
                  value={typeProduct}
                  onChange={e => setTypeProduct(e.target.value)}
                >
                  <option value="1">Áo</option>
                  <option value="2">Quần</option>
                </CFormSelect>
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="priceProduct">Giá sản phẩm</CFormLabel>
                <CFormInput
                  type="text"
                  id="priceProduct"
                  placeholder="50000"
                  value={priceProduct}
                  onChange={e => setPriceProduct(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="discountProduct">Giá giảm</CFormLabel>
                <CFormInput
                  type="text"
                  id="discountProduct"
                  placeholder="0"
                  value={discountProduct}
                  onChange={e => setDiscountProduct(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="quantityProduct">Số lượng sản phẩm</CFormLabel>
                <CFormInput
                  type="text"
                  id="quantityProduct"
                  placeholder="1000"
                  value={quantityInWarehouseProduct}
                  onChange={e => setQuantityInWarehouseProduct(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="descriptionProduct">Mô tả</CFormLabel>
                <CFormTextarea
                  id="descriptionProduct"
                  rows="3"
                  placeholder="Sang trọng, ôm sát"
                  value={descriptionProduct}
                  onChange={e => setDescriptionProduct(e.target.value)}
                ></CFormTextarea>
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="formFile">Ảnh sản phẩm</CFormLabel>
                <CFormInput
                  type="file"
                  id="formFile"
                  onChange={handleFileInputChange}
                />
              </div>
              {
                !ARR_DATA_EMPTY_VAL.includes(base64URLPicture) ? (
                  <div className="mb-3">
                    <CImage align="center" rounded src={base64URLPicture} width={200} height={220} />
                  </div>
                ) : ''
              }
              <div className="mb-3">
                <CCol className='d-md-flex justify-content-md-end'>
                  <CButton type="submit" onClick={submitAddProduct}>Thêm mới</CButton>
                </CCol>
              </div>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default FormControl
