import React, { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom';
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CForm,
  CFormInput,
  CFormLabel,
  // CFormTextarea,
  CFormSelect,
  CRow,
  // CImage,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CTable,
  CTableHead,
  CTableRow,
  CTableHeaderCell,
  CTableBody,
  CTableDataCell,
} from '@coreui/react'
import { toast } from 'react-toastify'
import Config from 'src/utils/config'
import DefineCode from 'src/utils/define_code'
import Util from 'src/utils/util'

async function editOrder(infoEditOrder) {
  return fetch(`${Config.link_api_backend}/admin/order/edit`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(infoEditOrder)
  })
  .then(data => data.json())
}

const Edit = () => {
  const navigate = useNavigate()
  const { id: idOrder } = useParams()

  // const ARR_DATA_EMPTY_VAL = Util.arr_data_empty_val
  const LIST_NAME_STATUS_ORDER = DefineCode.LIST_NAME_STATUS_ORDER
  // const getBase64 = Util.getBase64
  const isNotEmptyObj = Util.isNotEmptyObj

  // NOTE: Load data order
  useEffect(() => {
    fetch(`${Config.link_api_backend}/admin/order/${idOrder}`)
    .then(data => data.json())
    .then(resDataOrder => {
      let childRes 
      if (isNotEmptyObj(resDataOrder.success)) {
        let infoOrder
        childRes = resDataOrder.success
        infoOrder = childRes.data
        setSumOrder(infoOrder.sum)
        setVatOrder(infoOrder.vat)
        setTotalOrder(infoOrder.total)
        setStatusOrder(infoOrder.status)
        setDataInOrder(infoOrder.data)
      } else {
        // NOTE: Redirect and toast notification
        navigate('/admin/orders/search', { replace: true })
      }
    })
  },[idOrder, isNotEmptyObj, navigate])

  const [sumOrder, setSumOrder] = useState(0)
  const [vatOrder, setVatOrder] = useState(0)
  const [totalOrder, setTotalOrder] = useState(0)
  const [statusOrder, setStatusOrder] = useState(0)
  const [dataInOrder, setDataInOrder] = useState([])
  const [visibleModal, setVisibleModal] = useState(false)

  const submitEditOrder = async e => {
    e.preventDefault()
    let resDataEditProduct
    try {
      resDataEditProduct = await editOrder({
        id: idOrder,
        status: statusOrder,
      })
      if (resDataEditProduct) {
        let childRes
        if (isNotEmptyObj(resDataEditProduct.success)) {
          childRes = resDataEditProduct.success
          toast.success(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
          navigate('/admin/orders/search', { replace: true })
        } else {
          // CASE: resDataEditProduct.error
          childRes = resDataEditProduct.error
          toast.error(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      } else {
        throw new Error('Failed to edit order')
      }
    } catch (errEditOrder) {
      console.log("Error edit order:", errEditOrder)
      toast.error('Xảy ra lỗi hệ thống', {
        position: toast.POSITION.TOP_RIGHT
      })
    }
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardBody>
            <CForm>
              <div className="mb-3">
                <CFormLabel htmlFor="sumOrder">Tổng tiền chưa Thuế</CFormLabel>
                <CFormInput
                  type="text"
                  id="sumOrder"
                  placeholder=""
                  value={sumOrder}
                  disabled
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="vatOrder">Tiền Thuế</CFormLabel>
                <CFormInput
                  type="text"
                  id="vatOrder"
                  placeholder=""
                  value={vatOrder}
                  disabled
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="totalOrder">Tổng tiền đã có Thuế</CFormLabel>
                <CFormInput
                  type="text"
                  id="totalOrder"
                  placeholder=""
                  value={totalOrder}
                  disabled
                />
              </div>
              <div className="mb-3">
                <CFormLabel className="col-sm-2 col-form-label">
                  Trạng thái
                </CFormLabel>
                <CFormSelect
                  aria-label="Default select example"
                  value={statusOrder}
                  onChange={e => setStatusOrder(e.target.value)}
                >
                  {
                    LIST_NAME_STATUS_ORDER.map((infoStatus) => (
                      <option value={infoStatus.id}>{infoStatus.name}</option>
                    ))
                  }
                </CFormSelect>
              </div>
              <div className="mb-3">
                <CCol className='d-md-flex justify-content-md-end'>
                  <CButton className="mx-1" color="primary" variant="outline" onClick={() => setVisibleModal(!visibleModal)}>Chi tiết đơn hàng</CButton>
                  <CButton type="submit" onClick={submitEditOrder}>Sửa</CButton>
                </CCol>
              </div>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
      { dataInOrder.length > 0 ? (
        <CModal visible={visibleModal} onClose={() => setVisibleModal(false)}>
          <CModalHeader onClose={() => setVisibleModal(false)}>
            <CModalTitle>Chi tiết đơn hàng</CModalTitle>
          </CModalHeader>
          <CModalBody>
            <CTable>
              <CTableHead>
                <CTableRow>
                  <CTableHeaderCell scope="col">#</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Tên sp</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Giá sp</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Số lượng</CTableHeaderCell>
                </CTableRow>
              </CTableHead>
              <CTableBody>
                {
                  dataInOrder.map((product, index) => (
                    <CTableRow>
                      <CTableHeaderCell scope="row">{index+1}</CTableHeaderCell>
                      <CTableDataCell>{product.title}</CTableDataCell>
                      <CTableDataCell>{product.price}</CTableDataCell>
                      <CTableDataCell>{product.quantity}</CTableDataCell>
                    </CTableRow>
                  ))
                }
              </CTableBody>
            </CTable>
          </CModalBody>
          <CModalFooter>
            <CButton color="secondary" onClick={() => setVisibleModal(false)}>
              Close
            </CButton>
          </CModalFooter>
        </CModal>
      ) : '' }
    </CRow>
  )
}

export default Edit
