import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import {
  CCard,
  CCardBody,
  CCardFooter,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CFormLabel,
  CFormSelect,
  CButton,
  CPagination,
  CPaginationItem,
  CNavLink,
} from '@coreui/react'
import DatePicker from 'react-datepicker'
import CurrencyFormat from 'react-currency-format'
import Moment from 'moment'
import 'react-datepicker/dist/react-datepicker.css'
import Config from 'src/utils/config'
import DefineCode from 'src/utils/define_code'

async function searchOrder(entryDataSearchOrder) {
  // NOTE: Call api and return data to another function
  return fetch(`${Config.link_api_backend}/admin/order/search?` + new URLSearchParams(entryDataSearchOrder))
  .then(data => data.json())
}

const Search = () => {
  const SEVEN_DAYS_AGO = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)
  const CURRENT_DATE = new Date()
  // [START] INPUT SEARCH DATA
  const [statusOrderInput, setStatusOrderInput] = useState('')
  const [startDateInput, setStartDateInput] = useState(SEVEN_DAYS_AGO)
  const [endDateInput, setEndDateInput] = useState(CURRENT_DATE)
  // [END] INPUT SEARCH DATA
  const [listOrder, setListOrder] = useState([])
  const [listPagination, setListPagination] = useState([])
  const [currPagination, setCurrPagination] = useState(1)
  const [loading, setLoading] = useState(true)

  const LIST_NAME_STATUS_ORDER = DefineCode.LIST_NAME_STATUS_ORDER
    , DATE_TIME_FORMAT = Config.date_time_format

  // Make api get orders and apply for view
  useEffect(() => {
    searchOrder({
      status: statusOrderInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT)
    })
    .then(resDataSearchOrder => {
      setListOrder(resDataSearchOrder.data)
      setListPagination(resDataSearchOrder.list_pagination)
      setCurrPagination(resDataSearchOrder.curr_pagination)
      setLoading(false)
    })
    .catch((err) => {
      console.log("**Error search order:", err)
      setLoading(true)
    })
  },[statusOrderInput, startDateInput, endDateInput, DATE_TIME_FORMAT])

  
  const submitSearchOrder = async e => {
    e.preventDefault()
    let resDataSearchOrder
    resDataSearchOrder = await searchOrder({
      status: statusOrderInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT)
    })
    setListOrder(resDataSearchOrder.data)
    setListPagination(resDataSearchOrder.list_pagination)
    setCurrPagination(resDataSearchOrder.curr_pagination)
  }
  const editOrder = async e => {
    e.preventDefault()
  }
  const submitSearchByNumPagination = async e => {
    e.preventDefault()
    let resDataSearchProduct
      , valBtn = parseInt(e.target.text)
    resDataSearchProduct = await searchOrder({
      status: statusOrderInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      curr_pagination: valBtn
    })
    setListOrder(resDataSearchProduct.data)
    setListPagination(resDataSearchProduct.list_pagination)
    setCurrPagination(resDataSearchProduct.curr_pagination)
  }
  const submitSearchByPrevPaginationBtn = async e => {
    e.preventDefault()
    let resDataSearchProduct
      , valBtn = currPagination - 1
    resDataSearchProduct = await searchOrder({
      status: statusOrderInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      curr_pagination: valBtn
    })
    setListOrder(resDataSearchProduct.data)
    setListPagination(resDataSearchProduct.list_pagination)
    setCurrPagination(resDataSearchProduct.curr_pagination)
  }
  const submitSearchByNextPaginationBtn = async e => {
    e.preventDefault()
    let resDataSearchProduct
      , valBtn = currPagination + 1
    resDataSearchProduct = await searchOrder({
      status: statusOrderInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      curr_pagination: valBtn
    })
    setListOrder(resDataSearchProduct.data)
    setListPagination(resDataSearchProduct.list_pagination)
    setCurrPagination(resDataSearchProduct.curr_pagination)
  }
  
  if (!loading) {
    return (
      <CRow>
        <CCol xs={12}>
          <CCard className="mb-4">
            <CCardBody>
              <CRow className="mb-3">
                <CCol>
                  <CRow className="mb-3">
                    <CFormLabel className="col-sm-2 col-form-label">
                      Trạng thái
                    </CFormLabel>
                    <div className="col-sm-10">
                      <CFormSelect
                        aria-label="Default select example"
                        onChange={e => setStatusOrderInput(e.target.value)}
                      >
                        <option>Tất cả</option>
                        {
                          LIST_NAME_STATUS_ORDER.map(statusOrder => (
                            <option value={statusOrder.id}>{statusOrder.name}</option>
                          ))
                        }
                      </CFormSelect>
                    </div>
                  </CRow>
                </CCol>
                <CCol>
                  <CRow className="mb-3">
                    <CFormLabel className="col-sm-2 col-form-label">
                      Ngày tạo
                    </CFormLabel>
                    <div className="col-sm-10 d-md-flex">
                      <DatePicker
                        sm="auto"
                        selected={startDateInput}
                        onChange={(date) => setStartDateInput(date)}
                        className="col-form-label justify-content-md-start w-100"
                      />
                      <div sm="auto" className="col-form-label justify-content-md-center">
                        -
                      </div>
                      <DatePicker
                        sm="auto"
                        selected={endDateInput}
                        onChange={(date) => setEndDateInput(date)}
                        className="col-form-label justify-content-md-end w-100"
                      />
                    </div>
                  </CRow>
                </CCol>
              </CRow>
              <CRow className="mb-3">
                <CCol className='d-md-flex justify-content-md-end'>
                  <CButton type="submit" onClick={submitSearchOrder}>Tìm kiếm</CButton>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>

        <CCol xs={12}>
          <CCard className="mb-4">
            <CCardBody>
              <CTable hover>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">STT</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Tổng tiền chưa Thuế</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Tiền Thuế</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Tổng tiền đã có Thuế</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Trạng thái đơn hàng</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Ngày tạo</CTableHeaderCell>
                    <CTableHeaderCell scope="col">#</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {
                    listOrder.map((order, index) => (
                      <CTableRow>
                        <CTableHeaderCell scope="row">{index+1}</CTableHeaderCell>
                        <CTableDataCell>
                          <CurrencyFormat
                            decimalScale={2}
                            value={parseFloat(order.sum)}
                            displayType={'text'}
                            thousandSeparator={true}
                          />
                        </CTableDataCell>
                        <CTableDataCell>
                          <CurrencyFormat
                            decimalScale={2}
                            value={parseFloat(order.vat)}
                            displayType={'text'}
                            thousandSeparator={true}
                          />
                        </CTableDataCell>
                        <CTableDataCell>
                          <CurrencyFormat
                            decimalScale={2}
                            value={parseFloat(order.total)}
                            displayType={'text'}
                            thousandSeparator={true}
                          />
                        </CTableDataCell>
                        <CTableDataCell>{LIST_NAME_STATUS_ORDER.find(data => data.id === order.status).name}</CTableDataCell>
                        <CTableDataCell>{Moment(order.created_date).format(DATE_TIME_FORMAT)}</CTableDataCell>
                        <CTableHeaderCell scope="row">
                          <CButton type="button" onClick={editOrder} color="warning" variant="outline" className="m-1">
                            <CNavLink to={`/admin/orders/edit/${order.id}`} component={NavLink}>Sửa</CNavLink>
                          </CButton>
                        </CTableHeaderCell>
                      </CTableRow>
                    ))
                  }
                </CTableBody>
              </CTable>
            </CCardBody>
            <CCardFooter className='d-md-flex'>
              <CCol className='d-md-flex justify-content-md-start'>
                <CPagination aria-label="Page navigation example">
                  <CPaginationItem aria-label="Previous" disabled={listOrder.length === 0 || currPagination === 1 ? true : false} onClick={submitSearchByPrevPaginationBtn}>
                    <span aria-hidden="true">&laquo;</span>
                  </CPaginationItem>
                  {
                    listPagination.map((numPagination) => (
                      <CPaginationItem active={currPagination === numPagination ? true : false} onClick={submitSearchByNumPagination}>{numPagination}</CPaginationItem>
                    ))
                  }
                  <CPaginationItem aria-label="Next" disabled={listOrder.length === 0 || currPagination === listPagination[listPagination.length - 1] ? true : false} onClick={submitSearchByNextPaginationBtn}>
                    <span aria-hidden="true">&raquo;</span>
                  </CPaginationItem>
                </CPagination>
              </CCol>
              <CCol className='d-md-flex justify-content-md-end'>
              </CCol>
            </CCardFooter>
          </CCard>
        </CCol>
      </CRow>
    )
  } else {
    return null
  }
}

export default Search
