import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import { toast } from 'react-toastify'
import Config from 'src/utils/config'
import Util from 'src/utils/util'
import DefineCode from 'src/utils/define_code'

async function loginUser(credentials) {
  return fetch(`${Config.link_api_backend}/admin/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  })
    .then(data => data.json())
}

async function verifyOTP2FA(infoVerifyOTP) {
  return fetch(`${Config.link_api_backend}/admin/verify-otp-2fa`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(infoVerifyOTP)
  })
  .then(data => data.json())
}

const Login = ({ setToken }) => {
  const navigate = useNavigate()
  const [username, setUserName] = useState('')
  const [password, setPassword] = useState('')
  const [messSuccLoginTemp, setMessSuccLoginTemp] = useState('')
  // const [tokenAdminTemp, setTokenAdminTemp] = useState('')
  const [infoUserAdminTemp, setInfoUserAdminTemp] = useState('')
  const [otpVal, setOtpVal] = useState('')
  const [visibleVerifyQrCodeFormModal, setVisibleVerifyQrCodeFormModal] = useState(false)
  const isNotEmptyObj = Util.isNotEmptyObj
  const tokenAdmin = useSelector(state => state.user_admin)

  const STATUS_2FA_ACC_ADMIN = DefineCode.STATUS_2FA_ACC_ADMIN
  
  useEffect(() => {
    if(tokenAdmin) {
      navigate('/admin/', {replace: true})
    }
  }, [tokenAdmin, navigate])
  
  const signIn = async e => {
    e.preventDefault()
    let resDataLogin
    try {
      resDataLogin = await loginUser({
        username,
        password
      })
      // console.log(resDataLogin)
      if (resDataLogin) {
        let childRes
        if (isNotEmptyObj(resDataLogin.success)) {
          let tokenUserAdmin
            , infoUserAdmin
          childRes = resDataLogin.success
          tokenUserAdmin = childRes.token
          infoUserAdmin = childRes.info_acc
          if (childRes.token) {
            let strInfoUserAdmin = JSON.stringify(infoUserAdmin)
            if (infoUserAdmin.status_2fa === STATUS_2FA_ACC_ADMIN.ACTIVE) {
              // CASE: Use 2FA
              // NOTE: Save temp to response from api authentication
              setMessSuccLoginTemp(childRes.message)
              setInfoUserAdminTemp(infoUserAdmin)
              // NOTE: Show QR code form modal
              setVisibleVerifyQrCodeFormModal(true)
            } else {
              // CASE: Don't use 2FA
              toast.success(childRes.message, {
                position: toast.POSITION.TOP_RIGHT
              })
              setToken(tokenUserAdmin, strInfoUserAdmin)
              navigate('/admin')
            }
          } else {
            throw new Error('Failed to login')
          }
        } else {
          // CASE: resDataLogin.error
          childRes = resDataLogin.error
          toast.error(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      } else {
        throw new Error('Failed to login')
      }
    } catch (errAuth) {
      console.log("Error authenticating(admin):", errAuth)
    }
  }

  const submitVerifyOTP2FA = async e => {
    /**
     * B1: lấy mã OTP
     * B2: Xác thực và trả về thông báo
    */
    e.preventDefault()
    let resDataVerifyOTP
    try {
      resDataVerifyOTP = await verifyOTP2FA({
        id: infoUserAdminTemp.id,
        otp: otpVal
      })
      if (resDataVerifyOTP) {
        let childRes
        if (isNotEmptyObj(resDataVerifyOTP.success)) {
          // CASE: resDataGenerateQrCode.success
          let tokenUserAdmin
            , infoUserAdmin
          childRes = resDataVerifyOTP.success
          tokenUserAdmin = childRes.token
          infoUserAdmin = childRes.info_acc
          let strInfoUserAdmin = JSON.stringify(infoUserAdmin)
          setToken(tokenUserAdmin, strInfoUserAdmin)
          toast.success(messSuccLoginTemp, {
            position: toast.POSITION.TOP_RIGHT
          })
          // STEP: Invisible config QR code modal
          setVisibleVerifyQrCodeFormModal(false)
          // STEP: Redirect to admin's home
          navigate('/admin')
        } else {
          // CASE: resDataGenerateQrCode.error
          childRes = resDataVerifyOTP.error
          toast.error(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      } else {
        throw new Error('Failed to verify OTP')
      }
    } catch (errVerifyOTP) {
      console.log("Error verify OTP:", errVerifyOTP)
      toast.error('Xảy ra lỗi hệ thống', {
        position: toast.POSITION.TOP_RIGHT
      })
    }
  }

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={5}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm>
                    <h1>Đăng nhập</h1>
                    <p className="text-medium-emphasis">Trang quản trị</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser} />
                      </CInputGroupText>
                      <CFormInput
                        placeholder="Tài khoản"
                        autoComplete="username"
                        value={username}
                        onChange={e => setUserName(e.target.value)}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupText>
                        <CIcon icon={cilLockLocked} />
                      </CInputGroupText>
                      <CFormInput
                        type="password"
                        placeholder="Mật khẩu"
                        autoComplete="current-password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs={6}>
                        <CButton type='submit' color="primary" className="px-4" onClick={signIn}>
                          Đăng nhập
                        </CButton>
                      </CCol>
                      <CCol xs={6} className="text-right">
                        {/* <CButton color="link" className="px-0">
                          Forgot password?
                        </CButton> */}
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
      <CModal visible={visibleVerifyQrCodeFormModal} onClose={() => setVisibleVerifyQrCodeFormModal(false)}>
        <CModalHeader onClose={() => setVisibleVerifyQrCodeFormModal(false)}>
          <CModalTitle>Xác thực OTP</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CFormInput type="text" placeholder="Nhập mã OTP" aria-label="input verify otp" value={otpVal} onChange={e => setOtpVal(e.target.value)} />
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleVerifyQrCodeFormModal(false)}>
            Đóng
          </CButton>
          <CButton color="primary" onClick={submitVerifyOTP2FA}>Xác thực</CButton>
        </CModalFooter>
      </CModal>
    </div>
  )
}

export default Login