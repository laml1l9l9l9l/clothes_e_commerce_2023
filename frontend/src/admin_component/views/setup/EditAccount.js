import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CForm,
  CFormInput,
  CFormLabel,
  CRow,
} from '@coreui/react'
import { toast } from 'react-toastify'
import { useSelector } from 'react-redux'
import Config from 'src/utils/config'
import Util from 'src/utils/util'
import useTokenAdmin from 'src/admin_component/useToken'

async function updateUser(infoAcc) {
  return fetch(`${Config.link_api_backend}/admin/update-account`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(infoAcc)
  })
  .then(data => data.json())
}

const EditAccount = () => {
  const navigate = useNavigate()

  // const ARR_DATA_EMPTY_VAL = Util.arr_data_empty_val
  const isNotEmptyObj = Util.isNotEmptyObj

  const infoAcc = JSON.parse(useSelector(state => state.info_acc_admin))
  const [idAcc] = useState(infoAcc.id || null)
  const [username] = useState(infoAcc.username || '')
  const [password, setPassword] = useState('')
  const [fullName, setFullName] = useState(infoAcc.full_name || '')
  const [tel, setTel] = useState(infoAcc.tel || '')
  const { setTokenAdmin: setToken } = useTokenAdmin()

  const update = async e => {
    e.preventDefault()
    let resDataUpdateUser
    try {
      resDataUpdateUser = await updateUser({
        id: idAcc,
        password,
        full_name: fullName,
        tel: tel,
      })
      if (resDataUpdateUser) {
        let childRes
        if (isNotEmptyObj(resDataUpdateUser.success)) {
          let tokenUserAdmin
            , infoUserAdmin
          childRes = resDataUpdateUser.success
          tokenUserAdmin = childRes.token
          infoUserAdmin = childRes.info_acc
          if (tokenUserAdmin) {
            toast.success(childRes.message, {
              position: toast.POSITION.TOP_RIGHT
            })
            let strInfoUserAdmin = JSON.stringify(infoUserAdmin)
            setToken(tokenUserAdmin, strInfoUserAdmin)
            navigate('/admin', {replace: true})
          } else {
            throw new Error('Failed to update account')
          }
        } else {
          // CASE: resDataUpdateUser.error
          childRes = resDataUpdateUser.error
          console.log("Case - Error update account(admin):", childRes)
          toast.error(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      } else {
        throw new Error('Failed to update account')
      }
    } catch (errUpdAcc) {
      console.log("Error update account(admin):", errUpdAcc)
      toast.error(errUpdAcc.message, {
        position: toast.POSITION.TOP_RIGHT
      })
    }
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardBody>
            <CForm>
              <div className="mb-3">
                <CFormLabel htmlFor="username">Tài khoản</CFormLabel>
                <CFormInput
                  type="text"
                  id="username"
                  placeholder=""
                  value={username}
                  disabled
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="password">Mật khẩu</CFormLabel>
                <CFormInput
                  type="password"
                  id="password"
                  placeholder=""
                  value={password}
                  onChange={e => setPassword(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="fullName">Họ và tên</CFormLabel>
                <CFormInput
                  type="text"
                  id="fullName"
                  placeholder=""
                  value={fullName}
                  onChange={e => setFullName(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="tel">Số điện thoại</CFormLabel>
                <CFormInput
                  type="text"
                  id="tel"
                  placeholder=""
                  value={tel}
                  onChange={e => setTel(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CCol className='d-md-flex justify-content-md-end'>
                  <CButton type="submit" onClick={update}>Sửa</CButton>
                </CCol>
              </div>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default EditAccount
