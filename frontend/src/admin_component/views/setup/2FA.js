import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import {
  CCard,
  CCardBody,
  CCol,
  CRow,
  CButton,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CFormInput,
  CImage,
} from '@coreui/react'
import { toast } from 'react-toastify'
import Config from 'src/utils/config'
import Util from 'src/utils/util'
import DefineCode from 'src/utils/define_code'
import useTokenAdmin from 'src/admin_component/useToken'

async function generateQrCode2FA(infoGeneratedQrCode) {
  return fetch(`${Config.link_api_backend}/admin/generate-qr-2fa`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(infoGeneratedQrCode)
  })
  .then(data => data.json())
}

async function verifyOTP2FA(infoVerifyOTP) {
  return fetch(`${Config.link_api_backend}/admin/verify-otp-2fa`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(infoVerifyOTP)
  })
  .then(data => data.json())
}

async function removeQrCode2FA(infoRemoveQrCode2FA) {
  return fetch(`${Config.link_api_backend}/admin/remove-qr-2fa`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(infoRemoveQrCode2FA)
  })
  .then(data => data.json())
}

const Setup2FA = () => {
  const navigate = useNavigate()
  const tokenAcc = useSelector(state => state.user_admin)
  const infoAcc = JSON.parse(useSelector(state => state.info_acc_admin))
  const [visibleGeneratedQrCodeModal, setVisibleGeneratedQrCodeModal] = useState(false)
  const [visibleConfirmConfig2FAFormModal, setVisibleConfirmConfig2FAFormModal] = useState(false)
  const [visibleConfirmCancel2FAFormModal, setVisibleConfirmCancel2FAFormModal] = useState(false)
  const [qrCodeImageHTML, setQrCodeImageHTML] = useState('')
  const [otpVal, setOtpVal] = useState('')
  const isNotEmptyObj = Util.isNotEmptyObj
  const { setTokenAdmin: setToken } = useTokenAdmin()

  const STATUS_2FA_ACC_ADMIN = DefineCode.STATUS_2FA_ACC_ADMIN

  const submitGenerateQrCode2FA = async e => {
    /**
     * B1: lấy đc html qr code
     * B2: Xác thực thành công lưu b32 vào db
     * B3: Thông báo xác thực thành công
    */
    e.preventDefault()
    let resDataGenerateQrCode
    try {
      // STEP: Close confirm form 2FA
      setVisibleConfirmConfig2FAFormModal(false)
      resDataGenerateQrCode = await generateQrCode2FA({
        id: infoAcc.id,
        username: infoAcc.username
      })
      if (resDataGenerateQrCode) {
        let childRes
        if (isNotEmptyObj(resDataGenerateQrCode.success)) {
          childRes = resDataGenerateQrCode.success
          // toast.success(childRes.message, {
          //   position: toast.POSITION.TOP_RIGHT
          // })
          setQrCodeImageHTML(childRes.qr_html)
          infoAcc.status_2fa = STATUS_2FA_ACC_ADMIN.ACTIVE
          let strInfoUserAdmin = JSON.stringify(infoAcc)
          setToken(tokenAcc, strInfoUserAdmin)
          // STEP: Show qrcode in the modal
          setVisibleGeneratedQrCodeModal(!visibleGeneratedQrCodeModal)
        } else {
          // CASE: resDataGenerateQrCode.error
          childRes = resDataGenerateQrCode.error
          toast.error(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      } else {
        throw new Error('Failed to generate qr code')
      }
    } catch (errGenerateQrCode) {
      console.log("Error generate qr code:", errGenerateQrCode)
      toast.error('Xảy ra lỗi hệ thống', {
        position: toast.POSITION.TOP_RIGHT
      })
    }
  }

  const submitVerifyOTP2FA = async e => {
    /**
     * B1: lấy mã OTP
     * B2: Xác thực và trả về thông báo
    */
    e.preventDefault()
    let resDataVerifyOTP
    try {
      resDataVerifyOTP = await verifyOTP2FA({
        id: infoAcc.id,
        otp: otpVal
      })
      if (resDataVerifyOTP) {
        let childRes
        if (isNotEmptyObj(resDataVerifyOTP.success)) {
          // CASE: resDataGenerateQrCode.success
          let tokenUserAdmin
            , infoUserAdmin
          childRes = resDataVerifyOTP.success
          tokenUserAdmin = childRes.token
          infoUserAdmin = childRes.info_acc
          let strInfoUserAdmin = JSON.stringify(infoUserAdmin)
          setToken(tokenUserAdmin, strInfoUserAdmin)
          toast.success(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
          // STEP: Invisible config QR code modal
          setVisibleGeneratedQrCodeModal(false)
          // STEP: Redirect to admin's home
          navigate('/admin')
        } else {
          // CASE: resDataGenerateQrCode.error
          childRes = resDataVerifyOTP.error
          toast.error(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      } else {
        throw new Error('Failed to verify OTP')
      }
    } catch (errVerifyOTP) {
      console.log("Error verify OTP:", errVerifyOTP)
      toast.error('Xảy ra lỗi hệ thống', {
        position: toast.POSITION.TOP_RIGHT
      })
    }
  }

  const submitRemoveQrCode2FA = async e => {
    /**
     * B1: lấy id tài khoản
     * B2: Update cột base32_2fa = null, status_2fa = 0
     * B3: Trả về thông báo
    */
    e.preventDefault()
    let resDataRemoveQrCode
    try {
      resDataRemoveQrCode = await removeQrCode2FA({
        id: infoAcc.id
      })
      if (resDataRemoveQrCode) {
        let childRes
        if (isNotEmptyObj(resDataRemoveQrCode.success)) {
          let tokenUserAdmin
            , infoUserAdmin
          childRes = resDataRemoveQrCode.success
          tokenUserAdmin = childRes.token
          infoUserAdmin = childRes.info_acc
          let strInfoUserAdmin = JSON.stringify(infoUserAdmin)
          setToken(tokenUserAdmin, strInfoUserAdmin)
          toast.success(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
          // STEP: Redirect to admin's home
          navigate('/admin')
        } else {
          // CASE: resDataRemoveQrCode.error
          childRes = resDataRemoveQrCode.error
          toast.error(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      } else {
        throw new Error('Failed to remove qr code')
      }
    } catch (errGenerateQrCode) {
      console.log("Error remove qr code:", errGenerateQrCode)
      toast.error('Xảy ra lỗi hệ thống', {
        position: toast.POSITION.TOP_RIGHT
      })
    }
  }

  return (
    <>
      <CCard className="mb-4">
        <CCardBody>
          <CRow xs={{ gutterX: 5 }}>
            <CCol>
              <div className="p-3">
                <h4 className="card-title mb-0">
                  Thông tin tài khoản
                </h4>
                <p>
                  <b>Username: </b>
                  {infoAcc.username}
                </p>
                <p>
                  <b>Họ và tên: </b>
                  {infoAcc.full_name}
                </p>
              </div>
            </CCol>
            <CCol>
              <div className="p-3">
                <h4 className="card-title mb-0">
                  Cấu hình xác thực 2 thành phần (2FA)
                </h4>
                <p>
                  Bạn cần cài đặt app Google Authenticator trên điện thoại
                </p>
                {
                  infoAcc.status_2fa === STATUS_2FA_ACC_ADMIN.ACTIVE ?
                    <CButton color="danger" onClick={() => setVisibleConfirmCancel2FAFormModal(!visibleConfirmCancel2FAFormModal)}>Hủy cấu hình 2FA</CButton>
                  :
                    <CButton color="primary" onClick={() => setVisibleConfirmConfig2FAFormModal(!visibleConfirmConfig2FAFormModal)}>Cấu hình 2FA</CButton>
                }
              </div>
            </CCol>
          </CRow>
        </CCardBody>
      </CCard>
      <CModal visible={visibleConfirmConfig2FAFormModal} onClose={() => setVisibleConfirmConfig2FAFormModal(false)}>
        <CModalHeader onClose={() => setVisibleConfirmConfig2FAFormModal(false)}>
          <CModalTitle>Xác nhận cấu hình 2FA</CModalTitle>
        </CModalHeader>
        <CModalBody>
          Bạn chắc chắn muốn cấu hình 2FA
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleConfirmConfig2FAFormModal(false)}>
            Không
          </CButton>
          <CButton color="primary" onClick={submitGenerateQrCode2FA}>Có</CButton>
        </CModalFooter>
      </CModal>
      <CModal visible={visibleConfirmCancel2FAFormModal} onClose={() => setVisibleConfirmCancel2FAFormModal(false)}>
        <CModalHeader onClose={() => setVisibleConfirmCancel2FAFormModal(false)}>
          <CModalTitle>Hủy cấu hình 2FA</CModalTitle>
        </CModalHeader>
        <CModalBody>
          Bạn chắc chắn muốn hủy cấu hình 2FA
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleConfirmCancel2FAFormModal(false)}>
            Không
          </CButton>
          <CButton color="primary" onClick={submitRemoveQrCode2FA}>Có</CButton>
        </CModalFooter>
      </CModal>
      <CModal visible={visibleGeneratedQrCodeModal} onClose={() => {
        toast.warning('Tài khoản của bạn đã được cấu hình 2FA. Hãy cấu hình xác thực OTP để sử dụng khi đăng nhập vào hệ thống', {
          position: toast.POSITION.TOP_RIGHT
        })
        setVisibleGeneratedQrCodeModal(false)
      }}>
        <CModalHeader onClose={() => setVisibleGeneratedQrCodeModal(false)}>
          <CModalTitle>Cấu hình xác thực 2 thành phần</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <h4 className='text-warning'><i>Chú ý</i></h4>
          <div className='text-warning'><i>Tài khoản đã được cấu hình 2FA, bạn hãy làm theo hướng dẫn để sử dụng 2FA</i></div>
          <h4>Mã QR code</h4>
          <div>{qrCodeImageHTML && <CImage align="center" rounded src={qrCodeImageHTML} width={200} height={200} />}</div>
          <h4>Hướng dẫn</h4>
          <p>B1: Mở app Google Authenticator</p>
          <p>B2: Ở trong app, chọn icon '+'</p>
          <p>B3: Chọn 'Scan a barcode (or QR code)' và sử dụng camera của điện thoại để quét</p>
          <h4>Xác thực mã OTP</h4>
          <CFormInput type="text" placeholder="Nhập mã OTP" aria-label="input verify otp" value={otpVal} onChange={e => setOtpVal(e.target.value)} />
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisibleGeneratedQrCodeModal(false)}>
            Đóng
          </CButton>
          <CButton color="primary" onClick={submitVerifyOTP2FA}>Xác thực</CButton>
        </CModalFooter>
      </CModal>
    </>
  )
}

export default Setup2FA
