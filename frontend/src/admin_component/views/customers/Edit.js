import React, { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom';
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CForm,
  CFormInput,
  CFormLabel,
  CFormSelect,
  CRow,
} from '@coreui/react'
import { toast } from 'react-toastify'
import Config from 'src/utils/config'
import DefineCode from 'src/utils/define_code'
import Util from 'src/utils/util'

async function editCustomer(infoEditCustomer) {
  return fetch(`${Config.link_api_backend}/admin/customer/edit`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(infoEditCustomer)
  })
  .then(data => data.json())
}

const Edit = () => {
  const navigate = useNavigate()
  const { id: idCustomer } = useParams()

  // const ARR_DATA_EMPTY_VAL = Util.arr_data_empty_val
  const isNotEmptyObj = Util.isNotEmptyObj

  // NOTE: Load data customer
  useEffect(() => {
    fetch(`${Config.link_api_backend}/admin/customer/${idCustomer}`)
    .then(data => data.json())
    .then(customer => {
      let childRes
      if (isNotEmptyObj(customer.success)) {
        let infoCustomer
        childRes = customer.success
        infoCustomer = childRes.data
        setUsernameCustomer(infoCustomer.username)
        setFullNameCustomer(infoCustomer.fullname)
        setTelCustomer(infoCustomer.tel)
        setEmailCustomer(infoCustomer.email)
        setAddressCustomer(infoCustomer.address)
        setTaxCodeCustomer(infoCustomer.btax)
        setStatusCustomer(infoCustomer.status)
      } else {
        // NOTE: Redirect and toast notification
        navigate('/admin/customers/search', { replace: true })
      }
    })
  },[idCustomer, isNotEmptyObj, navigate])

  const STATUS_CUSTOMER = DefineCode.STATUS_CUSTOMER

  const DEFAULT_USERNAME_CUSTOMER = ''
  const DEFAULT_PASSWORD_CUSTOMER = ''
  const DEFAULT_FULL_NAME_CUSTOMER = ''
  const DEFAULT_TEL_CUSTOMER = ''
  const DEFAULT_EMAIL_CUSTOMER = ''
  const DEFAULT_ADDRESS_CUSTOMER = ''
  const DEFAULT_TAX_CODE_CUSTOMER = ''
  const DEFAULT_STATUS_CUSTOMER = STATUS_CUSTOMER.ACTIVE

  const [usernameCustomer, setUsernameCustomer] = useState(DEFAULT_USERNAME_CUSTOMER)
  const [pwdCustomer, setPwdCustomer] = useState(DEFAULT_PASSWORD_CUSTOMER)
  const [fullNameCustomer, setFullNameCustomer] = useState(DEFAULT_FULL_NAME_CUSTOMER)
  const [telCustomer, setTelCustomer] = useState(DEFAULT_TEL_CUSTOMER)
  const [emailCustomer, setEmailCustomer] = useState(DEFAULT_EMAIL_CUSTOMER)
  const [addressCustomer, setAddressCustomer] = useState(DEFAULT_ADDRESS_CUSTOMER)
  const [taxCodeCustomer, setTaxCodeCustomer] = useState(DEFAULT_TAX_CODE_CUSTOMER)
  const [statusCustomer, setStatusCustomer] = useState(DEFAULT_STATUS_CUSTOMER)

  const submitEditCustomer = async e => {
    e.preventDefault()
    let resDataEditCustomer
    try {
      resDataEditCustomer = await editCustomer({
        id: idCustomer,
        password: pwdCustomer,
        full_name: fullNameCustomer,
        tel: telCustomer,
        email: emailCustomer,
        address: addressCustomer,
        btax: taxCodeCustomer,
        status: statusCustomer
      })
      if (resDataEditCustomer) {
        let childRes
        if (isNotEmptyObj(resDataEditCustomer.success)) {
          childRes = resDataEditCustomer.success
          toast.success(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        } else {
          // CASE: resDataEditCustomer.error
          childRes = resDataEditCustomer.error
          toast.error(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      } else {
        throw new Error('Failed to edit customer')
      }
    } catch (errEditCustomer) {
      console.log("Error edit customer:", errEditCustomer)
      toast.error('Xảy ra lỗi hệ thống', {
        position: toast.POSITION.TOP_RIGHT
      })
      navigate('/admin/customers/search', { replace: true })
    }
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardBody>
            <CForm>
              <div className="mb-3">
                <CFormLabel htmlFor="usrCustomer">Tài khoản</CFormLabel>
                <CFormInput
                  type="text"
                  id="usrCustomer"
                  placeholder=""
                  value={usernameCustomer}
                  disabled
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="pwdCustomer">Mật khẩu</CFormLabel>
                <CFormInput
                  type="password"
                  id="pwdCustomer"
                  value={pwdCustomer}
                  onChange={e => setPwdCustomer(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="fullNameCustomer">Họ và tên</CFormLabel>
                <CFormInput
                  type="text"
                  id="fullNameCustomer"
                  placeholder="Lê Văn A"
                  value={fullNameCustomer}
                  onChange={e => setFullNameCustomer(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="phoneNumberCustomer">Số điện thoại</CFormLabel>
                <CFormInput
                  type="text"
                  id="phoneNumberCustomer"
                  placeholder="0377612300"
                  value={telCustomer}
                  onChange={e => setTelCustomer(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="emailCustomer">Email</CFormLabel>
                <CFormInput
                  type="text"
                  id="emailCustomer"
                  placeholder="abc@gmail.com"
                  value={emailCustomer}
                  onChange={e => setEmailCustomer(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="addressCustomer">Địa chỉ</CFormLabel>
                <CFormInput
                  type="text"
                  id="addressCustomer"
                  placeholder="So 12, ..."
                  value={addressCustomer}
                  onChange={e => setAddressCustomer(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="taxCodeCustomer">Mã số thuế</CFormLabel>
                <CFormInput
                  type="text"
                  id="taxCodeCustomer"
                  placeholder="012383745"
                  value={taxCodeCustomer}
                  onChange={e => setTaxCodeCustomer(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel className="col-sm-2 col-form-label">
                  Trạng thái
                </CFormLabel>
                <CFormSelect
                  aria-label="Default select example"
                  value={statusCustomer}
                  onChange={e => setStatusCustomer(e.target.value)}
                >
                  <option value="0">Đã vô hiệu hóa</option>
                  <option value="1">Đã kích hoạt</option>
                </CFormSelect>
              </div>
              <div className="mb-3">
                <CCol className='d-md-flex justify-content-md-end'>
                  <CButton type="submit" onClick={submitEditCustomer}>Sửa</CButton>
                </CCol>
              </div>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Edit
