import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import {
  CCard,
  CCardBody,
  CCardFooter,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
  CFormLabel,
  CFormInput,
  CFormSelect,
  CButton,
  CPagination,
  CPaginationItem,
  CNavLink,
} from '@coreui/react'
import { toast } from 'react-toastify'
import DatePicker from "react-datepicker"
import Moment from "moment"
import "react-datepicker/dist/react-datepicker.css"
import Config from 'src/utils/config'
import DefineCode from 'src/utils/define_code'
import Util from 'src/utils/util'

async function searchAccCustomer(entryDataSearchAccCustomer) {
  // NOTE: Call api and return data to another function
  return fetch(`${Config.link_api_backend}/admin/customer/search?` + new URLSearchParams(entryDataSearchAccCustomer))
  .then(data => data.json())
}

const Search = () => {
  const SEVEN_DAYS_AGO = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)
  const CURRENT_DATE = new Date()
  // [START] INPUT SEARCH DATA
  const [nameAccCustomerInput, setAccCustomerInput] = useState('')
  const [statusAccCustomerInput, setStatusAccCustomerInput] = useState('')
  const [startDateInput, setStartDateInput] = useState(SEVEN_DAYS_AGO)
  const [endDateInput, setEndDateInput] = useState(CURRENT_DATE)
  // [END] INPUT SEARCH DATA
  const [listAccCustomer, setListAccCustomer] = useState([])
  const [listPagination, setListPagination] = useState([])
  const [currPagination, setCurrPagination] = useState(1)
  const [loading, setLoading] = useState(true)

  const LIST_NAME_STATUS_ACCOUNT_CUSTOMER = DefineCode.LIST_NAME_STATUS_ACCOUNT_CUSTOMER
    , DATE_TIME_FORMAT = Config.date_time_format

  const isNotEmptyObj = Util.isNotEmptyObj

  // Make api get account customer and apply for view
  useEffect(() => {
    fetch(`${Config.link_api_backend}/admin/customer/search`)
    .then(data => data.json())
    searchAccCustomer({
      status: statusAccCustomerInput,
      username: nameAccCustomerInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT)
    })
    .then(resDataSearchAccCustomer => {
      setListAccCustomer(resDataSearchAccCustomer.data)
      setListPagination(resDataSearchAccCustomer.list_pagination)
      setCurrPagination(resDataSearchAccCustomer.curr_pagination)
      setLoading(false)
    })
    .catch((err) => {
      console.log("**Error search account customers:", err)
      setLoading(true)
    })
  },[statusAccCustomerInput, nameAccCustomerInput, startDateInput, endDateInput, DATE_TIME_FORMAT])

  
  const submitSearchAccCustomer = async e => {
    e.preventDefault()
    let resDataSearchAccCustomer
    resDataSearchAccCustomer = await searchAccCustomer({
      status: statusAccCustomerInput,
      username: nameAccCustomerInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT)
    })
    setListAccCustomer(resDataSearchAccCustomer.data)
    setListPagination(resDataSearchAccCustomer.list_pagination)
    setCurrPagination(resDataSearchAccCustomer.curr_pagination)
  }
  const editAccCustomer = async e => {
    e.preventDefault()
  }
  const deleteAccCustomer = async e => {
    e.preventDefault()
    let idAccCustomer = e.currentTarget.id
      , paramDeleteAccCustomer = { id: idAccCustomer }
    fetch(`${Config.link_api_backend}/admin/customer/delete`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(paramDeleteAccCustomer)
    })
    .then(data => data.json())
    .then(async resDataDelCustomer => {
      let childRes
      if (isNotEmptyObj(resDataDelCustomer.success)) {
        childRes = resDataDelCustomer.success
        toast.success(childRes.message, {
          position: toast.POSITION.TOP_RIGHT
        })
      } else {
        // CASE: resDataDelCustomer.error
        childRes = resDataDelCustomer.error
        toast.error(childRes.message, {
          position: toast.POSITION.TOP_RIGHT
        })
      }
      // STEP: Refresh data
      let resDataSearchAccCustomer = await searchAccCustomer({
        status: statusAccCustomerInput,
        username: nameAccCustomerInput,
        from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
        to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
        curr_pagination: currPagination
      })
      setListAccCustomer(resDataSearchAccCustomer.data)
      setListPagination(resDataSearchAccCustomer.list_pagination)
      setCurrPagination(resDataSearchAccCustomer.curr_pagination)
    })
  }
  const submitSearchByNumPagination = async e => {
    e.preventDefault()
    let resDataSearchAccCustomer
      , valBtn = parseInt(e.target.text)
    resDataSearchAccCustomer = await searchAccCustomer({
      status: statusAccCustomerInput,
      username: nameAccCustomerInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      curr_pagination: valBtn
    })
    setListAccCustomer(resDataSearchAccCustomer.data)
    setListPagination(resDataSearchAccCustomer.list_pagination)
    setCurrPagination(resDataSearchAccCustomer.curr_pagination)
  }
  const submitSearchByPrevPaginationBtn = async e => {
    e.preventDefault()
    let resDataSearchAccCustomer
      , valBtn = currPagination - 1
    resDataSearchAccCustomer = await searchAccCustomer({
      status: statusAccCustomerInput,
      username: nameAccCustomerInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      curr_pagination: valBtn
    })
    setListAccCustomer(resDataSearchAccCustomer.data)
    setListPagination(resDataSearchAccCustomer.list_pagination)
    setCurrPagination(resDataSearchAccCustomer.curr_pagination)
  }
  const submitSearchByNextPaginationBtn = async e => {
    e.preventDefault()
    let resDataSearchAccCustomer
      , valBtn = currPagination + 1
    resDataSearchAccCustomer = await searchAccCustomer({
      status: statusAccCustomerInput,
      username: nameAccCustomerInput,
      from_date: Moment(startDateInput).format(DATE_TIME_FORMAT),
      to_date: Moment(endDateInput).format(DATE_TIME_FORMAT),
      curr_pagination: valBtn
    })
    setListAccCustomer(resDataSearchAccCustomer.data)
    setListPagination(resDataSearchAccCustomer.list_pagination)
    setCurrPagination(resDataSearchAccCustomer.curr_pagination)
  }
  
  if (!loading) {
    return (
      <CRow>
        <CCol xs={12}>
          <CCard className="mb-4">
            <CCardBody>
              <CRow className="mb-3">
                <CCol>
                  <CRow className="mb-3">
                    <CFormLabel className="col-sm-2 col-form-label">
                      Trạng thái
                    </CFormLabel>
                    <div className="col-sm-10">
                      <CFormSelect
                        aria-label="Default select example"
                        onChange={e => setStatusAccCustomerInput(e.target.value)}
                      >
                        <option>Tất cả</option>
                        {
                          LIST_NAME_STATUS_ACCOUNT_CUSTOMER.map(statusAccCustomer => (
                            <option value={statusAccCustomer.id}>{statusAccCustomer.name}</option>
                          ))
                        }
                      </CFormSelect>
                    </div>
                  </CRow>
                </CCol>
                <CCol>
                  <CRow className="mb-3">
                      <CFormLabel htmlFor="nameAccCustomers" className="col-sm-2 col-form-label">
                        Tên tk
                      </CFormLabel>
                      <div className="col-sm-10">
                        <CFormInput
                          type="text"
                          id="nameAccCustomers"
                          onChange={e => setAccCustomerInput(e.target.value)}
                        />
                      </div>
                    </CRow>
                </CCol>
              </CRow>
              <CRow className="mb-3">
                <CCol>
                  <CRow className="mb-3">
                    <CFormLabel className="col-sm-2 col-form-label">
                      Ngày tạo
                    </CFormLabel>
                    <div className="col-sm-10 d-md-flex">
                      <DatePicker
                        sm="auto"
                        selected={startDateInput}
                        onChange={(date) => setStartDateInput(date)}
                        className="col-form-label justify-content-md-start w-100"
                      />
                      <div sm="auto" className="col-form-label justify-content-md-center">
                        -
                      </div>
                      <DatePicker
                        sm="auto"
                        selected={endDateInput}
                        onChange={(date) => setEndDateInput(date)}
                        className="col-form-label justify-content-md-end w-100"
                      />
                    </div>
                  </CRow>
                </CCol>
                <CCol>
                </CCol>
              </CRow>
              <CRow className="mb-3">
                <CCol className='d-md-flex justify-content-md-end'>
                  <CButton type="submit" onClick={submitSearchAccCustomer}>Tìm kiếm</CButton>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CCol>

        <CCol xs={12}>
          <CCard className="mb-4">
            <CCardBody>
              <CTable hover>
                <CTableHead>
                  <CTableRow>
                    <CTableHeaderCell scope="col">STT</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Tên tài khoản</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Trạng thái tài khoản</CTableHeaderCell>
                    <CTableHeaderCell scope="col">Ngày tạo</CTableHeaderCell>
                    <CTableHeaderCell scope="col">#</CTableHeaderCell>
                    <CTableHeaderCell scope="col">#</CTableHeaderCell>
                  </CTableRow>
                </CTableHead>
                <CTableBody>
                  {
                    listAccCustomer.map((infoAcc, index) => (
                      <CTableRow>
                        <CTableHeaderCell scope="row">{index+1}</CTableHeaderCell>
                        <CTableDataCell>{infoAcc.username}</CTableDataCell>
                        <CTableDataCell>{LIST_NAME_STATUS_ACCOUNT_CUSTOMER.find(data => data.id === infoAcc.status).name}</CTableDataCell>
                        <CTableDataCell>{Moment(infoAcc.created_date).format(Config.date_format)}</CTableDataCell>
                        <CTableHeaderCell scope="row">
                          <CButton type="button" onClick={editAccCustomer} color="warning" variant="outline" className="m-1">
                            <CNavLink to={`/admin/customers/edit/${infoAcc.id}`} component={NavLink}>Sửa</CNavLink>
                          </CButton>
                        </CTableHeaderCell>
                        <CTableHeaderCell scope="row">
                          <CButton type="button" id={infoAcc.id} onClick={deleteAccCustomer} color="danger" variant="outline" className="m-1">Xóa</CButton>
                        </CTableHeaderCell>
                      </CTableRow>
                    ))
                  }
                </CTableBody>
              </CTable>
            </CCardBody>
            <CCardFooter className='d-md-flex'>
              <CCol className='d-md-flex justify-content-md-start'>
                <CPagination aria-label="Page navigation example">
                  <CPaginationItem aria-label="Previous" disabled={listAccCustomer.length === 0 || currPagination === 1 ? true : false} onClick={submitSearchByPrevPaginationBtn}>
                    <span aria-hidden="true">&laquo;</span>
                  </CPaginationItem>
                  {
                    listPagination.map((numPagination) => (
                      <CPaginationItem active={currPagination === numPagination ? true : false} onClick={submitSearchByNumPagination}>{numPagination}</CPaginationItem>
                    ))
                  }
                  <CPaginationItem aria-label="Next" disabled={listAccCustomer.length === 0 || currPagination === listPagination[listPagination.length - 1] ? true : false} onClick={submitSearchByNextPaginationBtn}>
                    <span aria-hidden="true">&raquo;</span>
                  </CPaginationItem>
                </CPagination>
              </CCol>
              <CCol className='d-md-flex justify-content-md-end'>
              </CCol>
            </CCardFooter>
          </CCard>
        </CCol>
      </CRow>
    )
  } else {
    return null
  }
}

export default Search
