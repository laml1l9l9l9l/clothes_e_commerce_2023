import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CForm,
  CFormInput,
  CFormLabel,
  CRow,
} from '@coreui/react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import Config from 'src/utils/config'
import DefineCode from 'src/utils/define_code'
import Util from 'src/utils/util'

async function addCustomer(infoAddCustomer) {
  // NOTE: Call api and return data to another function
  return fetch(`${Config.link_api_backend}/admin/customer/add`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(infoAddCustomer)
  })
  .then(data => data.json())
}

const FormControl = () => {
  const navigate = useNavigate()
  const DEFAULT_USERNAME_CUSTOMER = ''
  const DEFAULT_PASSWORD_CUSTOMER = ''
  const DEFAULT_FULL_NAME_CUSTOMER = ''
  const DEFAULT_TEL_CUSTOMER = ''
  const DEFAULT_EMAIL_CUSTOMER = ''
  const DEFAULT_ADDRESS_CUSTOMER = ''
  const DEFAULT_TAX_CODE_CUSTOMER = ''

  const [usernameCustomer, setUsernameCustomer] = useState(DEFAULT_USERNAME_CUSTOMER)
  const [pwdCustomer, setPwdCustomer] = useState(DEFAULT_PASSWORD_CUSTOMER)
  const [fullNameCustomer, setFullNameCustomer] = useState(DEFAULT_FULL_NAME_CUSTOMER)
  const [telCustomer, setTelCustomer] = useState(DEFAULT_TEL_CUSTOMER)
  const [emailCustomer, setEmailCustomer] = useState(DEFAULT_EMAIL_CUSTOMER)
  const [addressCustomer, setAddressCustomer] = useState(DEFAULT_ADDRESS_CUSTOMER)
  const [taxCodeCustomer, setTaxCodeCustomer] = useState(DEFAULT_TAX_CODE_CUSTOMER)

  const ARR_DATA_EMPTY_VAL = Util.arr_data_empty_val
  const isNotEmptyObj = Util.isNotEmptyObj

  const submitAddProduct = async e => {
    e.preventDefault()
    let resDataAddProduct
    try {
      const STATUS_CUSTOMER = DefineCode.STATUS_CUSTOMER
      let DEFAULT_STATUS_CUSTOMER = STATUS_CUSTOMER.ACTIVE
      resDataAddProduct = await addCustomer({
        username: usernameCustomer,
        password: pwdCustomer,
        full_name: fullNameCustomer,
        tel: telCustomer,
        email: emailCustomer,
        address: addressCustomer,
        btax: taxCodeCustomer,
        status: DEFAULT_STATUS_CUSTOMER
      })
      if (resDataAddProduct) {
        let childRes
        if (isNotEmptyObj(resDataAddProduct.success)) {
          childRes = resDataAddProduct.success
          toast.success(childRes.message, {
            position: toast.POSITION.TOP_RIGHT
          })
          navigate('/admin/customers/search', { replace: true })
        } else {
          // CASE: resDataAddProduct.error
          childRes = resDataAddProduct.error
          toast.error(childRes.message.message, {
            position: toast.POSITION.TOP_RIGHT
          })
        }
      } else {
        throw new Error('Failed to add customer')
      }
    } catch (errAddProduct) {
      console.log("Error add customer:", errAddProduct)
      toast.error('Xảy ra lỗi hệ thống', {
        position: toast.POSITION.TOP_RIGHT
      })
    }
  }

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardBody>
            <CForm>
              <div className="mb-3">
                <CFormLabel htmlFor="usrCustomer">Tài khoản</CFormLabel>
                <CFormInput
                  type="text"
                  id="usrCustomer"
                  placeholder="khach_hang_1"
                  value={usernameCustomer}
                  onChange={e => setUsernameCustomer(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="pwdCustomer">Mật khẩu</CFormLabel>
                <CFormInput
                  type="password"
                  id="pwdCustomer"
                  value={pwdCustomer}
                  onChange={e => setPwdCustomer(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="fullNameCustomer">Họ và tên</CFormLabel>
                <CFormInput
                  type="text"
                  id="fullNameCustomer"
                  placeholder="Lê Văn A"
                  value={fullNameCustomer}
                  onChange={e => setFullNameCustomer(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="phoneNumberCustomer">Số điện thoại</CFormLabel>
                <CFormInput
                  type="text"
                  id="phoneNumberCustomer"
                  placeholder="0377612300"
                  value={telCustomer}
                  onChange={e => setTelCustomer(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="emailCustomer">Email</CFormLabel>
                <CFormInput
                  type="text"
                  id="emailCustomer"
                  placeholder="abc@gmail.com"
                  value={emailCustomer}
                  onChange={e => setEmailCustomer(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="addressCustomer">Địa chỉ</CFormLabel>
                <CFormInput
                  type="text"
                  id="addressCustomer"
                  placeholder="So 12, ..."
                  value={addressCustomer}
                  onChange={e => setAddressCustomer(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CFormLabel htmlFor="taxCodeCustomer">Mã số thuế</CFormLabel>
                <CFormInput
                  type="text"
                  id="taxCodeCustomer"
                  placeholder="0101383745"
                  value={taxCodeCustomer}
                  onChange={e => setTaxCodeCustomer(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <CCol className='d-md-flex justify-content-md-end'>
                  <CButton type="submit" onClick={submitAddProduct}>Thêm mới</CButton>
                </CCol>
              </div>
            </CForm>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default FormControl
