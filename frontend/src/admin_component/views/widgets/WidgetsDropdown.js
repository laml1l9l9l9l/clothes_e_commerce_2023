import React from 'react'
import {
  CRow,
  CCol,
  CWidgetStatsA,
} from '@coreui/react'
import CurrencyFormat from 'react-currency-format'

const WidgetsDropdown = ({statsMoney, statsOrders, statsProducts, statsCustomers}) => {
  return (
    <CRow>
      <CCol sm={6} lg={3}>
        <CWidgetStatsA
          className="mb-4"
          color="primary"
          value={
            <>
              <CurrencyFormat
                decimalScale={2}
                value={statsMoney}
                displayType={'text'}
                thousandSeparator={true}
                suffix={" VNĐ"}
              />
            </>
          }
          title={
            <div className="mb-3">
              Tổng số tiền đã nhận được
            </div>
          }
        />
      </CCol>
      <CCol sm={6} lg={3}>
        <CWidgetStatsA
          className="mb-4"
          color="info"
          value={
            <>
              <CurrencyFormat
                decimalScale={2}
                value={statsOrders}
                displayType={'text'}
                thousandSeparator={true}
              />
            </>
          }
          title={
            <div className="mb-3">
              Tổng số đơn hàng
            </div>
          }
        />
      </CCol>
      <CCol sm={6} lg={3}>
        <CWidgetStatsA
          className="mb-4"
          color="warning"
          value={
            <>
              <CurrencyFormat
                decimalScale={2}
                value={statsProducts}
                displayType={'text'}
                thousandSeparator={true}
              />
            </>
          }
          title={
            <div className="mb-3">
              Tổng số sản phẩm
            </div>
          }
        />
      </CCol>
      <CCol sm={6} lg={3}>
        <CWidgetStatsA
          className="mb-4"
          color="danger"
          value={
            <>
              <CurrencyFormat
                decimalScale={2}
                value={statsCustomers}
                displayType={'text'}
                thousandSeparator={true}
              />
            </>
          }
          title={
            <div className="mb-3">
              Tổng số khách hàng
            </div>
          }
        />
      </CCol>
    </CRow>
  )
}

export default WidgetsDropdown
