import React, { useState, useEffect } from 'react'
import {
  CCard,
  CCardBody,
  CButton,
  CButtonGroup,
  CCol,
  CRow,
} from '@coreui/react'
import {
  CChartBar,
} from '@coreui/react-chartjs'
import WidgetsDropdown from '../widgets/WidgetsDropdown'
import Config from 'src/utils/config'
import DefineCode from 'src/utils/define_code'
import Util from 'src/utils/util'

async function statsMoneyApi() {
  return fetch(`${Config.link_api_backend}/admin/stats/total-money`)
  .then(data => data.json())
}
async function statsOrdersApi() {
  return fetch(`${Config.link_api_backend}/admin/stats/total-orders`)
  .then(data => data.json())
}
async function statsProductsApi() {
  return fetch(`${Config.link_api_backend}/admin/stats/total-products`)
  .then(data => data.json())
}
async function statsCustomersApi() {
  return fetch(`${Config.link_api_backend}/admin/stats/total-customers`)
  .then(data => data.json())
}
async function statsRevenueApi(statsFilter) {
  return fetch(`${Config.link_api_backend}/admin/stats/revenue?` + new URLSearchParams(statsFilter))
  .then(data => data.json())
}

const Dashboard = () => {
  const DEFAULT_STATS_MONEY_FILTER = 'one_year'
  const DEFAULT_STATS_MONEY = 0
  const DEFAULT_STATS_ORDERS = 0
  const DEFAULT_STATS_PRODUCTS = 0
  const DEFAULT_STATS_CUSTOMERS = 0
  const DEFAULT_STATS_LABELS_REVENUE = []
  const DEFAULT_STATS_DATA_OF_LABELS_REVENUE = []
  const [statsMoneyFilter, setStatsMoneyFilter] = useState(DEFAULT_STATS_MONEY_FILTER)
  const [statsMoney, setStatsMoney] = useState(DEFAULT_STATS_MONEY)
  const [statsOrders, setStatsOrders] = useState(DEFAULT_STATS_ORDERS)
  const [statsProducts, setStatsProducts] = useState(DEFAULT_STATS_PRODUCTS)
  const [statsCustomers, setStatsCustomers] = useState(DEFAULT_STATS_CUSTOMERS)
  const [statsLabelsRevenue, setStatsLabelsRevenue] = useState(DEFAULT_STATS_LABELS_REVENUE)
  const [statsDataOfLabelsRevenue, setStatsDataOfLabelsRevenue] = useState(DEFAULT_STATS_DATA_OF_LABELS_REVENUE)

  const isNotEmptyObj = Util.isNotEmptyObj
  const ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ = DefineCode.ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ
  
  useEffect(() => {
    statsMoneyApi().then(resStats => {
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(resStats.stats)) {
        setStatsMoney(resStats.stats)
      }
    })
    statsOrdersApi().then(resStats => {
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(resStats.stats)) {
        setStatsOrders(resStats.stats)
      }
    })
    statsProductsApi().then(resStats => {
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(resStats.stats)) {
        setStatsProducts(resStats.stats)
      }
    })
    statsCustomersApi().then(resStats => {
      if (!ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ.includes(resStats.stats)) {
        setStatsCustomers(resStats.stats)
      }
    })
    statsRevenueApi({type: statsMoneyFilter}).then(resStats => {
      if (isNotEmptyObj(resStats)) {
        setStatsLabelsRevenue(resStats.labels)
        setStatsDataOfLabelsRevenue(resStats.data)
      }
    })
  }, [statsMoneyFilter, ARR_DATA_EMPTY_VALUE_OF_KEY_OBJ, isNotEmptyObj])

  const handleStatsMoneyFilterBtn = e => {
    e.preventDefault()
    let idBtn = e.target.id
    setStatsMoneyFilter(idBtn)
    statsRevenueApi({type: statsMoneyFilter}).then(resStats => {
      if (isNotEmptyObj(resStats)) {
        setStatsLabelsRevenue(resStats.labels)
        setStatsDataOfLabelsRevenue(resStats.data)
      }
    })
  }

  return (
    <>
      <WidgetsDropdown
        statsMoney={statsMoney}
        statsOrders={statsOrders}
        statsProducts={statsProducts}
        statsCustomers={statsCustomers}
      />
      <CCard className="mb-4">
        {/* <CCardHeader>Thống kê doanh thu</CCardHeader> */}
        <CCardBody>
          <CRow>
            <CCol sm={5}>
              <h4 id="traffic" className="card-title mb-0">
                Thống kê doanh thu
              </h4>
              {/* <div className="small text-medium-emphasis">January - July 2021</div> */}
            </CCol>
            <CCol sm={7} className="d-none d-md-block">
              <CButtonGroup className="float-end me-3" onClick={handleStatsMoneyFilterBtn}>
                {[
                  {id: 'six_months', value: '6 tháng'},
                  {id: 'one_year', value: '1 năm'},
                ].map((obj) => (
                  <CButton
                    color="outline-secondary"
                    id={obj.id}
                    key={obj.id}
                    className="mx-0"
                    active={obj.id === statsMoneyFilter}
                  >
                    {obj.value}
                  </CButton>
                ))}
              </CButtonGroup>
            </CCol>
          </CRow>
          <CChartBar
            data={{
              labels: statsLabelsRevenue,
              datasets: [
                {
                  label: 'Số tiền đã thu',
                  backgroundColor: '#4ce600',
                  data: statsDataOfLabelsRevenue,
                },
              ],
            }}
            labels="months"
          />
        </CCardBody>
      </CCard>

    </>
  )
}

export default Dashboard
