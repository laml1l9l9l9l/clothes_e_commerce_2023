import React from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { useSelector } from 'react-redux'
import {
  CAvatar,
  CDropdown,
  CDropdownDivider,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  // CBadge,
} from '@coreui/react'
import {
  // cilCreditCard,
  cilLockLocked,
  cilSettings,
  cilUser,
  // cilBell,
  // cilCommentSquare,
  // cilEnvelopeOpen,
  // cilFile,
  // cilTask,
} from '@coreui/icons'
import CIcon from '@coreui/icons-react'
import { toast } from 'react-toastify'

import avatar from './../../assets/images/avatars/avatar-default-icon.jpg'
import useToken from 'src/admin_component/useToken'

const AppHeaderDropdown = () => {
  const navigate = useNavigate()
  const { setTokenAdmin } = useToken()
  const tokenAdmin = useSelector(state => state.user_admin)

  const handleAuthentication = e => {
    e.preventDefault()
    if (tokenAdmin) {
      // CASE: Logout Account
      setTokenAdmin(null)
      toast.success('Đăng xuất thành công', {
        position: toast.POSITION.TOP_RIGHT
      })
      navigate('/admin/login', { replace: true })
    }
  }
  return (
    <CDropdown variant="nav-item">
      <CDropdownToggle placement="bottom-end" className="py-0" caret={false}>
        <CAvatar src={avatar} size="md" />
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        {/* <CDropdownHeader className="bg-light fw-semibold py-2">Account</CDropdownHeader>
        <CDropdownItem href="#">
          <CIcon icon={cilBell} className="me-2" />
          Updates
          <CBadge color="info" className="ms-2">
            42
          </CBadge>
        </CDropdownItem>
        <CDropdownItem href="#">
          <CIcon icon={cilEnvelopeOpen} className="me-2" />
          Messages
          <CBadge color="success" className="ms-2">
            42
          </CBadge>
        </CDropdownItem>
        <CDropdownItem href="#">
          <CIcon icon={cilTask} className="me-2" />
          Tasks
          <CBadge color="danger" className="ms-2">
            42
          </CBadge>
        </CDropdownItem>
        <CDropdownItem href="#">
          <CIcon icon={cilCommentSquare} className="me-2" />
          Comments
          <CBadge color="warning" className="ms-2">
            42
          </CBadge>
        </CDropdownItem> */}
        <CDropdownHeader className="bg-light fw-semibold py-2">Cấu hình</CDropdownHeader>
        <Link to='/admin/setup/edit-admin' style={{ textDecoration: 'none' }}>
          <CDropdownItem>
            <CIcon icon={cilUser} className="me-2" />
            Thông tin tài khoản
          </CDropdownItem>
        </Link>
        <Link to='/admin/setup/2fa' style={{ textDecoration: 'none' }}>
          <CDropdownItem>
            <CIcon icon={cilSettings} className="me-2" />
            Xác thực 2 thành phần
          </CDropdownItem>
        </Link>
        {/* <CDropdownItem href="#">
          <CIcon icon={cilCreditCard} className="me-2" />
          Thanh toán điện tử
          <CBadge color="secondary" className="ms-2">
            42
          </CBadge>
        </CDropdownItem>
        <CDropdownItem href="#">
          <CIcon icon={cilFile} className="me-2" />
          Projects
          <CBadge color="primary" className="ms-2">
            42
          </CBadge>
        </CDropdownItem> */}
        <CDropdownDivider />
        <CDropdownItem onClick={handleAuthentication}>
          <CIcon icon={cilLockLocked} className="me-2" />
          Đăng xuất
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default AppHeaderDropdown
