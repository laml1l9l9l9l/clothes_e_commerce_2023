import { useState } from 'react'
// import { useStateValue } from '../StateProvider'
import { useDispatch } from 'react-redux'
import Util from 'src/utils/util'

function useTokenAdmin() {
  // const [ {}, dispatch ] = useStateValue()
  const dispatch = useDispatch()
  const ARR_DATA_EMPTY_VAL = Util.arr_data_empty_val
  const getToken = () => {
    const userToken = sessionStorage.getItem('token-admin')
    return userToken
  }
  const getInfoAccAdmin = () => {
    const infoAcc = sessionStorage.getItem('info-acc-admin')
    return infoAcc
  }
  const [token, setToken] = useState(getToken())
  const [infoAccAd, setInfoAccAd] = useState(getInfoAccAdmin())
  
  const saveToken = (userToken, infoAccAdmin = null) => {
    if (userToken) {
      sessionStorage.setItem('token-admin', userToken)
      dispatch({
        type: 'SET_ADMIN',
        user: userToken
      })
      setToken(userToken)
    } else {
      sessionStorage.removeItem('token-admin')
      dispatch({
        type: 'SET_ADMIN',
        user: null
      })
      setToken(userToken)
    }

    if (infoAccAdmin) {
      sessionStorage.setItem('info-acc-admin', infoAccAdmin)
      dispatch({
        type: 'SET_INFO_USER_ADMIN',
        info_acc: infoAccAdmin
      })
      setInfoAccAd(infoAccAdmin)
    } else {
      sessionStorage.removeItem('info-acc-admin')
      dispatch({
        type: 'SET_INFO_USER_ADMIN',
        info_acc: null
      })
      setInfoAccAd(null)
    }
  }

  return {
    setTokenAdmin: saveToken,
    token_admin: token,
    info_acc_admin: infoAccAd
  }
}

export default useTokenAdmin