import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilPeople,
  cilUserFollow,
  cilNotes,
  cilSpeedometer,
} from '@coreui/icons'
import { CNavGroup, CNavItem, CNavTitle } from '@coreui/react'

const _nav = [
  {
    component: CNavItem,
    name: 'Thống kê',
    to: '/admin/dashboard',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: 'info',
    },
  },
  {
    component: CNavTitle,
    name: 'Sản phẩm',
  },
  {
    component: CNavGroup,
    name: 'Quản lý',
    to: '/admin/products/',
    icon: <CIcon icon={cilNotes} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Tra cứu sản phẩm',
        to: '/admin/products/search',
      },
      {
        component: CNavItem,
        name: 'Thêm mới sản phẩm',
        to: '/admin/products/add',
      },
      {
        component: CNavItem,
        name: 'Tra cứu đơn hàng',
        to: '/admin/orders/search',
      },
      {
        component: CNavItem,
        name: 'Tra cứu giao dịch',
        to: '/admin/transactions/search',
      },
    ],
  },
  // NOTE: Bỏ qua các chức năng này
  // {
  //   component: CNavTitle,
  //   name: 'Quản lý admin',
  // },
  // {
  //   component: CNavItem,
  //   name: 'Tạo tài khoản',
  //   to: '/admin/manager-admin/add-user',
  //   icon: <CIcon icon={cilAddressBook} customClassName="nav-icon" />,
  // },
  // {
  //   component: CNavItem,
  //   name: 'Quản trị admin',
  //   to: '/admin/theme/typography',
  //   icon: <CIcon icon={cilBadge} customClassName="nav-icon" />,
  // },
  // NOTE: Bỏ qua các chức năng này
  {
    component: CNavTitle,
    name: 'Quản lý người dùng',
  },
  {
    component: CNavItem,
    name: 'Tra cứu tài khoản',
    to: '/admin/customers/search',
    icon: <CIcon icon={cilPeople} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Tạo tài khoản',
    to: '/admin/customers/add',
    icon: <CIcon icon={cilUserFollow} customClassName="nav-icon" />,
  },
]

export default _nav
