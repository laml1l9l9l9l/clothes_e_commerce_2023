# Clothes E Commerce 2023

# Element of project
1. Nodejs (server side)
2. Reactjs (client side)
3. Mysql (database)

## Set up
1. Environments
- Node v14.19.0
2. Database
- Import file dump to db: ./document/dumps_db/dump-clothes_e_commerce-202306192046.sql